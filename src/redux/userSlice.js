import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import jwtDecode from "jwt-decode";
import { url, setHeaders, setHeadersPostPut } from "./api";

// initial state
const initialState = {
  _id: "",
  name: "",
  email: "",
  slug: "",
  address: "",
  profile_picture: "",
  phone_number: "",
  city_id: "",
  createdAt: "",
  userProfileStatus: "",
  userProfileStatusError: "",
  updateUser: {},
};

// fetch user
export const fetchMyProfile = createAsyncThunk(
  "user/fetchAsyncUserProfile",
  async ({ rejectWithValue }) => {
    try {
      const response = await axios.get(`${url}/user/my-profile`, setHeaders());
      return response.data;
    } catch (error) {
      console.log(error.response);
      return rejectWithValue(error.response.data);
    }
  }
);

// edit user
export const updateUserProfile = createAsyncThunk(
  "userProfile/editProfileUser",
  async (user, { rejectWithValue }) => {
    try {
      const response = await axios.put(
        `${url}/user/update`,
        {
          name: user.name,
          address: user.address,
          profile_picture: user.profilePicture,
          phone_number: user.phoneNumber,
          city_id: user.cityId,
        },
        setHeadersPostPut()
      );
      return response.data;
    } catch (error) {
      console.log(error.response);
      return rejectWithValue(error.response.data);
    }
  }
);

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchMyProfile.pending, (state, action) => {
      return { ...state, userProfileStatus: "pending" };
    });

    builder.addCase(fetchMyProfile.fulfilled, (state, action) => {
      if (action.payload) {
        return {
          ...action.payload,
          userProfileStatus: "success",
        };
      } else return state;
    });

    builder.addCase(fetchMyProfile.rejected, (state, action) => {
      return {
        ...state,
        userProfileStatus: "rejected",
        userProfileStatusError: action.payload,
      };
    });

    // update user
    builder.addCase(updateUserProfile.pending, (state, payload) => {
      return { ...state, updateUser: payload };
    });
    builder.addCase(updateUserProfile.fulfilled, (state, payload) => {
      return {
        ...state,
        updateUser: payload,
      };
    });
    builder.addCase(updateUserProfile.rejected, (state, payload) => {
      return {
        ...state,
        updateUser: payload,
      };
    });
  },
});

export const getUserProfile = (state) => state.user
export const updateUserProfileState = (state) => state.user.updateUser;
export default userSlice.reducer;
