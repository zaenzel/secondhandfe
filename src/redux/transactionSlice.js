import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { setHeaders, setHeadersPostOffer, setHeaderPostPut, url } from "./api";

export const getAllTransaction = createAsyncThunk(
  "transaction/getAllTransaction",
  async () => {
    const res = await axios.get(`${url}/transaction`, setHeaders());
    return res.data;
  }
);

export const getTransactionBySeller = createAsyncThunk(
  "transaction/getTransactionBySeller",
  async () => {
    const res = await axios.get(`${url}/transaction/seller`, setHeaders());
    return res.data;
  }
);

export const getTransactionByUser = createAsyncThunk(
  "transaction/getTransactionByUser",
  async () => {
    const res = await axios.get(`${url}/transaction/user`, setHeaders());
  }
);

export const getTransactionById = createAsyncThunk(
  "transaction/getTransactionById",
  async (id) => {
    const res = await axios.get(`${url}/transaction/${id}`, setHeaders());
  }
);

export const putDealTransaction = createAsyncThunk(
  "transaction/putDealTransaction",
  async (transaction) => {
    const res = await axios.put(
      `${url}/transaction/success/${transaction.productId}`,
      {
        accepted_bidder: transaction.userId,
      },
      setHeadersPostOffer()
    );
  }
);

export const putCancelTransaction = createAsyncThunk(
  "transaction/putCancelTransaction",
  async (transaction) => {
    const res = await axios.put(
      `${url}/transaction/cancel/${transaction.productId}`,
      {
        accepted_bidder: transaction.userId,
      },
      setHeadersPostOffer()
    );
  }
);

const initialState = {
  allTransaction: {},
  transactionByUser: {},
  transactionBySeller: {},
  transactionById: {},
  dealTransaction: {},
  cancelTransaction: {},
};

const transactionSlice = createSlice({
  name: "transaction",
  initialState,
  reducers: {},
  extraReducers: {
    // all transaction
    [getAllTransaction.rejected]: (state, { payload }) => {
      console.log("failed get All transaction");
      return { ...state, allTransaction: payload };
    },
    [getAllTransaction.pending]: (state, { payload }) => {
      console.log("pending get All transaction");
      return { ...state, allTransaction: payload };
    },
    [getAllTransaction.fulfilled]: (state, { payload }) => {
      console.log("success get All transaction");
      return { ...state, allTransaction: payload };
    },
    // get by user
    [getTransactionByUser.rejected]: (state, { payload }) => {
      console.log("failed get transaction by user");
      return { ...state, transactionByUser: payload };
    },
    [getTransactionByUser.pending]: (state, { payload }) => {
      console.log("pending get transaction by user");
      return { ...state, transactionByUser: payload };
    },
    [getTransactionByUser.fulfilled]: (state, { payload }) => {
      console.log("success get transaction by user");
      return { ...state, transactionByUser: payload };
    },
    // get by id
    [getTransactionById.rejected]: (state, { payload }) => {
      console.log("failed get transaction by id");
      return { ...state, transactionById: payload };
    },
    [getTransactionById.pending]: (state, { payload }) => {
      console.log("pending get transaction by id");
      return { ...state, transactionById: payload };
    },[getTransactionById.fulfilled]: (state, { payload }) => {
      console.log("success get transaction by id");
      return { ...state, transactionById: payload };
    },
    // get by seller
    [getTransactionBySeller.fulfilled]: (state, { payload }) => {
      console.log("success get transaction by seller");
      return { ...state, transactionBySeller: payload };
    },
    // put deal transaction
    [putDealTransaction.rejected]: (state, { payload }) => {
      console.log("failed deal transaction");
      return { ...state, dealTransaction: payload };
    },
    [putDealTransaction.pending]: (state, { payload }) => {
      console.log("pending deal transaction");
      return { ...state, dealTransaction: payload };
    },
    [putDealTransaction.fulfilled]: (state, { payload }) => {
      console.log("success deal transaction");
      return { ...state, dealTransaction: payload };
    },
    // put cancel transaction
    [putCancelTransaction.rejected]: (state, { payload }) => {
      console.log("failed cancel transaction");
      return { ...state, cancelTransaction: payload };
    },
    [putCancelTransaction.pending]: (state, { payload }) => {
      console.log("pending cancel transaction");
      return { ...state, cancelTransaction: payload };
    },
    [putCancelTransaction.fulfilled]: (state, { payload }) => {
      console.log("success cancel transaction");
      return { ...state, cancelTransaction: payload };
    },
  },
});

export const dealTransactionState = (state) =>
  state.transaction.dealTransaction;
export const cancelTransactionState = (state) =>
  state.transaction.cancelTransaction;
export const allTransactionState = (state) => state.transaction.allTransaction;
export const transactionByUserState = (state) =>
  state.transaction.transactionByUser;
export const transactionByIdState = (state) =>
  state.transaction.transactionById;
export const transactionBySellerState = (state) =>
  state.transaction.transactionBySeller;

export default transactionSlice.reducer;
