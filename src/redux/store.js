import { configureStore } from '@reduxjs/toolkit'
import authReducer from './authSlice'
import cityReducer from './citySlice'
import offerReducer from './offerSlice'
import productReducer from './productSlice'
import userReducer from './userSlice'
import wishlist from './wishlist'
import transactionReducer from './transactionSlice'
import notifs from './notifSlice'

export const store = configureStore({
    reducer:{
        auth: authReducer,
        product: productReducer,
        user: userReducer,
        city: cityReducer,
        offer: offerReducer,
        wishlist: wishlist,
        transaction: transactionReducer,
        notif: notifs,
    }
})