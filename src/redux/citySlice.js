import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { url } from "./api";

export const fetchAsyncCities = createAsyncThunk(
  "cities/fetchAsyncCities",
  async () => {
    const res = await axios.get(`${url}/city`);
    return res.data;
  }
);

export const fetchAsyncCityById = createAsyncThunk(
  "city/fetchAsyncCityById",
  async (id) => {
    const res = await axios.get(`${url}/city/${id}`);
    return res.data;
  }
);

const initialState = {
  cities: {},
  city: {}
};

const citySlice = createSlice({
  name: "city",
  initialState,
  reducers: {},
  extraReducers: {
    [fetchAsyncCities.pending]: () => {
      console.log("pending");
    },
    [fetchAsyncCities.fulfilled]: (state, { payload }) => {
      return { ...state, cities: payload };
    },
    [fetchAsyncCities.rejected]: () => {
      console.log("fetch failed");
    },
    [fetchAsyncCityById.pending]: () => {
      console.log("pending");
    },
    [fetchAsyncCityById.fulfilled]: (state, { payload }) => {
      return { ...state, city: payload };
    },
    [fetchAsyncCityById.rejected]: () => {
      console.log("fetch failed");
    },
  },
});

// ambil isi state
export const getAllCities = (state) => state.city.cities;
export const getCityById = (state) => state.city.city;

export default citySlice.reducer;
