import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import jwtDecode from "jwt-decode";
import { url } from "./api";

const initialState = {
  token: localStorage.getItem("token"),
  name: "",
  email: "",
  _id: "",
  registerStatus: "",
  registerMessage: "",
  loginStatus: "",
  loginError: "",
  userLoaded: false,
};

// REGISTER
export const registerUser = createAsyncThunk(
  "auth/registerUser",
  async (user, { rejectWithValue }) => {
    try {
      const response = await axios.post(`${url}/auth/signup`, {
        name: user.name,
        email: user.email,
        password: user.password,
      });
      return response.data;
    } catch (err) {
      // console.log(err.response.data);
      return rejectWithValue(err.response.data);
    }
  }
);

// LOGIN
export const loginUser = createAsyncThunk(
  "auth/loginUser",
  async (user, { rejectWithValue }) => {
    try {
      const token = await axios.post(`${url}/auth/login`, {
        email: user.email,
        password: user.password,
      });
      localStorage.setItem("token", token.data.data.token);
      console.log(token.data.data.token);
      return token.data;
    } catch (err) {
      // console.log(err.response.data);
      return rejectWithValue(err.response.data);
    }
  }
);

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    loadUser(state, action) {
      const token = state.token;

      if (token) {
        const user = jwtDecode(token);
        if (jwtDecode(token).exp > Date.now() / 1000) {
          return {
            ...state,
            token,
            name: user.name,
            email: user.email,
            _id: user.userId,
            userLoaded: true,
          };
        }
      } else {
        localStorage.clear();
      }
    },

    logoutUser(state, action) {
      localStorage.removeItem("token");

      return {
        ...state,
        token: "",
        name: "",
        email: "",
        _id: "",
        registerStatus: "",
        registerMessage: "",
        loginStatus: "",
        loginError: "",
        userLoaded: false,
      };
    },
  },
  extraReducers: (builder) => {
    // register
    builder.addCase(registerUser.pending, (state, action) => {
      return { ...state, registerStatus: "pending" };
    });
    builder.addCase(registerUser.fulfilled, (state, action) => {
      if (action.payload) {
        const user = action.payload;
        return {
          // ...state,
          registerMessage: action.payload.message,
          // name: user.name,
          // email: user.email,
          // _id: user.userId,
          registerStatus: "success",
        };
      } else return state;
    });
    builder.addCase(registerUser.rejected, (state, action) => {
      return {
        ...state,
        registerStatus: "rejected",
        registerMessage: action.payload.message,
      };
    });

    // login
    builder.addCase(loginUser.pending, (state, action) => {
      return { ...state, loginStatus: "pending" };
    });
    builder.addCase(loginUser.fulfilled, (state, action) => {
      if (action.payload) {
        const user = jwtDecode(action.payload.data.token);
        return {
          ...state,
          token: action.payload.data.token,
          loginMessage: action.payload.message,
          name: user.name,
          email: user.email,
          _id: user.userId,
          loginStatus: "success",
        };
      } else return state;
    });
    builder.addCase(loginUser.rejected, (state, action) => {
      return {
        ...state,
        loginStatus: "rejected",
        loginMessage: action.payload.message,
      };
    });
  },
});

export const { loadUser, logoutUser } = authSlice.actions;
export default authSlice.reducer;
