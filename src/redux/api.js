import jwtDecode from "jwt-decode";

export const url = "https://secondhandbe.herokuapp.com/api/v1";
// export const url = "https://dev-secondhandbe.herokuapp.com/api/v1"

export const setHeaders = () => {
  const headers = {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  };

  if (jwtDecode(localStorage.getItem("token")).exp < Date.now() / 1000) {
    localStorage.clear();
  }

  return headers;
};

export const setHeadersPostPut = () => {
  const headers = {
    headers: {
      "Content-Type": "multipart/form-data",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  };

  if (jwtDecode(localStorage.getItem("token")).exp < Date.now() / 1000) {
    localStorage.clear();
  }

  return headers;
};

export const setHeadersPostOffer = () => {
  const headers = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  };

  if (jwtDecode(localStorage.getItem("token")).exp < Date.now() / 1000) {
    localStorage.clear();
  }

  return headers;
};
