import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { setHeaders, setHeadersPostPut, url } from "./api";


export const fetchAsyncProducts = createAsyncThunk(
  "products/fetchAsyncProducts",
  async (page) => {
    const res = await axios.get(`${url}/product?page=${page}&limit=12`);
    return res.data;
  }
);

export const fetchProductsByFilter = createAsyncThunk(
  "products/fetchProductsByFilter",
  async (filter) => {
    const res = await axios.get(`${url}/product?filter=${filter}`);
    return res.data;
  }
);

export const fetchAsyncProductsUser = createAsyncThunk(
  "products/fetchAsyncProductsUser",
  async (id) => {
    const res = await axios.get(`${url}/product/user/${id}`);
    return res.data;
  }
);

export const productByHobi = createAsyncThunk(
  "products/productByHobi",
  async (page) => {
    const res = await axios.get(
      `${url}/product/category/hobi?page=${page}&limit=12`
    );
    return res.data;
  }
);

export const productByKendaraan = createAsyncThunk(
  "products/productByKendaraan",
  async (page) => {
    const res = await axios.get(
      `${url}/product/category/kendaraan?page=${page}&limit=12`
    );
    return res.data;
  }
);

export const productByBaju = createAsyncThunk(
  "products/productByBaju",
  async (page) => {
    const res = await axios.get(
      `${url}/product/category/baju?page=${page}&limit=12`
    );
    return res.data;
  }
);

export const productByElektronik = createAsyncThunk(
  "products/productByElektronik",
  async (page) => {
    const res = await axios.get(
      `${url}/product/category/elektronik?page=${page}&limit=12`
    );
    return res.data;
  }
);

export const productByKesehatan = createAsyncThunk(
  "products/productByKesehatan",
  async (page) => {
    const res = await axios.get(
      `${url}/product/category/kesehatan?page=${page}&limit=12`
    );
    return res.data;
  }
);

// detail product
export const fetchAsyncDetailProduct = createAsyncThunk(
  "products/fetchAsyncDetailProducts",
  async (slug) => {
    const res = await axios.get(
      `${url}/product/detail-with-auth/${slug}`,
      setHeaders()
    );
    return res.data;
  }
);

export const fetchDetailProductNoAuth = createAsyncThunk(
  "products/fetchDetailProductNoAuth",
  async (slug) => {
    const res = await axios.get(`${url}/product/detail-no-auth/${slug}`);
    return res.data;
  }
);

// add product
export const addProduct = createAsyncThunk(
  "products/addProduct",
  async (formData, { rejectWithValue }) => {
    try {
      const res = await axios.post(
        `${url}/product`,
        formData,
        setHeadersPostPut()
      );
      return res.data;
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  }
);

// delete product
export const deleteAsyncProduct = createAsyncThunk(
  "product/deleteMessage",
  async (slug, { rejectWithValue }) => {
    const res = await axios.delete(`${url}/product/${slug}`, setHeaders());
    return res.data;
  }
);

// update product
export const updateProduct = createAsyncThunk(
  "products/updateProduct",
  async ({ slug, formData }, { rejectWithValue }) => {
    try {
      const res = await axios.put(
        `${url}/product/${slug}`,
        formData,
        setHeadersPostPut()
      );
      return res.data;
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  }
);

// SEARCH : get Product by slug
export const fetchProductBySlug = createAsyncThunk(
  "products/fetchProductBySlug",
  async (slug, { rejectWithValue }) => {
    try {
      const res = await axios.get(`${url}/product/${slug}`);
      return res.data;
    } catch (error) {
      return rejectWithValue(error.response.message);
    }
  }
);

const initialState = {
  products: {},
  filter: {},
  productsUser: {},
  detail: {},
  detailNoAuth: {},
  hobi: {},
  kendaraan: {},
  baju: {},
  elektronik: {},
  kesehatan: {},
  newProduct: {},
  editProduct: {},
  modalBargain: false,
  preview: {},
  status: "",
};

const productSlice = createSlice({
  name: "product",
  initialState,
  reducers: {
    showModalBargain: (state) => {
      state.modalBargain = true;
    },
    removeDetail: (state) => {
      state.detail = {};
    },
    removeStatus: (state) => {
      state.status = "";
    },
    removeProductUser: (state) => {
      state.productsUser = {};
    },
    previewProduct: (state, { payload }) => {
      state.preview = payload
    },
  },
  extraReducers: {
    // get all product
    [fetchAsyncProducts.pending]: () => {
      console.log("pending");
    },
    [fetchAsyncProducts.fulfilled]: (state, { payload }) => {
      return { ...state, products: payload };
    },
    [fetchAsyncProducts.rejected]: () => {
      console.log("fetch failed");
    },
    // filter
    [fetchProductsByFilter.pending]: (state) => {
      return { ...state, status: "pending" };
    },
    [fetchProductsByFilter.fulfilled]: (state, { payload }) => {
      return { ...state, filter: payload, status: "success" };
    },
    [fetchProductsByFilter.rejected]: (state) => {
      return { ...state, status: "rejected" };
    },
    // get product by user
    [fetchAsyncProductsUser.pending]: (state) => {
      return { ...state, statusProductUser: "pending" };
    },
    [fetchAsyncProductsUser.fulfilled]: (state, { payload }) => {
      return { ...state, productsUser: payload, statusProductUser: "success" };
    },
    // get detail product
    [fetchAsyncDetailProduct.fulfilled]: (state, { payload }) => {
      return { ...state, detail: payload };
    },
    [fetchDetailProductNoAuth.fulfilled]: (state, { payload }) => {
      return { ...state, detailNoAuth: payload };
    },
    // get by kategori
    [productByHobi.fulfilled]: (state, { payload }) => {
      return { ...state, hobi: payload };
    },
    [productByKendaraan.fulfilled]: (state, { payload }) => {
      return { ...state, kendaraan: payload };
    },
    [productByBaju.fulfilled]: (state, { payload }) => {
      return { ...state, baju: payload };
    },
    [productByElektronik.fulfilled]: (state, { payload }) => {
      return { ...state, elektronik: payload };
    },
    [productByKesehatan.fulfilled]: (state, { payload }) => {
      return { ...state, kesehatan: payload };
    },
    // add
    [addProduct.pending]: (state, action) => {
      return { ...state, newProduct: action.payload, status: "pending" };
    },
    [addProduct.fulfilled]: (state, action) => {
      return { ...state, newProduct: action.payload, status: "addProduct" };
    },
    [addProduct.rejected]: (state, action) => {
      return { ...state, newProduct: action.payload, status: "rejected" };
    },
    // delete
    [deleteAsyncProduct.fulfilled]: (state, action) => {
      console.log("delete success");
      console.log(action.payload);
      if (action.payload) {
        return { ...state, statusDelete: "success", status: "deleteProduct" };
      }
    },
    [deleteAsyncProduct.pending]: (state, action) => {
      console.log("delete pending");
      if (action.payload) {
        return {
          ...state,
          statusDelete: action.payload.message,
          status: "deleteProduct",
        };
      }
    },
    [deleteAsyncProduct.rejected]: (state, action) => {
      console.log("delete reject");
      if (action.payload) {
        return { ...state, statusDelete: action.payload };
      }
    },
    // update
    [updateProduct.pending]: (state, action) => {
      return { ...state, editProduct: action.payload, status: "pending" };
    },
    [updateProduct.fulfilled]: (state, action) => {
      return { ...state, editProduct: action.payload, status: "updateProduct" };
    },
    [updateProduct.rejected]: (state, action) => {
      console.log(action.payload);
      return { ...state, editProduct: action.payload, status: "rejected" };
    },
  },
});

export const {
  removeDetail,
  removeStatus,
  showModalBargain,
  removeProductUser,
  previewProduct,
} = productSlice.actions;
// ambil isi state
export const getAllProducts = (state) => state.product.products;
export const getAllProductFilter = (state) => state.product.filter;
export const getAllProductsUser = (state) => state.product.productsUser;
export const getStatusProductUser = (state) => state.product.statusProductUser;
export const getDetailProduct = (state) => state.product.detail;
export const getDetailProductNoAuth = (state) => state.product.detailNoAuth;
export const getProductHobi = (state) => state.product.hobi;
export const getProductKendaraan = (state) => state.product.kendaraan;
export const getProductBaju = (state) => state.product.baju;
export const getProductElektronik = (state) => state.product.elektronik;
export const getProductKesehatan = (state) => state.product.kesehatan;
export const getStatusProduct = (state) => state.product.status;
export const getdeleteStatusProduct = (state) => state.product.statusDelete;
export const getModalBargain = (state) => state.product.modalBargain;
export const productNewState = (state) => state.product.newProduct;
export const editNewState = (state) => state.product.editProduct;
export const previewState = (state) => state.product.preview;
export default productSlice.reducer;
