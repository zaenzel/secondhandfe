//redux notifSlice.js
import axios from 'axios';
import { url, setHeaders } from './api';
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

//fetchAsyncNotifs
export const fetchAsyncNotifs = createAsyncThunk(
    'notif/fetchAsyncNotifs',
    async () => {
        const res = await axios.get(`${url}/notification`, setHeaders());
        return res.data;
    }
);

//initialState
const initialState = {
    notifs: {},
    status: {}
}

//createSlice
const notifSlice = createSlice({
    name: 'notif',
    initialState,
    reducers: {},
    extraReducers: {
        [fetchAsyncNotifs.pending]: (state, action) => {
            return { ...state, notifs: action.payload, status:"pending" };
        },
        [fetchAsyncNotifs.fulfilled]: (state, { payload }) => {
            return { ...state, notifs: payload, status:"success" };
        },
        [fetchAsyncNotifs.rejected]: (state, action) => {
            return { ...state, notifs: action.payload, status:"rejected" };
        },
    },
});

//ambil isi state
export const getAllNotifs = (state) => state.notif.notifs;
export const getStatusNotif = (state) => state.notif.status

export default notifSlice.reducer;