import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { setHeaders, setHeadersPostPut, url } from "./api";

// get wishlist
export const fetchWishlist = createAsyncThunk(
  "wishlist/fetchWishlist",
  async () => {
    try {
      const res = await axios.get(`${url}/wishlist`, setHeaders());
      return res.data;
    } catch (error) {
      console.log(error);
    }
  }
);

// add wishlist
export const addWishlist = createAsyncThunk(
  "wishlist/addWishlist",
  async (id) => {
    try {
      const res = await axios.post(
        `${url}/wishlist`,
        { product_id: id },
        setHeaders()
      );
      return res.data;
    } catch (error) {
      console.log(error);
    }
  }
);

// delete wishlist
export const deleteWishlist = createAsyncThunk(
  "wishlist/deleteWishlist",
  async (id) => {
    try {
      const res = await axios.delete(`${url}/wishlist/${id}`, setHeaders());
      return res.data;
    } catch (error) {
      console.log(error);
    }
  }
);

const initialState = {
  statusWishList: "",
  wishlist: {},
};

const wishlistSlice = createSlice({
  name: "wishlist",
  initialState,
  reducers: {
    removeStatusWishlist: (state) => {
      state.statusWishList = "";
    },
  },
  extraReducers: {
    // get
    [fetchWishlist.pending]: (state) => {
      return { ...state, statusWishList: "pending" };
    },
    [fetchWishlist.fulfilled]: (state, { payload }) => {
      return { ...state, wishlist: payload };
    },
    [fetchWishlist.rejected]: (state) => {
      return { ...state, statusWishList: "rejected" };
    },
    // add
    [addWishlist.pending]: (state) => {
      return { ...state, statusWishList: "pending" };
    },
    [addWishlist.fulfilled]: (state) => {
      return { ...state, statusWishList: "wishListAdd" };
    },
    [addWishlist.rejected]: (state) => {
      return { ...state, statusWishList: "rejected" };
    },
    // delete
    [deleteWishlist.pending]: (state) => {
      return { ...state, statusWishList: "pending" };
    },
    [deleteWishlist.fulfilled]: (state) => {
      return { ...state, statusWishList: "wishListDelete" };
    },
    [deleteWishlist.rejected]: (state) => {
      return { ...state, statusWishList: "rejected" };
    },
  },
});

export const { removeStatusWishlist } = wishlistSlice.actions;
export const getWishlist = (state) => state.wishlist.wishlist;
export const getStatusWishList = (state) => state.wishlist.statusWishList;
export default wishlistSlice.reducer;
