import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { setHeaders, setHeadersPostOffer, setHeadersPostPut, url } from "./api";

export const fetchAllProductOffer = createAsyncThunk(
  "offer/fetchAllProductOffer",
  async () => {
    const res = await axios.get(
      `${url}/disc-product-offer/seller`,
      setHeaders()
    );
    return res.data;
  }
);

export const postProductOffer = createAsyncThunk(
  "offer/postProductOffer",
  async (offer, { rejectWithValue }) => {
    try {
      const res = await axios.post(
        `${url}/disc-product-offer`,
        {
          product_id: offer.productId,
          bargain_price: offer.bargainPrice,
        },
        setHeadersPostOffer()
      );
      return res.data;
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  }
);

export const fetchProductOfferById = createAsyncThunk(
  "offer/fetchProductOfferById",
  async (id) => {
    const res = await axios.get(
      `${url}/disc-product-offer/${id}`,
      setHeaders()
    );
    return res.data;
  }
);

export const putAcceptProductOffer = createAsyncThunk(
  "offer/putAcceptProductOffer",
  async (offer, { rejectWithValue }) => {
    try {
      const res = await axios.put(
        `${url}/disc-product-offer/accept/${offer.userId}`,
        {
          product_id: offer.productId,
        },
        setHeadersPostOffer()
      );
      return res.data;
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  }
);

export const putRejectProductOffer = createAsyncThunk(
  "offer/putRejectProductOffer",
  async (offer, { rejectWithValue }) => {
    try {
      const res = await axios.put(
        `${url}/disc-product-offer/reject/${offer.userId}`,
        {
          product_id: offer.productId,
        },
        setHeadersPostOffer()
      );
      return res.data;
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  }
);

export const deleteProductOffer = createAsyncThunk(
  "offer/deleteProductOffer",
  async (id) => {
    const res = await axios.delete(
      `${url}/disc-product-offer/${id}`,
      setHeaders()
    );
    return res.data;
  }
);

// BY BIDDER

export const fetchOfferByBidder = createAsyncThunk(
  "offer/fetchOfferByBidder",
  async () => {
    const res = await axios.get(
      `${url}/disc-product-offer/bidder`,
      setHeaders()
    );
    return res.data;
  }
);

export const fetchOfferByBidderPending = createAsyncThunk(
  "offer/fetchOfferByBidderPending",
  async () => {
    const res = await axios.get(
      `${url}/disc-product-offer/bidder/status/pending`,
      setHeaders()
    );
    return res.data;
  }
);

export const fetchOfferByBidderAccepted = createAsyncThunk(
  "offer/fetchOfferByBidderAccepted",
  async () => {
    const res = await axios.get(
      `${url}/disc-product-offer/bidder/status/accepted`,
      setHeaders()
    );
    return res.data;
  }
);

export const fetchOfferByBidderRejected = createAsyncThunk(
  "offer/fetchOfferByBidderRejected",
  async () => {
    const res = await axios.get(
      `${url}/disc-product-offer/bidder/status/rejected`,
      setHeaders()
    );
    return res.data;
  }
);

// BY SELLER

export const fetchOfferBySeller = createAsyncThunk(
  "offer/fetchOfferBySeller",
  async () => {
    const res = await axios.get(
      `${url}/disc-product-offer/seller`,
      setHeaders()
    );
    return res.data;
  }
);

export const fetchOfferBySellerPending = createAsyncThunk(
  "offer/fetchOfferBySellerPending",
  async () => {
    const res = await axios.get(
      `${url}/disc-product-offer/seller/status/pending`,
      setHeaders()
    );
    return res.data;
  }
);

export const fetchOfferBySellerAccepted = createAsyncThunk(
  "offer/fetchOfferBySellerAccepted",
  async () => {
    const res = await axios.get(
      `${url}/disc-product-offer/seller/status/accepted`,
      setHeaders()
    );
    return res.data;
  }
);

export const fetchOfferBySellerRejected = createAsyncThunk(
  "offer/fetchOfferBySellerRejected",
  async () => {
    const res = await axios.get(
      `${url}/disc-product-offer/seller/status/rejected`,
      setHeaders()
    );
    return res.data;
  }
);

const initialState = {
  allProductOffer: {},
  productOfferById: {},
  createProductOffer: {},
  acceptOffer: {},
  rejectOffer: {},
  deleteOffer: {},
  offerByBidder: {},
  offerByBidderPending: {},
  offerByBidderAccepted: {},
  offerByBidderRejected: {},
  offerBySeller: {},
  offerBySellerPending: {},
  offerBySellerAccepted: {},
  offerBySellerRejected: {},
};

const offerSlice = createSlice({
  name: "offer",
  initialState,
  reducers: {
    removeStatusOffer: (state) => {
      state.statusPostOffer = "";
    },
    removeStatusAcceptOffer: (state) => {
      state.statusAcceptOffer = "";
    },
    removeStatusRejectOffer: (state) => {
      state.statusRejectOffer = "";
    },
  },
  extraReducers: {
    [fetchAllProductOffer.rejected]: (state, { payload }) => {
      console.log("failed fetch all product offer!");
      return { ...state, allProductOffer: payload };
    },
    [fetchAllProductOffer.pending]: (state, { payload }) => {
      console.log("pending fetch all product offer!");
      return { ...state, allProductOffer: payload };
    },
    [fetchAllProductOffer.fulfilled]: (state, { payload }) => {
      console.log("success fetch all product offer!");
      return { ...state, allProductOffer: payload };
    },
    [fetchProductOfferById.rejected]: (state, { payload }) => {
      console.log("failed fetch product offer by id!");
      return { ...state, productOfferById: payload };
    },
    [fetchProductOfferById.pending]: (state, { payload }) => {
      console.log("pending fetch product offer by id!");
      return { ...state, productOfferById: payload };
    },
    [fetchProductOfferById.fulfilled]: (state, { payload }) => {
      console.log("success fetch product offer by id!");
      return { ...state, productOfferById: payload };
    },
    [postProductOffer.rejected]: (state, action) => {
      console.log("failed post product offer!");
      console.log(action.payload);
      return {
        ...state,
        createProductOffer: action.payload,
        statusPostOffer: "rejected",
      };
    },
    [postProductOffer.pending]: (state, action) => {
      console.log("pending post product offer!");
      console.log(action.payload);
      return {
        ...state,
        createProductOffer: action.payload,
        statusPostOffer: "pending",
      };
    },
    [postProductOffer.fulfilled]: (state, action) => {
      console.log(action.payload);
      return {
        ...state,
        createProductOffer: action.payload,
        statusPostOffer: "success",
      };
    },
    [putAcceptProductOffer.rejected]: (state, action) => {
      console.log("failed accept offer!");
      return {
        ...state,
        acceptOffer: action.payload,
        statusAcceptOffer: "rejected",
      };
    },
    [putAcceptProductOffer.pending]: (state, action) => {
      console.log("pending accept offer!");
      return {
        ...state,
        acceptOffer: action.payload,
        statusAcceptOffer: "pending",
      };
    },
    [putAcceptProductOffer.fulfilled]: (state, action) => {
      console.log("success accept offer!");
      return {
        ...state,
        acceptOffer: action.payload,
        statusAcceptOffer: "success",
      };
    },
    [putRejectProductOffer.rejected]: (state, action) => {
      console.log("failed reject offer!");
      return {
        ...state,
        rejectOffer: action.payload,
        statusRejectOffer: "rejected",
      };
    },
    [putRejectProductOffer.pending]: (state, action) => {
      console.log("pending reject offer!");
      return {
        ...state,
        rejectOffer: action.payload,
        statusRejectOffer: "pending",
      };
    },
    [putRejectProductOffer.fulfilled]: (state, action) => {
      console.log("success reject offer!");
      return {
        ...state,
        rejectOffer: action.payload,
        statusRejectOffer: "success",
      };
    },
    [deleteProductOffer.rejected]: (state, { payload }) => {
      console.log("failed delete offer!");
      return { ...state, deleteOffer: payload };
    },
    [deleteProductOffer.pending]: (state, { payload }) => {
      console.log("pending delete offer!");
      return { ...state, deleteOffer: payload };
    },
    [deleteProductOffer.fulfilled]: (state, { payload }) => {
      console.log("success delete offer!");
      return { ...state, deleteOffer: payload };
    },
    [fetchOfferByBidder.rejected]: (state, { payload }) => {
      console.log("failed get offer by bidder!");
      return { ...state, offerByBidder: payload };
    },
    [fetchOfferByBidder.pending]: (state, { payload }) => {
      console.log("pending get offer by bidder!");
      return { ...state, offerByBidder: payload };
    },
    [fetchOfferByBidder.fulfilled]: (state, { payload }) => {
      console.log("success get offer by bidder!");
      return { ...state, offerByBidder: payload };
    },
    [fetchOfferByBidderPending.rejected]: (state, { payload }) => {
      console.log("failed get offer by bidder Pending!");
      return { ...state, offerByBidderPending: payload };
    },
    [fetchOfferByBidderPending.pending]: (state, { payload }) => {
      console.log("pending get offer by bidder Pending!");
      return { ...state, offerByBidderPending: payload };
    },
    [fetchOfferByBidderPending.fulfilled]: (state, { payload }) => {
      console.log("success get offer by bidder Pending!");
      return { ...state, offerByBidderPending: payload };
    },
    [fetchOfferByBidderAccepted.rejected]: (state, { payload }) => {
      console.log("failed get offer by bidder Accepted!");
      return { ...state, offerByBidderAccepted: payload };
    },
    [fetchOfferByBidderAccepted.pending]: (state, { payload }) => {
      console.log("pending get offer by bidder Accepted!");
      return { ...state, offerByBidderAccepted: payload };
    },
    [fetchOfferByBidderAccepted.fulfilled]: (state, { payload }) => {
      console.log("success get offer by bidder Accepted!");
      return { ...state, offerByBidderAccepted: payload };
    },
    [fetchOfferByBidderRejected.rejected]: (state, { payload }) => {
      console.log("failed get offer by bidder Rejected!");
      return { ...state, offerByBidderRejected: payload };
    },
    [fetchOfferByBidderRejected.pending]: (state, { payload }) => {
      console.log("pending get offer by bidder Rejected!");
      return { ...state, offerByBidderRejected: payload };
    },
    [fetchOfferByBidderRejected.fulfilled]: (state, { payload }) => {
      console.log("success get offer by bidder Rejected!");
      return { ...state, offerByBidderRejected: payload };
    },
    [fetchOfferBySeller.rejected]: (state, { payload }) => {
      console.log("failed get offer by seller!");
      return { ...state, offerBySeller: payload };
    },
    [fetchOfferBySeller.pending]: (state, { payload }) => {
      console.log("pending get offer by seller!");
      return { ...state, offerBySeller: payload };
    },
    [fetchOfferBySeller.fulfilled]: (state, { payload }) => {
      console.log("success get offer by seller!");
      return { ...state, offerBySeller: payload };
    },
    [fetchOfferBySellerPending.rejected]: (state, { payload }) => {
      console.log("failed get offer by seller Pending!");
      return { ...state, offerBySellerPending: payload };
    },
    [fetchOfferBySellerPending.pending]: (state, { payload }) => {
      console.log("pending get offer by seller Pending!");
      return { ...state, offerBySellerPending: payload };
    },
    [fetchOfferBySellerPending.fulfilled]: (state, { payload }) => {
      console.log("success get offer by seller Pending!");
      return { ...state, offerBySellerPending: payload };
    },
    [fetchOfferBySellerAccepted.rejected]: (state, { payload }) => {
      console.log("failed get offer by seller Accepted!");
      return { ...state, offerBySellerAccepted: payload };
    },
    [fetchOfferBySellerAccepted.pending]: (state, { payload }) => {
      console.log("pending get offer by seller Accepted!");
      return { ...state, offerBySellerAccepted: payload };
    },
    [fetchOfferBySellerAccepted.fulfilled]: (state, { payload }) => {
      console.log("success get offer by seller Accepted!");
      return { ...state, offerBySellerAccepted: payload };
    },
    [fetchOfferBySellerRejected.rejected]: (state, { payload }) => {
      console.log("failed get offer by seller Rejected!");
      return { ...state, offerBySellerRejected: payload };
    },
    [fetchOfferBySellerRejected.pending]: (state, { payload }) => {
      console.log("pending get offer by seller Rejected!");
      return { ...state, offerBySellerRejected: payload };
    },
    [fetchOfferBySellerRejected.fulfilled]: (state, { payload }) => {
      console.log("success get offer by seller Rejected!");
      return { ...state, offerBySellerRejected: payload };
    },
  },
});

export const getAllProductOffer = (state) => state.offer.allProductOffer;
export const getProductOfferById = (state) => state.offer.productOfferById;
export const createProductOfferState = (state) =>
  state.offer.createProductOffer;
export const statusPostOfferState = (state) => state.offer.statusPostOffer;
export const acceptOfferState = (state) => state.offer.acceptOffer;
export const statusAcceptOfferState = (state) => state.offer.statusAcceptOffer;
export const rejectOfferState = (state) => state.offer.rejectOffer;
export const statusRejectOfferState = (state) => state.offer.statusRejectOffer;
export const deleteOfferState = (state) => state.offer.deleteOffer;
export const offerByBidderState = (state) => state.offer.offerByBidder;
export const offerByBidderPendingState = (state) =>
  state.offer.offerByBidderPending;
export const offerByBidderAcceptedState = (state) =>
  state.offer.offerByBidderAccepted;
export const offerByBidderRejectedState = (state) =>
  state.offer.offerByBidderRejected;
export const offerBySellerState = (state) => state.offer.offerBySeller;
export const offerBySellerPendingState = (state) =>
  state.offer.offerBySellerPending;
export const offerBySellerAcceptedState = (state) =>
  state.offer.offerBySellerAccepted;
export const offerBySellerRejectedState = (state) =>
  state.offer.offerBySellerRejected;

export const {
  removeStatusOffer,
  removeStatusAcceptOffer,
  removeStatusRejectOffer,
} = offerSlice.actions;
export default offerSlice.reducer;
