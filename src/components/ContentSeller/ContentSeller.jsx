import React, { useEffect, useState } from "react";
import ButtonCategorySeller from "../Button/ButtonCategory/ButtonCategorySeller";
import CardCategorySeller from "../Card/CardCategorySeller";
import CardProduct from "../Card/CardProduct";
import CardTerjual from "../Card/CardTerjual";
import { BsPlusLg } from "react-icons/bs";
import NullData from "../NullData/NullData";
import { Link, useNavigate } from "react-router-dom";
import CardProdukTrx from "../Card/CardProdukTrx";
import { useDispatch, useSelector } from "react-redux";
import {
  getAllProductsUser,
  getStatusProductUser,
} from "../../redux/productSlice";
import LoadingCard from "../Loading/LoadingCard";
import {
  offerByBidderAcceptedState,
  offerByBidderPendingState,
  offerByBidderRejectedState,
  offerByBidderState,
  offerBySellerAcceptedState,
  offerBySellerPendingState,
  offerBySellerRejectedState,
  offerBySellerState,
} from "../../redux/offerSlice";
import { getWishlist } from "../../redux/wishlist";
import CardYourOffers from "../Card/CardYourOffers";
import {
  allTransactionState,
  transactionBySellerState,
} from "../../redux/transactionSlice";
import DropdownSeller from "../DropdownSeller/DropdownSeller";
import { fetchAsyncProductsUser } from "../../redux/productSlice";
import { fetchMyProfile, getUserProfile } from "../../redux/userSlice";

const ContentSeller = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const AllProductUser = useSelector(getAllProductsUser);
  const statusProductUser = useSelector(getStatusProductUser);
  const statusProduct = useSelector(getStatusProductUser);
  const allProductWishlist = useSelector(getWishlist);
  const allOfferBySeller = useSelector(offerBySellerState);
  const allOfferBySellerPending = useSelector(offerBySellerPendingState);
  const allOfferBySellerAccepted = useSelector(offerBySellerAcceptedState);
  const allOfferBySellerRejected = useSelector(offerBySellerRejectedState);
  const allOfferByBidder = useSelector(offerByBidderState);
  const allOfferByBidderPending = useSelector(offerByBidderPendingState);
  const allOfferByBidderAccepted = useSelector(offerByBidderAcceptedState);
  const allOfferByBidderRejected = useSelector(offerByBidderRejectedState);
  const getAllTransactionState = useSelector(allTransactionState);
  const getAllSold = useSelector(transactionBySellerState);

  const [product, setProduct] = useState(true);
  const [interest, setInterest] = useState(false);
  const [sold, setSold] = useState(false);
  const [trx, setTrx] = useState(false);
  const [offers, setOffers] = useState(false);
  const [filterOffer, setFilterOffer] = useState("all");

  let renderProductUser,
    renderWishlist,
    renderSold,
    renderTransaksi,
    renderOffers = "";

  renderProductUser =
    AllProductUser.message === "success" ? (
      AllProductUser.total_items === 0 ? (
        <div className="grid grid-cols-2 gap-3">
          <div
            className="card-addProduct h-52"
            onClick={(e) => navigate("addproduct")}
          >
            <BsPlusLg />
            <p className="text-xs">Tambah Produk</p>
          </div>
        </div>
      ) : (
        <div className="grid grid-cols-2 sm:grid-cols-3 gap-3 lg:gap-7">
          <div
            className="card-addProduct"
            onClick={(e) => navigate("addproduct")}
          >
            <BsPlusLg />
            <p className="text-xs">Tambah Produk</p>
          </div>
          {AllProductUser.products.rows.map((product, index) => {
            return <CardProduct key={index} data={product} />;
          })}
        </div>
      )
    ) : (
      <div className="grid grid-cols-3 gap-3">
        <LoadingCard />
        <LoadingCard />
        <LoadingCard />
        <LoadingCard />
      </div>
    );

  renderWishlist =
    allProductWishlist.message === "success" ? (
      allProductWishlist.wishlists.length === 0 ? (
        <div className="py-10 lg:py-0">
          <NullData wishlist={true} />
        </div>
      ) : (
        <div className="">
          {allProductWishlist.wishlists.map((product, index) => {
            return <CardProdukTrx key={index} data={product} wishlist={true} />;
          })}
        </div>
      )
    ) : (
      <div className="grid grid-cols-2 gap-3">
        <LoadingCard />
        <LoadingCard />
        <LoadingCard />
        <LoadingCard />
      </div>
    );

  renderSold =
    getAllSold && getAllSold.message === "Transactions found" ? (
      getAllSold.data.length === 0 ? (
        <div className="py-10 lg:py-0">
          <NullData sold={true} />
        </div>
      ) : (
        <div className="">
          {getAllSold.data.map((product, index) => {
            return <CardTerjual key={index} data={product} />;
          })}
        </div>
      )
    ) : (
      <div className="grid grid-cols-2 gap-3">
        <LoadingCard />
        <LoadingCard />
        <LoadingCard />
        <LoadingCard />
      </div>
    );

  renderTransaksi =
    allOfferBySeller && allOfferBySeller.message === "success" ? (
      filterOffer === "all" ? (
        allOfferBySeller.result.disc_products.length !== 0 ? (
          <>
            {allOfferBySeller.result.disc_products.map((product, index) => {
              return <CardProdukTrx key={index} data={product} />;
            })}
          </>
        ) : null
      ) : filterOffer === "pending" ? (
        allOfferBySellerPending.result.disc_products.length !== 0 ? (
          <>
            {allOfferBySellerPending.result.disc_products.map(
              (product, index) => {
                return <CardProdukTrx key={index} data={product} />;
              }
            )}
          </>
        ) : null
      ) : filterOffer === "accepted" ? (
        allOfferBySellerAccepted.result.disc_products.length !== 0 ? (
          <>
            {allOfferBySellerAccepted.result.disc_products.map(
              (product, index) => {
                return <CardProdukTrx key={index} data={product} />;
              }
            )}
          </>
        ) : null
      ) : filterOffer === "rejected" ? (
        allOfferBySellerRejected.result.disc_products.length !== 0 ? (
          <>
            {allOfferBySellerRejected.result.disc_products.map(
              (product, index) => {
                return <CardProdukTrx key={index} data={product} />;
              }
            )}
          </>
        ) : null
      ) : null
    ) : (
      <div className="grid grid-cols-2 gap-3">
        <LoadingCard />
        <LoadingCard />
        <LoadingCard />
        <LoadingCard />
      </div>
    );

  renderOffers =
    allOfferByBidder &&
    allOfferByBidder.message === "success" ? (
      filterOffer === "all" ? (
        allOfferByBidder.result.disc_products.length !== 0 ? (
          <>
            {allOfferByBidder.result.disc_products.map((product, index) => {
              return <CardYourOffers key={index} data={product} />;
            })}
          </>
        ) : null
      ) : filterOffer === "pending" ? (
        allOfferByBidderPending.result.disc_products.length !== 0 ? (
          <>
            {allOfferByBidderPending.result.disc_products.map(
              (product, index) => {
                return <CardYourOffers key={index} data={product} />;
              }
            )}
          </>
        ) : null
      ) : filterOffer === "accepted" ? (
        allOfferByBidderAccepted.result.disc_products.length !== 0 ? (
          <>
            {allOfferByBidderAccepted.result.disc_products.map(
              (product, index) => {
                return <CardYourOffers key={index} data={product} />;
              }
            )}
          </>
        ) : null
      ) : filterOffer === "rejected" ? (
        allOfferByBidderRejected.result.disc_products.length !== 0 ? (
          <>
            {allOfferByBidderRejected.result.disc_products.map(
              (product, index) => {
                return <CardYourOffers key={index} data={product} />;
              }
            )}
          </>
        ) : null
      ) : (
        <div className="grid grid-cols-2 gap-3">
          <LoadingCard />
          <LoadingCard />
          <LoadingCard />
          <LoadingCard />
        </div>
      )
    ) : null;

  return (
    <div
      className="space-y-5 lg:flex lg:space-x-5"
      data-testid="content-seller"
    >
      <nav>
        <CardCategorySeller
          product={product}
          interest={interest}
          sold={sold}
          trx={trx}
          offers={offers}
          setProduct={setProduct}
          setInterest={setInterest}
          setSold={setSold}
          setTrx={setTrx}
          setOffers={setOffers}
        />
        <ButtonCategorySeller
          product={product}
          interest={interest}
          sold={sold}
          trx={trx}
          offers={offers}
          setProduct={setProduct}
          setInterest={setInterest}
          setSold={setSold}
          setTrx={setTrx}
          setOffers={setOffers}
        />
      </nav>
      <section className="px-3 lg:px-0 lg:flex-grow">
        {(product && renderProductUser) ||
          (trx && (
            <>
              <DropdownSeller setFilterOffer={setFilterOffer} />
              {renderTransaksi}
            </>
          )) ||
          (interest && renderWishlist) ||
          (sold && renderSold) ||
          (offers && (
            <>
              <DropdownSeller setFilterOffer={setFilterOffer} />
              {renderOffers}
            </>
          ))}
      </section>
    </div>
  );
};

export default ContentSeller;
