import React from "react";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import gambar from "../../images/jam.png";
import { fetchMyProfile } from "../../redux/userSlice";
import EditDeleteModal from "../Modal/EditDeleteModal";

const CardProduct = ({ data }) => {
  const dispatch = useDispatch();

  const [showEditDeleteModal, setShowEditDeleteModal] = useState(false);
  const userProfile = useSelector((state) => state.user);


  return (
    <>
      {userProfile.userProfileStatus === "success" &&
      userProfile.data.id === data.user_id ? (
        <div className="relative flex flex-col cursor-pointer shadow-lg p-2 rounded-md justify-center transition-all hover:scale-110" >
          <button
            onClick={() => setShowEditDeleteModal(true)}
            className="flex flex-col space-y-2 text-left"
          >
            <img
              src={data.product_images[0].product_images_path}
              alt={data.product_images[0].product_images_name}
              className="rounded-md h-28 lg:h-36 object-contain truncate"
            />
            <h1 className="text-sm text-neutral-5 font-poppins truncate">
              {data.product_name}
            </h1>
            <p className="text-xs text-neutral-3 font-poppins truncate">
              {data.category_product.category_name}
            </p>
            <p className="text-sm text-neutral-5 font-poppins truncate">
              Rp. {data.product_price.toLocaleString("id-ID")}
            </p>
          </button>
          <EditDeleteModal
            showEditDeleteModal={showEditDeleteModal}
            setShowEditDeleteModal={setShowEditDeleteModal}
            data={data}
          />
        </div>
      ) : (
        <Link to={`/detail/${data.slug}`}>
          <div className="flex flex-col space-y-2 cursor-pointer shadow-lg p-2 rounded-md justify-center transition-all hover:scale-110">
            <img
              src={data.product_images[0].product_images_path}
              alt={data.product_images[0].product_images_name}
              className="rounded-md h-28 lg:h-36 object-contain"
            />
            <h1 className="text-sm text-neutral-5 font-poppins truncate">
              {data.product_name}
            </h1>
            <p className="text-xs text-neutral-3 font-poppins truncate">
              {data.category_product.category_name}
            </p>
            <p className="text-sm text-neutral-5 font-poppins truncate">
              Rp. {data.product_price.toLocaleString("id-ID")}
            </p>
          </div>
        </Link>
      )}
    </>
  );
};

export default CardProduct;
