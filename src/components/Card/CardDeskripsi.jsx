import React from "react";

const CardDeskripsi = ({product}) => {
  return (
    <div className="px-5 py-4 shadow-lg rounded-2xl font-poppins space-y-3 border-gray-10 bg-white">
      <h1 className="text-neutral-5 font-medium text-sm">Deskripsi</h1>
      <p className="text-neutral-3 text-sm">
        {product.product.product_desc}
      </p>
    </div>
  );
};

export default CardDeskripsi;
