import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { deleteWishlist, getStatusWishList } from "../../redux/wishlist";
import { BiDetail } from "react-icons/bi";
import { MdLocalOffer, MdDelete, MdOutlineClose } from "react-icons/md";
import { BsList } from "react-icons/bs";
import MenuCardTrx from "../Modal/MenuCardTrx";
import { showModalBargain } from "../../redux/productSlice";

const CardProdukTrx = ({ data, sold, wishlist }) => {
  //   const hargaTawwar = data.harga - 100000;
  const [status, setStatus] = useState();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [open, setOpen] = useState(false);

  const handleDelete = (e) => {
    dispatch(deleteWishlist(data.id));
    setTimeout(() => {
      window.location.reload();
    }, 1000);
  };

  return (
    <div>
      {wishlist ? (
        <div className="relative flex justify-between px-2 py-4 shadow-lg font-poppins rounded-md cursor-pointer">
          <div className="flex space-x-2">
            <img
              src={data.wishlist_product.product_images[0].product_images_path}
              alt="img produk"
              className="w-20 h-20 object-cover rounded-lg sm:w-28 sm:h-28"
            />
            <div className="flex flex-col justify-between text-neutral-5 pt-2">
              <p className="text-sm sm:text-base">
                {data.wishlist_product.product_name}
              </p>
              <p className="text-sm text-neutral-3">
                {data.wishlist_product.category_product.category_name}
              </p>
              <p className="text-sm">
                Rp.{" "}
                {data.wishlist_product.product_price.toLocaleString("id-ID")}
              </p>
            </div>
          </div>

          <div className="relative">
            <button
              className="absolute right-0 text-xl z-20 sm:hidden"
              onClick={(e) => setOpen(!open)}
            >
              {open ? <MdOutlineClose className="text-white" /> : <BsList />}
            </button>
          </div>
          {open && <MenuCardTrx data={data} handleDelete={handleDelete} />}

          <div className="hidden sm:flex flex-col space-y-3 text-neutral-3 pt-2  sm:text-sm">
            <button
              className="flex text-xs items-center py-1 px-3 border-blue-200 border hover:bg-blue-600 hover:text-white rounded-md transition-all duration-300 group"
              onClick={(e) => navigate(`/detail/${data.wishlist_product.slug}`)}
            >
              <BiDetail className="text-lg mr-2 " /> Detail
            </button>
            <button
              className="flex text-xs items-center py-1 px-3 border-red-200 border hover:bg-red-600 hover:text-white rounded-md transition-all duration-300 group"
              onClick={handleDelete}
            >
              <MdDelete className="text-lg mr-2 " /> Delete
            </button>
          </div>
        </div>
      ) : (
        <Link to={`tawaran/${data.id}`}>
          <div className="flex basis-9/12 grow shrink justify-between px-2 py-4 shadow-lg font-poppins rounded-md cursor-pointer transition-all hover:scale-105">
            <div className="flex space-x-2">
              <img
                src={data.product_offered.product_images[0].product_images_path}
                alt="img produk"
                className="w-20 h-20 object-cover rounded-lg sm:w-28 sm:h-28"
              />
              <div className="flex flex-col justify-between text-neutral-5 pt-2">
                <p className="text-sm sm:text-base">
                  {data.product_offered.product_name}
                </p>
                <p className="text-sm text-neutral-3">ga ada kategory</p>
                <p className="text-sm">
                  Rp.{" "}
                  {data.product_offered.product_price.toLocaleString("id-ID")}
                </p>
              </div>
            </div>
            {wishlist ? null : (
              <div className="flex flex-col justify-between text-end text-neutral-5 pt-2">
                {data.status === "rejected" && (
                  <p className="text-xs text-red-700 sm:text-sm">
                    {data.status}
                  </p>
                )}
                {data.status === "pending" && (
                  <p className="text-xs text-yellow-400 sm:text-sm">
                    {data.status}
                  </p>
                )}
                {data.status === "accepted" && (
                  <p className="text-xs text-green-700 sm:text-sm">
                    {data.status}
                  </p>
                )}
                <p className="text-[10px] sm:text-sm">
                  {sold ? "harga deal :" : "harga tawaran :"}
                </p>
                <p className="text-sm font-semibold sm:text-base">
                  Rp. {data.bargain_price.toLocaleString("id-ID")}
                </p>
              </div>
            )}
          </div>
        </Link>
      )}
    </div>
  );
};

export default CardProdukTrx;
