import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { MdOutlineFavoriteBorder, MdFavorite } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import { addWishlist } from "../../redux/wishlist";
import { useEffect } from "react";
import { fetchAsyncProductsUser } from "../../redux/productSlice";
import { fetchMyProfile } from "../../redux/userSlice";
import {
  fetchOfferByBidder,
  offerByBidderAcceptedState,
  offerByBidderPendingState,
  offerByBidderState,
  removeStatusOffer,
} from "../../redux/offerSlice";

const CardCheckOut = ({
  setShowBargainModal,
  preview,
  product,
  wishList,
  handleWishlist,
}) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const userProfile = useSelector((state) => state.user);
  const isAuth = useSelector((state) => state.auth);
  const allOfferByBidderPending = useSelector(offerByBidderPendingState);
  const allOfferByBidderAccepted = useSelector(offerByBidderAcceptedState);

  useEffect(() => {
    dispatch(fetchMyProfile(userProfile));
    dispatch(fetchOfferByBidder());
  }, []);

  const productId = product.product.id;

  const bidderArrPending =
    allOfferByBidderPending &&
    allOfferByBidderPending.message === "success" &&
    allOfferByBidderPending.result.disc_products
      .slice()
      .some((product) => product.product_id === productId);

  const bidderArrAccepted =
    allOfferByBidderAccepted &&
    allOfferByBidderAccepted.message === "success" &&
    allOfferByBidderAccepted.result.disc_products
      .slice()
      .some((product) => product.product_id === productId);

  return (
    <div className="py-4 px-5 shadow-lg rounded-2xl font-poppins space-y-3 border-gray-100 bg-white">
      <p className="text-sm font-medium truncate">
        {product.product.product_name}
      </p>
      <p className="text-xs text-neutral-3">
        {product.product.category_product.category_name}
      </p>
      <p className="text-sm">
        Rp. {product.product.product_price.toLocaleString("id-ID")}
      </p>
      {preview ? (
        <div className="hidden lg:block space-y-3 w-56">
          <button
            className="btn-category-active px-5 py-3 w-full justify-center hover:bg-primary-purple-5"
            onClick={(e) => {
              navigate("/dashboard");
            }}
          >
            Terbitkan
          </button>
          <button
            className="border-[1px] border-primary-purple-4 hover:bg-slate-50 w-full px-4 py-3 text-neutral-4 text-sm flex justify-center rounded-xl"
            onClick={(e) => {
              navigate(-1);
            }}
          >
            Edit
          </button>
        </div>
      ) : (
        <div className="space-y-2 hidden lg:block">
          {userProfile.userProfileStatus === "success" &&
            userProfile.data.id === product.product.user_id && (
              <button
                className="btn-category-active px-5 py-3 w-full justify-center hover:bg-primary-purple-5"
                onClick={(e) => {
                  navigate(`/dashboard/updateproduct/${product.product.slug}`);
                }}
              >
                Edit
              </button>
            )}
          {userProfile.userProfileStatus === "success" &&
            userProfile.data.id !== product.product.user_id &&
            bidderArrPending === false &&
            bidderArrAccepted === false && (
              <button
                className="btn-category-active px-5 py-3 w-full justify-center hover:bg-primary-purple-5"
                onClick={(e) => {
                  if (!isAuth._id) {
                    navigate("/login");
                  }
                  dispatch(removeStatusOffer());
                  setShowBargainModal(true);
                }}
              >
                Saya tertarik dan ingin nego
              </button>
            )}
          {userProfile.userProfileStatus === "success" &&
            userProfile.data.id !== product.product.user_id &&
            bidderArrPending === true && (
              <button
                className="btn-category-active px-5 py-3 w-full justify-center bg-gray-400"
                disabled
              >
                Menunggu respon penjual
              </button>
            )}
          {userProfile &&
            userProfile.userProfileStatus === "success" &&
            userProfile.data.id !== product.product.user_id &&
            bidderArrAccepted === true && (
              <button
                className="btn-category-active px-5 py-3 w-full justify-center bg-yellow-400"
                disabled
              >
                Tawaran diterima
              </button>
            )}
          <button
            className="transition-colors px-4 md:px-5 py-3 text-neutral-4 bg-white text-sm flex items-center justify-between w-full font-poppins rounded-xl border-2"
            onClick={(e) => handleWishlist(e)}
          >
            Simpan ke wishlist
            {wishList ? (
              <MdFavorite className="text-red-500" />
            ) : (
              <MdOutlineFavoriteBorder />
            )}
          </button>
        </div>
      )}
    </div>
  );
};

export default CardCheckOut;
