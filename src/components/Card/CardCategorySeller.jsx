import React from "react";
import { FaCube, FaRegHeart, FaDollarSign, FaAngleRight } from "react-icons/fa";
import { GrTransaction } from "react-icons/gr";
import { MdOutlineLocalOffer } from "react-icons/md";

const CardCategorySeller = ({
  product,
  interest,
  sold,
  trx,
  offers,
  setProduct,
  setInterest,
  setSold,
  setTrx,
  setOffers
}) => {
  return (
    <>
      <div className="m-3 p-3 border w-max hidden lg:block">
        <h3 className="text-lg font-bold">Kategori</h3>
        <ul className="w-full rounded-lg mt-2 mb-3">
          <li className="mb-1  border-b">
            <a
              href="#"
              className={
                product
                  ? "w-fill flex p-3 hover:text-primary-purple-4 text-primary-purple-4"
                  : "w-fill flex p-3 hover:text-primary-purple-4"
              }
              onClick={(e) => {
                setProduct(true);
                setInterest(false);
                setSold(false);
                setTrx(false);
                setOffers(false)
              }}
            >
              <FaCube className="w-5 h-full" />
              <p className="ml-2 truncate flex">Semua Produk</p>
              <FaAngleRight className="w-5 h-full ml-auto mr-0" />
            </a>
          </li>
          <li className="mb-1 border-b">
            <a
              href="#"
              className={
                interest
                  ? "w-fill flex p-3 hover:text-primary-purple-4 text-primary-purple-4"
                  : "w-fill flex p-3 hover:text-primary-purple-4"
              }
              onClick={(e) => {
                setInterest(true);
                setProduct(false);
                setSold(false);
                setTrx(false);
                setOffers(false)
              }}
            >
              <FaRegHeart className="w-5 h-full" />
              <p className="ml-2 truncate flex">Diminati</p>
              <FaAngleRight className="w-5 h-full ml-auto mr-0" />
            </a>
          </li>
          <li className="mb-1  border-b">
            <a
              href="#"
              className={
                sold
                  ? "w-fill flex p-3 hover:text-primary-purple-4 text-primary-purple-4"
                  : "w-fill flex p-3 hover:text-primary-purple-4"
              }
              onClick={(e) => {
                setSold(true);
                setInterest(false);
                setProduct(false);
                setTrx(false);
                setOffers(false)
              }}
            >
              <FaDollarSign className="w-5 h-full" />
              <p className="ml-2 truncate flex">Terjual</p>
              <FaAngleRight className="w-5 h-full ml-auto mr-0" />
            </a>
          </li>
          <li className="">
            <a
              href="#"
              className={
                trx
                  ? "w-fill flex p-3 hover:text-primary-purple-4 text-primary-purple-4"
                  : "w-fill flex p-3 hover:text-primary-purple-4"
              }
              onClick={(e) => {
                setTrx(true);
                setSold(false);
                setInterest(false);
                setProduct(false);
                setOffers(false)
              }}
            >
              <GrTransaction className="w-5 h-full" />
              <p className="ml-2 truncate flex">Iklanmu</p>
              <FaAngleRight className="w-5 h-full ml-auto mr-0" />
            </a>
          </li>
          <li className="">
            <a
              href="#"
              className={
                offers
                  ? "w-fill flex p-3 hover:text-primary-purple-4 text-primary-purple-4"
                  : "w-fill flex p-3 hover:text-primary-purple-4"
              }
              onClick={(e) => {
                setOffers(true)
                setTrx(false);
                setSold(false);
                setInterest(false);
                setProduct(false);
              }}
            >
              <MdOutlineLocalOffer className="w-5 h-full" />
              <p className="ml-2 truncate flex">Tawaranmu</p>
              <FaAngleRight className="w-5 h-full ml-auto mr-0" />
            </a>
          </li>
        </ul>
      </div>
    </>
  );
};

export default CardCategorySeller;
