import React from "react";
import { FiCamera } from "react-icons/fi";
import { useNavigate } from "react-router-dom";
import avatar from "../../images/sasuke.jpg";
import productCarousel from "../ProductCarousel/ProductCarousel";

const CardSeller = ({ btnEdit, product, data }) => {

  const navigate = useNavigate();
  return (
    <div className="flex justify-between py-4 px-5 h-fit shadow-lg rounded-2xl font-poppin border-gray-10 items-center bg-white font-poppins">
      <div className="flex">
        {data && data.data.profile_picture_path === null ||
        product && product.product.seller.profile_picture_path === null ? (
          <div className="bg-primary-purple-1 py-3 px-5 rounded-2xl w-full self-center">
            <p className="text-primary-purple-4">{data && data.data.name.charAt(0) || product && product.product.seller.name.charAt(0)} </p>
          </div>
        ) : (
          <img
            src={
              data
                ? data.data.profile_picture_path
                : product.product.seller.profile_picture_path
            }
            alt="avatar"
            className="w-12 h-12 object-contain rounded-xl truncate"
          />
        )}

        <div className="space-y-2 ml-2 max-w-[190px]">
          {product && (
            <p className="text-sm font-medium truncate">
              {product.product.seller.name}{" "}
            </p>
          )}
          {data && (
            <p className="text-sm font-medium truncate">{data.data.name} </p>
          )}
          {product && (
            <p className="text-sm font-medium truncate">
              {product.product.seller.address}{" "}
            </p>
          )}
          {data && (
            <p className="text-sm font-medium truncate">{data.data.address} </p>
          )}
        </div>
      </div>
      {btnEdit && (
        <button
          className="border-primary-purple-2 border-2 hover:bg-primary-purple-3 hover:text-white text-xs text-neutral-5 py-1 px-3 rounded-lg"
          onClick={(e) => navigate("/profile-edit")}
        >
          Edit
        </button>
      )}
    </div>
  );
};

export default CardSeller;
