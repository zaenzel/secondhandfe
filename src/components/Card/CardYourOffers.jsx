import React from "react";
import { Link } from "react-router-dom";

const CardYourOffers = ({ data }) => {
  return (
    <div>
      {/* <Link to={`/detail/${data.product_offered.slug}`}> */}
        <div className="flex basis-9/12 grow shrink justify-between px-2 py-4 shadow-lg font-poppins rounded-md transition-all hover:scale-105">
          <div className="flex space-x-2">
            <img
              src={data.product_offered.product_images[0].product_images_path}
              alt="img produk"
              className="w-20 h-20 object-cover rounded-lg sm:w-28 sm:h-28"
            />
            <div className="flex flex-col justify-between text-neutral-5 pt-2">
              <p className="text-sm sm:text-base">
                {data.product_offered.product_name}
              </p>
              <p className="text-sm text-neutral-3">kategory</p>
              <p className="text-sm">
                Rp. {data.product_offered.product_price.toLocaleString("id-ID")}
              </p>
            </div>
          </div>
          <div className="flex flex-col justify-between text-end text-neutral-5 pt-2">
            <p className="text-xs text-gray-400 sm:text-sm">
              {data.status === "rejected" && (
                <p className="text-xs text-red-700 sm:text-sm">{data.status}</p>
              )}
              {data.status === "pending" && (
                <p className="text-xs text-yellow-400 sm:text-sm">
                  {data.status}
                </p>
              )}
              {data.status === "accepted" && (
                <p className="text-xs text-green-700 sm:text-sm">
                  {data.status}
                </p>
              )}
            </p>
            <p className="text-[10px] sm:text-sm">harga tawaranmu</p>
            <p className="text-sm font-semibold sm:text-base">
              Rp. {data.bargain_price.toLocaleString("id-ID")}
            </p>
          </div>
        </div>
      {/* </Link> */}
    </div>
  );
};

export default CardYourOffers;
