import React, { useState } from "react";
import { Link } from "react-router-dom";
import jam from "../../images/jam.png";

const CardTerjual = ({ data }) => {
  const [status, setStatus] = useState();
  //

  return (
    <div>
      <Link to={``}>
        <div className="flex justify-between px-2 py-4 shadow-lg rounded-md cursor-pointer transition-all hover:scale-105">
          <div className="flex space-x-2">
            <img
              src={
                data.order_transaction_product.product_images[0]
                  .product_images_path
              }
              alt="img produk"
              className="w-20 h-20 object-cover rounded-lg sm:w-28 sm:h-28"
            />
            <div className="flex flex-col justify-between text-neutral-5 pt-2">
              <p className="text-sm sm:text-base">
                {data.order_transaction_product.product_name}
              </p>
              <p className="text-sm text-neutral-3">
                {data.order_transaction_product.category_product.category_name}
              </p>
              <p className="text-sm truncate">
                {data.order_transaction_user.name}
              </p>
            </div>
          </div>
          <div className="flex flex-col justify-between text-end text-neutral-5 pt-2">
            <p className="text-xs sm:text-sm text-green-500">
              {data.order_transaction_product.status}
            </p>
            <p className="text-xsx sm:text-sm">harga deal :</p>
            <p className="text-xs font-semibold sm:text-base">
              Rp.{" "}
              {data.order_transaction_product.product_price.toLocaleString(
                "id-ID"
              )}
            </p>
          </div>
        </div>
      </Link>
    </div>
  );
};

export default CardTerjual;
