import React from "react";
import { Link } from "react-router-dom";
import gambar from "../../images/jam.png";

const CardSellerNew = ({ data }) => {
  return (
    <Link to={``}>
      <div className="flex flex-col space-y-2 cursor-pointer shadow-md p-2 rounded-md justify-center transition-all hover:scale-110">
        <img
          src={`https://secondhandbe.herokuapp.com/public/images/products/${data.product_images[0].product_images_name}`}
          alt={data.product_images[0].product_images_name}
          className="rounded-md h-28 lg:h-36 object-contain"
        />
        <h1 className="text-sm text-neutral-5 font-poppins truncate">
          {data.product_name}
        </h1>
        <p className="text-xs text-neutral-3 font-poppins truncate">
          {data.category_product.category_name}
        </p>
        <p className="text-sm text-neutral-5 font-poppins truncate">
          Rp. {data.product_price.toLocaleString("id-ID")}
        </p>
      </div>
    </Link>
  );
};

export default CardSellerNew;
