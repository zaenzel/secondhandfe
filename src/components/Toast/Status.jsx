import React from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteProductState,
  getdeleteStatusProduct,
  getStatusProduct,
  getStatusUpdateProduct,
  removeStatus,
} from "../../redux/productSlice";
import { getStatusWishList, removeStatusWishlist } from "../../redux/wishlist";

const Status = ({
  wishList,
  remove,
  deleteWishList,
  deleteProduct,
  addProduct,
  updateProduct,
}) => {
  const dispatch = useDispatch();
  setTimeout(() => {
    dispatch(removeStatus());
    dispatch(removeStatusWishlist());
  }, 2500);

  return (
    <div
      className={
        remove
          ? "absolute py-2 px-10 rounded-lg z-30 left-1/2 top-20 -translate-x-1/2 text-center text-sm text-white bg-red-400"
          : "absolute py-2 px-10 rounded-lg z-30 left-1/2 top-20 -translate-x-1/2 text-center text-sm text-white bg-green-400"
      }
      data-testid="status-toast"
    >
      {(wishList && "Berhasil ditambahkan ke wishlist") ||
        (deleteWishList && "Berhasil dihapus dari wishlist") ||
        (deleteProduct && "Produk berhasil dihapus") ||
        (addProduct && "Produk berhasil diterbitkan") ||
        (updateProduct && "Produk berhasil di edit")}
      {/* {(statusAdd.message === "success" && "Produk berhasil diterbitkan") ||
        (statusUpdate.message === "success" && "Produk berhasil diedit") ||
        (deleteProduct.message === "success" && "Produk berhasil dihapus") ||
        (statusWishlist && "Berhasil ditambahkan ke wishlist")} */}
    </div>
  );
};

export default Status;
