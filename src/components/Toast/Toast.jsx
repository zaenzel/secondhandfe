import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  createProductOfferState,
  fetchOfferByBidder,
  fetchOfferByBidderAccepted,
  fetchOfferByBidderPending,
  fetchOfferByBidderRejected,
  fetchOfferBySeller,
  fetchOfferBySellerAccepted,
  fetchOfferBySellerPending,
  fetchOfferBySellerRejected,
  statusPostOfferState,
} from "../../redux/offerSlice";
import { getTransactionBySeller } from "../../redux/transactionSlice";

const Toast = ({ product, setShowBargainModal }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const postOfferRes = useSelector(createProductOfferState);
  const statusPostOffer = useSelector(statusPostOfferState);
  const [showToast, setShowToast] = useState(true);

  setTimeout(() => {
    setShowBargainModal(false);
    if (statusPostOffer === "success") {
      // navigate(`/detail/${product.product.slug}`)
      dispatch(fetchOfferByBidder());
      dispatch(fetchOfferByBidderAccepted());
      dispatch(fetchOfferByBidderPending());
      dispatch(fetchOfferByBidderRejected());
      dispatch(fetchOfferBySeller());
      dispatch(fetchOfferBySellerAccepted());
      dispatch(fetchOfferBySellerPending());
      dispatch(fetchOfferBySellerRejected());
      dispatch(getTransactionBySeller());
      navigate("/dashboard");
    }
    setShowToast(false);
  }, 2000);

  return (
    <>
      {showToast ? (
        <div className="flex justify-center mt-3">
          {statusPostOffer === "rejected" && (
            <div
              class="bg-yellow-500 fixed z-50 shadow-lg mx-auto max-w-full text-sm pointer-events-auto bg-clip-padding rounded-lg text-center block py-2 px-4 mb-3"
              id="static-example"
              role="alert"
              aria-live="assertive"
              aria-atomic="true"
              data-mdb-autohide="false"
            >
              <p class="font-bold text-white flex items-center text-center">
                <svg
                  aria-hidden="true"
                  focusable="false"
                  data-prefix="fas"
                  data-icon="exclamation-triangle"
                  class="w-4 h-4 mr-2 fill-current"
                  role="img"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 576 512"
                >
                  <path
                    fill="currentColor"
                    d="M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"
                  ></path>
                </svg>
                {postOfferRes.message}
              </p>
            </div>
          )}
          {statusPostOffer === "success" && (
            <div
              class="bg-green-500 fixed z-50 shadow-lg mx-auto max-w-full text-sm pointer-events-auto bg-clip-padding rounded-lg text-center block py-2 px-4 mb-3"
              id="static-example"
              role="alert"
              aria-live="assertive"
              aria-atomic="true"
              data-mdb-autohide="false"
            >
              <p class="font-bold text-white flex items-center">
                <svg
                  aria-hidden="true"
                  focusable="false"
                  data-prefix="fas"
                  data-icon="exclamation-triangle"
                  class="w-4 h-4 mr-2 fill-current"
                  role="img"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 576 512"
                >
                  <path
                    fill="currentColor"
                    d="M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"
                  ></path>
                </svg>
                Harga tawarmu berhasil dikirimkan ke penjual
              </p>
            </div>
          )}
        </div>
      ) : null}
    </>
  );
};

export default Toast;
