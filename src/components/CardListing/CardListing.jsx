import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import CardProduct from "../Card/CardProduct";
import ButtonCategory from "../Button/ButtonCategory/ButtonCategory";
import {
  fetchAsyncProducts,
  getAllProducts,
  getProductBaju,
  getProductElektronik,
  getProductHobi,
  getProductKendaraan,
  getProductKesehatan,
  productByBaju,
  productByElektronik,
  productByHobi,
  productByKendaraan,
  productByKesehatan,
} from "../../redux/productSlice";
import LoadingCard from "../Loading/LoadingCard";
import { MdSkipPrevious } from "react-icons/md";

const CardListing = () => {
  const dispatch = useDispatch();
  const AllProducts = useSelector(getAllProducts);
  const ProductByHobi = useSelector(getProductHobi);
  const ProductByKendaraan = useSelector(getProductKendaraan);
  const ProductByBaju = useSelector(getProductBaju);
  const ProductByElektronik = useSelector(getProductElektronik);
  const ProductByKesehatan = useSelector(getProductKesehatan);

  const [currentPageAll, setCurrentPageAll] = useState(1);
  const [currentPageHobi, setCurrentPageHobi] = useState(1);
  const [currentPageKendaraan, setCurrentPageKendaraan] = useState(1);
  const [currentPageBaju, setCurrentPageBaju] = useState(1);
  const [currentPageElektronik, setCurrentPageElektronik] = useState(1);
  const [currentPageKesehatan, setCurrentPageKesehatan] = useState(1);

  const [semua, setSemua] = useState(true);
  const [hobi, setHobi] = useState(false);
  const [kendaraan, setKendaraan] = useState(false);
  const [baju, setBaju] = useState(false);
  const [elektronik, setElektronik] = useState(false);
  const [kesehatan, setKesehatan] = useState(false);
  const [productPage, setProductPage] = useState({
    filter: "",
    page: 1,
  });

  let renderAll,
    renderHobi,
    renderKendaraan,
    renderBaju,
    renderElektronik,
    renderKesehatan = "";

  const handleNext = () => {
    if (semua === true) {
      dispatch(fetchAsyncProducts(currentPageAll + 1));
      setCurrentPageAll(currentPageAll + 1);
    }
    if (hobi === true) {
      dispatch(productByHobi(currentPageHobi + 1));
      setCurrentPageHobi(currentPageHobi + 1);
    }
    if (kendaraan === true) {
      dispatch(productByKendaraan(currentPageKendaraan + 1));
      setCurrentPageKendaraan(currentPageKendaraan + 1);
    }
    if (baju === true) {
      dispatch(productByBaju(currentPageBaju + 1));
      setCurrentPageBaju(currentPageBaju + 1);
    }
    if (elektronik === true) {
      dispatch(productByElektronik(currentPageElektronik + 1));
      setCurrentPageElektronik(currentPageElektronik + 1);
    }
    if (kesehatan === true) {
      dispatch(productByKesehatan(currentPageKesehatan + 1));
      setCurrentPageKesehatan(currentPageKesehatan + 1);
    }
  };

  const handlePrev = () => {
    if (semua === true) {
      dispatch(fetchAsyncProducts(currentPageAll - 1));
      setCurrentPageAll(currentPageAll - 1);
    }
    if (hobi === true) {
      dispatch(productByHobi(currentPageHobi - 1));
      setCurrentPageHobi(currentPageHobi - 1);
    }
    if (kendaraan === true) {
      dispatch(productByKendaraan(currentPageKendaraan - 1));
      setCurrentPageKendaraan(currentPageKendaraan - 1);
    }
    if (baju === true) {
      dispatch(productByBaju(currentPageBaju - 1));
      setCurrentPageBaju(currentPageBaju - 1);
    }
    if (elektronik === true) {
      dispatch(productByElektronik(currentPageElektronik - 1));
      setCurrentPageElektronik(currentPageElektronik - 1);
    }
    if (kesehatan === true) {
      dispatch(productByKesehatan(currentPageKesehatan - 1));
      setCurrentPageKesehatan(currentPageKesehatan - 1);
    }
  };

  renderAll =
    AllProducts.message === "success" ? (
      AllProducts.total_items > 0 ? (
        AllProducts.products.rows.map((product, index) => {
          return <CardProduct key={index} data={product} />;
        })
      ) : (
        <h1>Belum ada barang</h1>
      )
    ) : (
      <>
        <LoadingCard />
        <LoadingCard />
        <LoadingCard />
        <LoadingCard />
        <LoadingCard />
        <LoadingCard />
        <LoadingCard />
        <LoadingCard />
        <LoadingCard />
      </>
    );

  renderHobi =
    ProductByHobi.message === "success" ? (
      ProductByHobi.total_items > 0 ? (
        ProductByHobi.products.rows.map((product, index) => {
          return <CardProduct key={index} data={product} />;
        })
      ) : (
        <h1>Belum ada barang</h1>
      )
    ) : (
      <LoadingCard />
    );

  renderKendaraan =
    ProductByKendaraan.message === "success" ? (
      ProductByKendaraan.total_items > 0 ? (
        ProductByKendaraan.products.rows.map((product, index) => {
          return <CardProduct key={index} data={product} />;
        })
      ) : (
        <h1>Belum ada barang</h1>
      )
    ) : (
      <LoadingCard />
    );

  renderBaju =
    ProductByBaju.message === "success" ? (
      ProductByBaju.total_items > 0 ? (
        ProductByBaju.products.rows.map((product, index) => {
          return <CardProduct key={index} data={product} />;
        })
      ) : (
        <h1>Belum ada barang</h1>
      )
    ) : (
      <LoadingCard />
    );

  renderElektronik =
    ProductByElektronik.message === "success" ? (
      ProductByElektronik.total_items > 0 ? (
        ProductByElektronik.products.rows.map((product, index) => {
          return <CardProduct key={index} data={product} />;
        })
      ) : (
        <h1>Belum ada barang</h1>
      )
    ) : (
      <LoadingCard />
    );

  renderKesehatan =
    ProductByKesehatan.message === "success" ? (
      ProductByKesehatan.total_items > 0 ? (
        ProductByKesehatan.products.rows.map((product, index) => {
          return <CardProduct key={index} data={product} />;
        })
      ) : (
        <h1>Belum ada barang</h1>
      )
    ) : (
      <LoadingCard />
    );

  return (
    <div className="space-y-10" data-testid="card-listing">
      <div className="space-y-3">
        <h1 className="text-sm text-neutral-5 font-bold px-3">
          Telusuri Kategori
        </h1>
        <ButtonCategory
          semua={semua}
          setSemua={setSemua}
          hobi={hobi}
          setHobi={setHobi}
          kendaraan={kendaraan}
          setKendaraan={setKendaraan}
          baju={baju}
          setBaju={setBaju}
          elektronik={elektronik}
          setElektronik={setElektronik}
          kesehatan={kesehatan}
          setKesehatan={setKesehatan}
        />
      </div>

      {/* flex flex-wrap justify-center items-center lg:justify-start gap-2 lg:gap-5 */}

      <div>
        <div className="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 xl:grid-cols-6 gap-3 px-3">
          {(semua && renderAll) ||
            (hobi && renderHobi) ||
            (kendaraan && renderKendaraan) ||
            (baju && renderBaju) ||
            (elektronik && renderElektronik) ||
            (kesehatan && renderKesehatan)}
        </div>

        <div className="container mx-auto flex justify-end items-center space-x-5 -translate-x-3 mt-10 md:max-w-screen-md lg:max-w-screen-lg text-neutral-4">
          <button
            className={
              (semua && currentPageAll === 1 && "hidden") ||
              (hobi && currentPageHobi === 1 && "hidden") ||
              (kendaraan && currentPageKendaraan === 1 && "hidden") ||
              (baju && currentPageBaju === 1 && "hidden") ||
              (elektronik && currentPageElektronik === 1 && "hidden") ||
              (kesehatan && currentPageKesehatan === 1 && "hidden")
            }
            onClick={handlePrev}
          >
            <MdSkipPrevious className="text-2xl" />
          </button>
          <div className="flex space-x-2 font-poppins text-sm items-center justify-center">
            {/* current page */}
            <p>Page</p>
            <p className="px-2 py-1 border-2 border-primary-purple-4 rounded-md mr-1 text-primary-purple-3">
              {semua && currentPageAll}
              {hobi && currentPageHobi}
              {kendaraan && currentPageKendaraan}
              {baju && currentPageBaju}
              {elektronik && currentPageElektronik}
              {kesehatan && currentPageKesehatan}
            </p>
            {/* point */}
            <p className="text-sm">of</p>
            {/* total page */}
            <p className="px-2 py-1">
              {semua && AllProducts.total_pages}
              {hobi && ProductByHobi.total_pages}
              {kendaraan && ProductByKendaraan.total_pages}
              {baju && ProductByBaju.total_pages}
              {elektronik && ProductByElektronik.total_pages}
              {kesehatan && ProductByKesehatan.total_pages}
            </p>
          </div>
          <button className="text-3xl rotate-180" onClick={handleNext}>
            <MdSkipPrevious
              className={
                (semua &&
                  currentPageAll === AllProducts.total_pages &&
                  "hidden") ||
                (hobi &&
                  currentPageHobi === ProductByHobi.total_pages &&
                  "hidden") ||
                (kendaraan &&
                  currentPageKendaraan === ProductByKendaraan.total_pages &&
                  "hidden") ||
                (baju &&
                  currentPageBaju === ProductByBaju.total_pages &&
                  "hidden") ||
                (elektronik &&
                  currentPageElektronik === ProductByElektronik.total_pages &&
                  "hidden") ||
                (kesehatan &&
                  currentPageKesehatan === ProductByKesehatan.total_pages &&
                  "hidden")
              }
            />
          </button>
        </div>
      </div>
    </div>
  );
};

export default CardListing;
