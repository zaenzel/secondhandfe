import React from "react";
import nullImg from "../../images/null.png";

const NullData = ({ wishlist, sold }) => {
  return (
    <div className="flex flex-col justify-center items-center space-y-5">
      <img
        src={nullImg}
        alt="gambar null data"
        className="h-32 object-contain"
      />
      <p className="text-sm text-neutral-3 text-center px-5 max-w-[18rem]">
        {wishlist
          && "Belom ada barang yang masuk ke wishlist"
          ||
          sold && "Belom ada barang yang terjual nih, sabar ya innallaha ma`ashobirin"}
      </p>
    </div>
  );
};

export default NullData;
