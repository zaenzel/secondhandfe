import React from 'react'

const Description = () => {
  return (
    <div className='border mt-8 p-8 w-1/2'>
      <h1 className='font-bold'>deskripsi</h1>
      <p className='text-left mb-8'>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Architecto amet, optio inventore provident debitis ad ullam voluptas enim quisquam perferendis, aperiam quas blanditiis ea cum culpa fugit id molestiae maiores. Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos, sint praesentium? Sint corrupti non magni excepturi libero, quae dolor, optio rerum debitis repudiandae sapiente itaque vel ipsa iure soluta! Pariatur! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Alias, quam dolorum? Nam pariatur ea sint mollitia illum quod animi accusantium unde hic rem. Voluptatem quos tempore id a, enim quidem?</p>
      <p className='text-left'>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Architecto amet, optio inventore provident debitis ad ullam voluptas enim quisquam perferendis, aperiam quas blanditiis ea cum culpa fugit id molestiae maiores. Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos, sint praesentium? Sint corrupti non magni excepturi libero, quae dolor, optio rerum debitis repudiandae sapiente itaque vel ipsa iure soluta! Pariatur! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Alias, quam dolorum? Nam pariatur ea sint mollitia illum quod animi accusantium unde hic rem. Voluptatem quos tempore id a, enim quidem?</p>
    </div>
  )
}

export default Description