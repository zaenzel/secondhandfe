import React, { useState } from "react";

const LoadingScreen = ({done}) => {
  const [style, setStyle] = useState({});
  setTimeout(() => {
    const newStyle = {
      opacity: 1,
      width: `${done}%`,
    };

    setStyle(newStyle);
  }, 300);

  return (
    <div className="flex justify-center items-center w-screen h-screen" data-testid="loading-screen">
      <div className="space-y-7 flex flex-col items-center">
        <h1>Loading</h1>
        <div className="border-2 rounded-full bg-gray-100 w-64 h-6 position:relative">
          <div
            className="bg-indigo-600 shadow-md rounded-full text-white flex items-center justify-center h-full transition-all opacity-0 w-0"
            style={style}
          >
            {done}%
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoadingScreen;
