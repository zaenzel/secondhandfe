import React from "react";

const LoadingCard = () => {
  return (
    <div className="animate-pulse flex flex-col space-y-2 shadow-md border-2 p-2 rounded-md justify-center" data-testid="loading-card">
      <img
        src=""
        alt=""
        className="rounded-md h-28 lg:h-36 object-contain bg-slate-200"
      />
      <div className="text-sm text-neutral-5 font-poppins truncate bg-slate-200 w-full h-4"></div>
      <div className="text-xs text-neutral-3 font-poppins truncate bg-slate-200 w-6 h-2"></div>
      <div className="text-sm text-neutral-5 font-poppins truncate bg-slate-200 w-24 h-3"></div>
    </div>
  );
};

export default LoadingCard;
