import React from "react";
import Carousel from "better-react-carousel";
const CarouselDetail = ({ product }) => {
  const arrImg = product.product.product_images

  return (
    <div>
      <Carousel cols={1} rows={1} gap={5} loop>
        {arrImg.map((e, i) => {
          return (
            <Carousel.Item key={i} data-testid="carousel-detail">
              <img
                className="w-full min-h-72 max-h-80 lg:rounded-2xl object-contain lg:object-cover"
                src={e.product_images_path}
                alt={e.product_images_name}
              />
            </Carousel.Item>
          );
        })}
      </Carousel>
    </div>
  );
};

export default CarouselDetail;
