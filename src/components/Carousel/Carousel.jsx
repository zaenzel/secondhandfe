import { React, useEffect, useState } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "../../styles/carousel.css";

const Carousel = () => {
  //if using API uncomment this
  // const [dataMobil, setDataMobil] = useState([])

  //   useEffect(() => {
  //       axios.get('https://6259265392dc8873186a0356.mockapi.io/api/binar/cars', {})
  //       .then((res) => {
  //           setDataMobil(res.data)
  //       })
  //   }, [])
  var settings = {
    adaptiveHeight: true,
    variableWidth: true,
    infinite: true,
    focusOnSelect: true,
    centerMode: true,
    slidesToShow: 1.5,
    slidesToScroll: 1,
    arrows: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          dots: false,
        },
      },
      {
        breakpoint: 913,
        settings: {
          // adaptiveHeight: false,
          // variableWidth: false,
          className: "w-screen",
          slidesToShow: 1,
          dots: true,
        },
      },
      {
        breakpoint: 541,
        settings: {
          // adaptiveHeight: false,
          className: "mt-8",
          variableWidth: false,
          slidesToShow: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          className: "mt-4 h-16",
          adaptiveHeight: false,
          variableWidth: false,
          slidesToShow: 1,
        },
      },
    ],
  };
  return (
    <>
      {/* if using API uncomment this}
            {/* <Slider {...settings}>
              {dataMobil.map((item) => {
                return(
                  <div className='mt-32'>
                    <img src={item.image} alt="" className='object-cover h-64 w-122'/>
                  </div>
                )
              })}
                </Slider> */}

      <div className="w-screen md:mt-16 hidden sm:block" data-testid="carousel">
        <Slider {...settings}>
          <div className="slider__item">
            <img src="./images/img_banner.png" alt=""/>
          </div>
          <div className="slider__item">
            <img src="./images/img_banner.png" alt="" />
          </div>
          <div className="slider__item">
            <img src="./images/img_banner.png" alt="" />
          </div>
          <div className="slider__item">
            <img src="./images/img_banner.png" alt="" />
          </div>
          <div className="slider__item">
            <img src="./images/img_banner.png" alt="" />
          </div>
          <div className="slider__item">
            <img src="./images/img_banner.png" alt="" />
          </div>
        </Slider>
      </div>

      <section className="relative sm:hidden">
        <div className="h-auto">
          <img src="./images/banner-mobile.png" alt="" className="w-screen"/>
        </div>
      </section>
    </>
  );
};

export default Carousel;
