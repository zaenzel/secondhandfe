import React, { useEffect } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { GrClose } from "react-icons/gr";
import "./EditDeleteModal.css";
import { MdOutlineDelete } from "react-icons/md";
import { BiEditAlt, BiShow } from "react-icons/bi";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteAsyncProduct,
  getdeleteStatusProduct,
  getStatusProduct,
} from "../../redux/productSlice";

const EditDeleteModal = ({
  showEditDeleteModal,
  setShowEditDeleteModal,
  data,
}) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const deleteResponse = useSelector(getdeleteStatusProduct);
  const status = useSelector(getStatusProduct);

  const handleDelete = (e) => {
    e.preventDefault();
    dispatch(deleteAsyncProduct(data.slug));
    setShowEditDeleteModal(false);
  };
  if (status === "deleteProduct") {
    window.location.reload();
  }
  
  return (
    <>
      {showEditDeleteModal === true ? (
        <>
          <div className="absolute p-3 flex justify-center bg-white top-0 left-0 right-0 rounded-md w-full h-full">
            <div className="flex justify-center items-center bg-white rounded-md w-full">
              <div className="w-full">
                <Link to={`/detail/${data.slug}`}>
                  <div className="flex p-3 space-x-5 cursor-pointer hover:rounded-md hover:bg-primary-purple-1">
                    <BiShow className="text-primary-purple-4" />
                    <div className="text-start space-y-1">
                      <p className="text-sm font-semibold text-neutral-5">
                        View
                      </p>
                    </div>
                  </div>
                </Link>
                <Link to={`/dashboard/updateproduct/${data.slug}`}>
                  <div className="flex p-3 space-x-5 cursor-pointer hover:rounded-md hover:bg-primary-purple-1">
                    <BiEditAlt className="text-primary-purple-4" />
                    <div className="text-start space-y-1">
                      <p className="text-sm font-semibold text-neutral-5">
                        Edit
                      </p>
                    </div>
                  </div>
                </Link>
                <div
                  onClick={handleDelete}
                  className="flex p-3 space-x-5 cursor-pointer hover:rounded-md hover:bg-primary-purple-1"
                >
                  <MdOutlineDelete className="text-primary-purple-4" />
                  <div className="text-start space-y-1">
                    <p className="text-sm font-semibold text-neutral-5">
                      {deleteResponse === "pending" ? "Loading.." : "Delete"}
                    </p>
                  </div>
                </div>
                <div
                  onClick={() => {
                    setShowEditDeleteModal(false);
                  }}
                  className="flex justify-center mx-3 p-2 mt-2 space-x-5 cursor-pointer rounded-md hover:rounded-md text-primary-purple-4 border border-primary-purple-4"
                >
                  <div className="space-y-1">
                    <p className="text-sm text-center font-semibold text-primary-purple-4">
                      Cancel
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : null}
    </>
  );
};

export default EditDeleteModal;
