import React, { useState } from "react";
import BargainModal from "./BargainModal";
import DealModal from "./DealModal";
import StatusModal from "./StatusModal";
import EditDeleteModal from "./EditDeleteModal";

const ToggleModal = () => {
  const [showBargainModal, setShowBargainModal] = useState(false);
  const [showDealModal, setShowDealModal] = useState(false);
  const [showStatusModal, setShowStatusModal] = useState(false);
  const [showEditDeleteModal, setShowEditDeleteModal] = useState(false);
  return (
    <>
      {/* BARGAIN MODAL */}
      <div>
        {/* button toggle modals */}
        <button
          className="bg-primary-purple-4 text-white active:bg-pink-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
          type="button"
          onClick={() => setShowBargainModal(true)}
        >
          show bargain modal
        </button>
        {/* card show modals*/}
        <BargainModal
          showBargainModal={showBargainModal}
          setShowBargainModal={setShowBargainModal}
        />
      </div>

      {/* DEAL MODAL */}
      <div>
        {/* button toggle modals */}
        <button
          className="bg-primary-purple-4 text-white active:bg-pink-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
          type="button"
          onClick={() => setShowDealModal(true)}
        >
          show deal modal
        </button>
        {/* card show modals*/}
        <DealModal
          showDealModal={showDealModal}
          setShowDealModal={setShowDealModal}
        />
      </div>

      {/* STATUS MODAL */}
      <div>
        {/* button toggle modals */}
        <button
          className="bg-primary-purple-4 text-white active:bg-pink-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
          type="button"
          onClick={() => setShowStatusModal(true)}
        >
          show status modal
        </button>
        {/* card show modals*/}
        <StatusModal
          showStatusModal={showStatusModal}
          setShowStatusModal={setShowStatusModal}
        />
      </div>

      {/* EDIT DELETE MODAL */}
      <div>
        {/* button toggle modals */}
        <button
          className="bg-primary-purple-4 text-white active:bg-pink-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
          type="button"
          onClick={() => setShowEditDeleteModal(true)}
        >
          show edit delete modal
        </button>
        {/* card show modals*/}
        <EditDeleteModal
          showEditDeleteModal={showEditDeleteModal}
          setShowEditDeleteModal={setShowEditDeleteModal}
        />
      </div>
    </>
  );
};

export default ToggleModal;
