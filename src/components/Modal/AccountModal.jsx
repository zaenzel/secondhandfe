import React, { useState, useEffect } from "react";
import jam from "../../images/jam.png";
import { FiCamera } from "react-icons/fi";
import { Link } from "react-router-dom";
import { BiEditAlt } from "react-icons/bi";
import { AiOutlineSetting } from "react-icons/ai";
import { IoIosLogOut } from "react-icons/io";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { logoutUser } from "../../redux/authSlice";
import { fetchMyProfile } from "../../redux/userSlice";
import { removeProductUser } from "../../redux/productSlice";

const AccountModal = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const userProfile = useSelector((state) => state.user);

  console.log(userProfile);

  useEffect(() => {
    // dispatch(fetchMyProfile(userProfile));
    if (userProfile.userProfileStatus === "rejected") {
      window.location.href = "/";
    }
  }, [dispatch]);

  return (
    <>
      {userProfile.userProfileStatus === "pending" && <p>Loading...</p>}
      {userProfile.userProfileStatus === "success" && (
        <div className="p-3 bg-white border-slate-300 drop-shadow-md rounded-xl space-y-3 cursor-default">
          <div className="flex justify-center flex-col">
            {userProfile.data.profile_picture === null ? (
              <div className="flex justify-center bg-primary-purple-1 p-5 rounded-2xl w-20 self-center">
                <FiCamera className="text-primary-purple-4" />
              </div>
            ) : (
              <div className="flex justify-center rounded-2xl w-20 self-center">
                <img
                  className="rounded-md object-cover"
                  src={userProfile.data.profile_picture_path}
                  alt={userProfile.data.profile_picture}
                />
              </div>
            )}

            <div className="text-center space-y-1">
              <p className="font-normal text-base text-neutral-5 mt-2 underline">
                {userProfile.data.name}
              </p>
            </div>
          </div>
          <div className="divide-y-2 divide-gray-100 w-80">
            <Link to={"/profile-edit"}>
              <div className="flex p-3 space-x-5">
                <BiEditAlt className="text-primary-purple-4" />
                <div className="text-start space-y-1">
                  <p className="text-sm font-semibold text-neutral-5">
                    Ubah Profil
                  </p>
                </div>
              </div>
            </Link>
            
            <button
              onClick={() => {
                dispatch(logoutUser(null));
                dispatch(removeProductUser())
                navigate("/");
              }}
              className="flex p-3 space-x-5"
            >
              <IoIosLogOut className="text-primary-purple-4" />
              <div className="text-start space-y-1">
                <p className="text-sm font-semibold text-neutral-5">Keluar</p>
              </div>
            </button>
          </div>
        </div>
      )}
    </>
  );
};

export default AccountModal;
