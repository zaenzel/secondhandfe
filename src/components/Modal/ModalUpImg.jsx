import React, { useState } from "react";
import { AiOutlineCloseCircle } from "react-icons/ai";
import { useDropzone } from "react-dropzone";
import { data } from "autoprefixer";

const ModalUpImg = ({ setOpenModalUp, setImg, data, setData }) => {
  const [files, setFiles] = useState([]);
  const { acceptedFiles, fileRejections, getRootProps, getInputProps, open } =
    useDropzone({
      // maxFiles: 4,
      noClick: true,
      noKeyboard: true,
      onDrop: (acceptedFiles) => {
        setFiles(
          acceptedFiles.map((file) =>
            Object.assign(file, {
              preview: URL.createObjectURL(file),
            })
          )
        );
      },
      //   accept: { "image/png": [".png"] }, di chrome gabisa
    });

  const photoName = files.map((img) => {
    return img.name;
  });

  const fileAccItems = acceptedFiles.map((file) => (
    <li className="text-sm text-neutral-5" key={file.path}>
      {file.path}
      <span className="ml-5 text-xs text-neutral-3">"{file.size} bytes"</span>
    </li>
  ));

  // const fileRejectionItems = fileRejections.map(({ file, errors }) => (
  //   <li key={file.path}>
  //     {file.path} - {file.size} bytes
  //     <ul>
  //       {errors.map((e) => (
  //         <li key={e.code}>{e.message}</li>
  //       ))}
  //     </ul>
  //   </li>
  // ));

  return (
    <div
      className="w-96 p-5 bg-white border-solid border-2 border-gray-200 space-y-5 shadow-2xl"
      data-testid="modal-upImg"
    >
      <div className="absolute top-2 right-2">
        <AiOutlineCloseCircle
          className="text-xl cursor-pointer"
          onClick={(e) => setOpenModalUp(false)}
        />
      </div>
      <div
        {...getRootProps()}
        className="py-10 space-y-3 flex flex-col items-center"
      >
        <input {...getInputProps()} />
        <p className="text-md text-neutral-3">Taro foto disini</p>
        <p className=" text-neutral-3">-atau-</p>
        <button
          onClick={open}
          className="py-2 w-56 bg-indigo-400 hover:bg-indigo-700 text-white text-sm"
        >
          Pilih file dari perangkat anda
        </button>
      </div>
      <div>
        {files.length < 1 && <p>maks 4 foto</p>}
        <p>{fileAccItems}</p>
      </div>
      <div className="space-x-2 flex justify-end mt-5">
        <button
          className="px-2 py-1 bg-white border-2 border-gray-400 text-neutral-5 rounded-md hover:bg-gray-300 text-sm"
          onClick={(e) => setOpenModalUp(false)}
        >
          Batal
        </button>
        <button
          className="px-2 py-1 bg-indigo-600 text-white rounded-md hover:bg-indigo-800 text-sm"
          onClick={(e) => {
            setImg((prevImg) => prevImg.concat(files));
            setOpenModalUp(false);
            setData({ ...data, img: files });
          }}
        >
          Upload
        </button>
      </div>
    </div>
  );
  //   className="w-96 p-20 bg-white border-solid border-2 border-gray-500"
};

export default ModalUpImg;
