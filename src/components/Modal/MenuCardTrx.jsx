import React from "react";
import { BiDetail } from "react-icons/bi";
import { MdLocalOffer, MdDelete } from "react-icons/md";
import { useNavigate } from "react-router-dom";

const MenuCardTrx = ({ data, handleDelete }) => {
  const navigate = useNavigate();
  return (
    <>
      <div className="absolute right-2 top-1/2 -translate-y-3 z-10">
        <ul className="flex text-center items-center space-x-2 text-white text-xs font-semibold">
          <li
            className="flex items-center py-2 px-4 rounded-md bg-blue-600"
            onClick={(e) => navigate(`/detail/${data.wishlist_product.slug}`)}
          >
            <BiDetail className="mr-2" /> Detail
          </li>
          <li
            className="flex items-center py-2 px-4 rounded-md bg-red-600"
            onClick={handleDelete}
          >
            <MdDelete className="mr-2" /> Delete
          </li>
        </ul>
      </div>
      <div className="absolute rounded-md opacity-75 inset-0 bg-black"></div>
    </>
  );
};

export default MenuCardTrx;
