import React from "react";
import miniWatch from "../../images/miniWatch.png";
import { GrClose } from "react-icons/gr";
import { BsWhatsapp } from "react-icons/bs";
import "./DealModal.css";
import avatar from "../../images/sasuke.jpg";
import { useNavigate, useParams } from "react-router-dom";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchProductOfferById,
  getProductOfferById,
} from "../../redux/offerSlice";

const DealModal = ({ showDealModal, setShowDealModal }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { id } = useParams();

  const productOfferById = useSelector(getProductOfferById);

  useEffect(() => {
    dispatch(fetchProductOfferById(id));
  }, [dispatch, id]);

  // console.log(productOfferById);

  return (
    <div data-testid="deal-modal">
      {showDealModal ? (
        <>
          {productOfferById && productOfferById.message === "success" ? (
            <div className="deal-modal-wrapper">
              <div className="modal-card">
                <div className="button-container">
                  <button
                    onClick={() => {
                      setShowDealModal(false);
                      navigate("/dashboard");
                    }}
                  >
                    <GrClose />
                  </button>
                </div>
                <h5 className="modal-title">
                  Yeay kamu berhasil mendapat harga yang sesuai
                </h5>
                <p className="modal-text">
                  Segera hubungi pembeli melalui whatsapp untuk transaksi
                  selanjutnya
                </p>
                <div className="modal-detail-product">
                  <h5 className="text-center font-bold">Product Match</h5>
                  <div className="buyer-info-wrapper">
                    <img
                      src={
                        productOfferById.result.disc_product.bidder
                          .profile_picture_path
                      }
                      alt="avatar"
                      className="w-12 h-12 object-cover rounded-xl"
                    />
                    <div>
                      <h6 className="text-sm font-medium truncate">
                        {productOfferById.result.disc_product.bidder.name}
                      </h6>
                      <p className="text-xs text-neutral-3">
                        {productOfferById.result.disc_product.bidder.address}
                      </p>
                    </div>
                  </div>
                  <div className="product-info-wrapper">
                    <img
                      src={
                        productOfferById.result.disc_product.product_offered
                          .product_images[0].product_images_path
                      }
                      alt="mini watch"
                      className="w-12 h-12 object-cover rounded-xl"
                    />
                    <div className="text-sm font-normal space-y-1 truncate">
                      <h6>
                        {
                          productOfferById.result.disc_product.product_offered
                            .product_name
                        }
                      </h6>
                      <p>
                        Rp. {productOfferById.result.disc_product.product_offered.product_price.toLocaleString(
                            "id-ID"
                          )}
                      </p>
                      <p>
                        Rp. {productOfferById.result.disc_product.bargain_price.toLocaleString(
                          "id-ID"
                        )}
                      </p>
                    </div>
                  </div>
                </div>

                <a
                  href={`https://wa.me/${productOfferById.result.disc_product.bidder.phone_number}`}
                  target="_blank"
                >
                  <button
                    type="submit"
                    className="send-button"
                    onClick={() => {
                      setShowDealModal(false);
                      navigate("/dashboard");
                    }}
                  >
                    <p>Hubungi via Whatsapp</p>
                    <BsWhatsapp className="" />
                  </button>
                </a>
              </div>
            </div>
          ) : null}
          {/* opacity around the card */}
          <div className="opacity-75 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </div>
  );
};

export default DealModal;
