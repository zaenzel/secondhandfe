import React from "react";
import { Link } from "react-router-dom";
import jam from "../../images/jam.png"

const ModalSearch = ({product}) => {
  return (
    <Link to={`detail/${product.slug}`}>
    <div className="mt-2 px-7 py-2 flex space-x-2">
      <img src={product.product_images[0].product_images_path} alt="img product" className="w-16 h-16 object-cover rounded-sm"/>
      <div className="space-y-1">
        <p className="text-sm text-neutral-5">{product.product_name}</p>
        <p className="text-xsx text-neutral-3">{product.category_product.category_name}</p>
      </div>
    </div>
    </Link>
  );
};

export default ModalSearch;
