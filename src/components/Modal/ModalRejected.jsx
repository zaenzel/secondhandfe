import React from "react";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { removeStatus } from "../../redux/productSlice";

const ModalRejected = ({ editProduct, newProduct }) => {
  const dispatch = useDispatch();

  setTimeout(() => {
    dispatch(removeStatus());
  }, 1000);
  return (
    <>
      <div className="absolute top-0 left-1/2 -translate-x-[50%] z-30 bg-white p-10 rounded-b-xl w-72 text-center border shadow-md text-red-500 text-sm" data-testid="modal-rejected">
        {/* {editProduct.message || newProduct.message} */}
        {newProduct}
      </div>
      <div className="opacity-25 fixed inset-0 z-20 bg-black"></div>
    </>
  );
};

export default ModalRejected;
