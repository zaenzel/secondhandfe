import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import jam from "../../images/jam.png";
import { getAllNotifs, getStatusNotif } from "../../redux/notifSlice";
import LoadingCard from "../Loading/LoadingCard";
import NullData from "../NullData/NullData";

const NotifModal = () => {
  const getAllNotif = useSelector(getAllNotifs);
  const getStatus = useSelector(getStatusNotif);

  let renderModalNotif,
    thumbnail = "";

  thumbnail = getAllNotif.result.thumbnail.slice(0, 3).map((product, index) => {
    return product;
  });

  const date = getAllNotif.result.createdAt.map((date, index) => {
    return date;
  });

  // const isReadAll =
  //   getAllNotif &&
  //   getAllNotif.message === "success" &&
  //   getAllNotif.result.notification
  //     .slice(0, 3)
  //     .some((notif) => notif.is_read === true);

  // console.log(!isReadAll);

  renderModalNotif =
    getAllNotif && getAllNotif.message === "success" ? (
      getAllNotif.result.notification.length === 0 ? (
        <div className="py-10 lg:py-0">
          <NullData props={false} />
        </div>
      ) : (
        <div className="flex justify-center flex-col">
          <div className="flex justify-center flex-col sm:w-fit self-center">
            {getAllNotif.result.notification
              .slice(0, 3)
              .map((product, index) => (
                <Link to={product.url} key={index}>
                  <div className="flex p-3 relative space-x-5 sm:w-[350px]">
                    <img
                      src={thumbnail[index]}
                      alt={product.product_notification.product_name.slice(
                        0,
                        6
                      )}
                      className="w-12 h-12 rounded-xl text-sm"
                    />
                    <div className="text-start space-y-1">
                      <p className="text-xsx text-neutral-3">
                        {product.action_message}
                      </p>
                      <p className="text-xs text-neutral-5">
                        {product.product_notification.product_name}
                      </p>
                      <p className="text-xs text-neutral-5">
                        Rp.{" "}
                        {product.product_notification.product_price.toLocaleString(
                          "id-ID"
                        )}
                      </p>
                      {product.additional_info_1 && (
                        <p className="text-xs text-neutral-5">
                          {product.additional_info_1} Rp.{" "}
                          {product.bargain_price.toLocaleString("id-ID")}
                        </p>
                      )}
                      {product.additional_info_2 && (
                        <p className="text-xsx text-neutral-3">
                          {product.additional_info_2}
                        </p>
                      )}
                    </div>
                    <div className="flex sm:absolute sm:right-2 space-x-2">
                      <p className="text-xsx text-neutral-3">{date[index]}</p>
                      {product.is_read === false ? (
                        <div className="p-1 h-1 items-center rounded-full bg-alert-red" />
                      ) : null}
                    </div>
                  </div>

              <hr />
                </Link>
              ))}
          </div>
          
        </div>
      )
    ) : (
      <div className="grid grid-cols-2 gap-3">
        <LoadingCard />
      </div>
    );

  return (
    <div className="p-3 bg-white border-slate-300 drop-shadow-md rounded-xl space-y-3 divide-y-2 divide-gray-100">
      {getStatus === "pending" && <p>loading</p>}
      {getAllNotif.message === "success" && renderModalNotif}
      <Link to="/notification">
        <div className="bg-primary-purple-1 rounded-md py-2">
          <p className="text-sm text-primary-purple-4">
            Lihat semua notifikasi
          </p>
        </div>
      </Link>
    </div>
  );
};

export default NotifModal;
