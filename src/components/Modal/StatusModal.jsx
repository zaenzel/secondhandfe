import React, { useState } from "react";
import miniWatch from "../../images/miniWatch.png";
import { GrClose } from "react-icons/gr";
import { BsWhatsapp } from "react-icons/bs";
import "./StatusModal.css";
import avatar from "../../images/sasuke.jpg";
import "./StatusModal.css";
import { getProductOfferById } from "../../redux/offerSlice";
import { useDispatch, useSelector } from "react-redux";
import {
  cancelTransactionState,
  dealTransactionState,
  putCancelTransaction,
  putDealTransaction,
} from "../../redux/transactionSlice";
import { useNavigate } from "react-router-dom";

const StatusModal = ({ showStatusModal, setShowStatusModal }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const productOfferById = useSelector(getProductOfferById);
  const dealTransaction = useSelector(dealTransactionState);
  const cancelTransaction = useSelector(cancelTransactionState);

  const [transaction, setTransaction] = useState({
    userId:
      productOfferById && productOfferById.message === "success"
        ? productOfferById.result.disc_product.user_id
        : "",
    productId:
      productOfferById && productOfferById.message === "success"
        ? productOfferById.result.disc_product.product_id
        : "",
  });

  const [selected, setSelected] = useState("deal");

  const handleChange = (e) => {
    setSelected(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(selected);
    if (selected === "deal") {
      dispatch(putDealTransaction(transaction));
      setShowStatusModal(false);
      navigate(`/dashboard`);
      console.log(dealTransaction);
    } else {
      dispatch(putCancelTransaction(transaction));
      setShowStatusModal(false);
      navigate(`/dashboard`);
      console.log(cancelTransaction);
    }
  };

  return (
    <div data-testid="status-modal">
      {showStatusModal ? (
        <>
          <div className="status-modal-wrapper">
            <form onSubmit={handleSubmit} className="modal-card">
              <div className="button-container">
                <button
                  onClick={() => {
                    setShowStatusModal(false);
                  }}
                >
                  <GrClose />
                </button>
              </div>
              <h5 className="modal-title">
                Perbarui status penjualan produkmu
              </h5>

              <div className="radio-group-wrapper">
                <div>
                  <div className="form-check mb-4">
                    <input
                      className="form-check-input appearance-none rounded-full h-4 w-4 border-2 border-neutral-2 bg-white checked:bg-primary-purple-4 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-3 cursor-pointer"
                      type="radio"
                      name="flexRadioDefault"
                      id="flexRadioDefault1"
                      value="deal"
                      checked={selected === "deal"}
                      onChange={handleChange}
                    />
                    <label
                      className="form-check-label flex flex-col text-gray-800"
                      for="flexRadioDefault1"
                    >
                      <p className="font-medium">Berhasil terjual</p>
                      <p className="text-base text-neutral-3">
                        Kamu telah sepakat menjual produk ini kepada pembeli
                      </p>
                    </label>
                  </div>
                  <div className="form-check mb-4">
                    <input
                      className="form-check-input appearance-none rounded-full h-4 w-4 border-2 border-neutral-2 bg-white checked:bg-primary-purple-4 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-3 cursor-pointer"
                      type="radio"
                      name="flexRadioDefault"
                      id="flexRadioDefault2"
                      value="cancel"
                      checked={selected === "cancel"}
                      onChange={handleChange}
                    />
                    <label
                      className="form-check-label flex flex-col text-gray-800"
                      for="flexRadioDefault2"
                    >
                      <p className="font-medium">Batalkan transaksi</p>
                      <p className="text-base text-neutral-3">
                        Kamu membatalkan transaksi produk ini dengan pembeli
                      </p>
                    </label>
                  </div>
                </div>
              </div>

              <button type="submit" className="send-button">
                <p>Kirim</p>
              </button>
            </form>
          </div>

          {/* opacity around the card */}
          <div className="opacity-75 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </div>
  );
};

export default StatusModal;
