import React, { useEffect, useState } from "react";
import { BsBoxArrowInRight, BsSearch, BsListUl } from "react-icons/bs";
import { IoMdNotificationsOutline } from "react-icons/io";
import { VscAccount } from "react-icons/vsc";
import logo from "../../images/logo.png";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import NotifModal from "../Modal/NotifModal";
import BtnHamburger from "./BtnHamburger";
import AccountModal from "../Modal/AccountModal";
import { fetchMyProfile } from "../../redux/userSlice";
import {
  fetchAsyncProducts,
  fetchProductsByFilter,
  getAllProductFilter,
  getStatusProduct,
  removeStatus,
  setSearchOnTrue,
} from "../../redux/productSlice";
import { getAllNotifs } from "../../redux/notifSlice";
import ModalSearch from "../Modal/ModalSearch";
import "./Navbar.css";

const Navbar = ({ input }) => {
  const [notif, setNotif] = useState(false);
  const [showAccountModal, setShowAccountModal] = useState(false);
  const [search, setSearch] = useState("");

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const isAuth = useSelector((state) => state.auth);
  const getAllNotif = useSelector(getAllNotifs);
  const product = useSelector(getAllProductFilter);
  const status = useSelector(getStatusProduct);

  const searchChange = (e) => {
    setSearch(e.target.value);
  };

  const searchSubmit = (e) => {
    e.preventDefault();
    dispatch(fetchProductsByFilter(search));
  };

  let statusPending = (
    <div className="flex p-4">
      <svg
        role="status"
        className="w-5 h-5 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-primary-purple-2"
        viewBox="0 0 100 101"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
          fill="currentColor"
        />
        <path
          d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
          fill="currentFill"
        />
      </svg>
      <p className="text-sm">mencari barang</p>
    </div>
  );

  let statusSuccess = 
    product.message === "success" &&
    product.products.rows.map((product, index) => {
      return <ModalSearch product={product} key={index} />;
    });

  const searchFokus = () => {
    dispatch(removeStatus());
  };

  let renderSearch = search
    ? // product.message === "success" && product.products.rows.length !== 0 ? (
      (status === "pending" && statusPending) ||
      (status === "success" && statusSuccess) ||
      (product.total_items === 0 && (
        <p className="p-4 text-sm ">ga ada barang</p>
      ))
    : null;

  const oneUnread =
    getAllNotif &&
    getAllNotif.message === "success" &&
    getAllNotif.result.notification
      .slice(0, 3)
      .some((notif) => notif.is_read === false);

  return (
    <header
      className="sm:fixed sm:bg-white absolute top-0 w-full z-10 sm:border-b-2"
      data-testid="navbar-true"
    >
      <nav className="flex items-center p-3 bg-transparent space-x-3 sm:px-10 lg:px-20">
        <div className="flex items-center space-x-2 w-full">
          <Link to="/">
            <div className="hidden sm:block">
              <img src={logo} alt="logo" />
            </div>
          </Link>
          <BtnHamburger isAuth={isAuth} />
          {input && (
            <div className="relative">
              <form
                onSubmit={searchSubmit}
                className="flex items-center p-3 bg-white sm:bg-[#EEEEEE] rounded-2xl w-full max-w-md"
              >
                <input
                  placeholder="Cari disini"
                  className="text-sm w-full outline-none bg-transparent px-2"
                  onChange={searchChange}
                  onFocus={searchFokus}
                />
                <button>
                  <BsSearch className="text-xl text-neutral-3 cursor-pointer" />
                </button>
              </form>
              <div className={search ? "result show" : "result"}>
                {renderSearch}
              </div>
            </div>
          )}
        </div>
        <div className="hidden sm:block">
          {isAuth._id ? (
            <div className="flex text-2xl space-x-5">
              <button
                onClick={(e) => {
                  navigate("/dashboard");
                }}
              >
                <BsListUl />
              </button>
              <button
                className={notif ? "text-blue-700" : "text-neutral-5"}
                onClick={(e) => {
                  setNotif(!notif);
                  setShowAccountModal(false);
                }}
              >
                {notif && (
                  <div className="absolute z-20 right-20 top-14 xl:right-24">
                    <NotifModal />
                  </div>
                )}
                <div className="flex space-x-0">
                  <div className="flex space-x-0">
                    <IoMdNotificationsOutline />
                    {oneUnread ? (
                      <div className="p-1 h-1 items-center rounded-full bg-alert-red" />
                    ) : null}
                  </div>
                </div>
              </button>
              <button
                className={
                  showAccountModal ? "text-blue-700" : "text-neutral-5"
                }
                onClick={(e) => {
                  setShowAccountModal(!showAccountModal);
                  setNotif(false);
                }}
              >
                {showAccountModal && (
                  <div className="absolute z-20 right-12 top-14 xl:right-16">
                    <AccountModal />
                  </div>
                )}
                <VscAccount />
              </button>
            </div>
          ) : (
            <Link to="/login">
              <button
                type="button"
                className="text-white bg-purple-800 hover:bg-purple-700 focus:ring-4 focus:outline-none focus:ring-purple-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center lg:mr-3 md:mr-0 flex"
              >
                <span className="mr-3 text-xl">
                  <BsBoxArrowInRight />
                </span>
                Masuk
              </button>
            </Link>
          )}
        </div>
      </nav>
    </header>
  );
};

export default Navbar;
