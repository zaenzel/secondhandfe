import React, { Fragment } from "react";
import { Menu, Transition } from "@headlessui/react";
import { BsBoxArrowInRight, BsSearch, BsList, BsListUl } from "react-icons/bs";
import { IoMdNotificationsOutline } from "react-icons/io";
import { AiOutlineClose, AiOutlineHome } from "react-icons/ai";
import { VscAccount } from "react-icons/vsc";
import { Link } from "react-router-dom";

const BtnHamburger = ({ isAuth }) => {
  return (
    <Menu as="div" className="bg-white p-3 rounded-2xl sm:hidden">
      <Menu.Button as="div" className="items-center">
        <BsList className="text-xl" />
      </Menu.Button>
      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items className="pt-10 fixed z-30 left-0 top-0 h-screen divide-y-2 divide-gray-100 rounded-md bg-white shadow-lg focus:outline-none ">
          {isAuth._id ? (
            <>
              <Menu.Item
                as="div"
                className="p-3 text-sm font-bold flex justify-center hover:bg-primary-purple-3 hover:text-white hover:rounded-md"
              >
                {({ active }) => (
                  <Link className="flex items-center gap-2 " to='/'>
                    Second Hand
                    <AiOutlineClose className="text-lg" />
                  </Link>
                )}
              </Menu.Item>
              <Menu.Item
                as="div"
                className="p-3 text-sm flex hover:bg-primary-purple-3 hover:text-white hover:rounded-md"
              >
                {({ active }) => (
                  <Link className="flex items-center gap-2" to="/">
                    <AiOutlineHome className="text-xl" />
                    Home
                  </Link>
                )}
              </Menu.Item>
              <Menu.Item
                as="div"
                className="p-3 text-sm flex hover:bg-primary-purple-3 hover:text-white hover:rounded-md"
              >
                {({ active }) => (
                  <Link className="flex items-center gap-2" to="/dashboard">
                    <BsListUl className="text-xl" />
                    Daftar jual
                  </Link>
                )}
              </Menu.Item>
              <Menu.Items
                as="div"
                className="p-3 text-sm flex hover:bg-primary-purple-3 hover:text-white hover:rounded-md"
              >
                {({ active }) => (
                  <Link className="flex items-center gap-2" to="/notification">
                    <IoMdNotificationsOutline className="text-xl" />
                    Notifikasi
                  </Link>
                )}
              </Menu.Items>
              <Menu.Item
                as="div"
                className="p-3 text-sm flex hover:bg-primary-purple-3 hover:text-white hover:rounded-md"
              >
                {({ active }) => (
                  <Link
                    className="flex items-center gap-2"
                    to="/account-settings"
                  >
                    <VscAccount className="text-xl" />
                    Akun
                  </Link>
                )}
              </Menu.Item>
            </>
          ) : (
            <>
              <Menu.Item
                as="div"
                className="p-3 text-sm font-bold flex justify-center hover:bg-primary-purple-3 hover:text-white hover:rounded-md"
              >
                {({ active }) => (
                  <a className="flex items-center gap-2 ">
                    Second Hand
                    <AiOutlineClose className="text-lg" />
                  </a>
                )}
              </Menu.Item>
              <Menu.Item
                as="div"
                className="p-3 text-sm flex hover:bg-primary-purple-3 hover:text-white hover:rounded-md"
              >
                {({ active }) => (
                  <a className="flex items-center gap-2" href="/login">
                    <BsBoxArrowInRight />
                    Masuk
                  </a>
                )}
              </Menu.Item>
            </>
          )}
        </Menu.Items>
      </Transition>
    </Menu>
  );
};

export default BtnHamburger;
