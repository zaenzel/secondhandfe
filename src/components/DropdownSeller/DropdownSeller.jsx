import React from "react";
import { RiArrowDropDownLine } from "react-icons/ri";

const DropdownSeller = ({setFilterOffer}) => {
  return (
    <div class="inline-block relative w-full mb-2">
      <select
        class="block appearance-none w-full bg-white border border-gray-300 px-4 py-2 pr-8 rounded-xl leading-tight focus:outline-none focus:shadow-outline"
        id="product_category"
        name="product_category"
        onChange={(e) => setFilterOffer(e.target.value)}
      >
        <option value="all">Semua</option>
        <option value="pending">Pending</option>
        <option value="accepted">Diterima</option>
        <option value="rejected">Ditolak</option>
      </select>
      <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
        <RiArrowDropDownLine className="text-3xl" />
      </div>
    </div>
  );
};

export default DropdownSeller;
