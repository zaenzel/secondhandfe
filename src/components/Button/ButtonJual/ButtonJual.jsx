import React from "react";
import { BiPlus } from "react-icons/bi";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

const ButtonJual = () => {
  const isAuth = useSelector(state => state.auth)
  const navigate = useNavigate();
  return (
    <button
      className="btn-category-active hover:bg-primary-purple-5"
      onClick={(e) => {
        if (!isAuth._id) {
          navigate("/login");
        }else{
          navigate("/dashboard/addproduct")
        }
      }}

      data-testid="btn-jual"
    >
      <span>
        <BiPlus className="text-lg" />
      </span>
      Jual
    </button>
  );
};

export default ButtonJual;
