import React from "react";
import { FaCube, FaRegHeart, FaDollarSign } from "react-icons/fa";
import { GrTransaction } from "react-icons/gr";
import { MdOutlineLocalOffer } from "react-icons/md";

const ButtonCategorySeller = ({
  product,
  interest,
  sold,
  trx,
  offers,
  setProduct,
  setInterest,
  setSold,
  setTrx,
  setOffers
}) => {
  
  return (
    <div className="btnCategory lg:hidden">
      <button
        className={product ? "btn-category-active" : "btn-category"}
        onClick={(e) => {
          setProduct(true);
          setInterest(false);
          setSold(false);
          setTrx(false)
          setOffers(false)
        }}
      >
        <span className="mr-2 text-xl">
          <FaCube />
        </span>
        Produk
      </button>
      <button
        className={interest ? "btn-category-active" : "btn-category"}
        onClick={(e) => {
          setInterest(true);
          setProduct(false);
          setSold(false);
          setTrx(false)
          setOffers(false)
        }}
      >
        <span className="mr-2 text-xl">
          <FaRegHeart />
        </span>
        Diminati
      </button>
      <button
        className={sold ? "btn-category-active" : "btn-category"}
        onClick={(e) => {
          setSold(true);
          setInterest(false);
          setProduct(false);
          setTrx(false)
          setOffers(false)
        }}
      >
        <span className="mr-2 text-xl">
          <FaDollarSign />
        </span>
        Terjual
      </button>
      <button
        className={trx ? "btn-category-active" : "btn-category"}
        onClick={(e) => {
          setTrx(true)
          setSold(false);
          setInterest(false);
          setProduct(false);
          setOffers(false)
        }}
      >
        <span className="mr-2 text-xl">
          <GrTransaction />
        </span>
        Iklanmu
      </button>
      <button
        className={offers ? "btn-category-active" : "btn-category"}
        onClick={(e) => {
          setOffers(true)
          setTrx(false)
          setSold(false);
          setInterest(false);
          setProduct(false);
        }}
      >
        <span className="mr-2 text-xl">
          <MdOutlineLocalOffer />
        </span>
        Tawaranmu
      </button>
    </div>
  );
};

export default ButtonCategorySeller;
