import React, { useState } from "react";
import { useEffect } from "react";
import { FiSearch } from "react-icons/fi";
import { MdCasino } from "react-icons/md";
import { useDispatch } from "react-redux";
import { fetchAsyncProducts } from "../../../redux/productSlice";

const ButtonCategory = ({
  semua,
  setSemua,
  hobi,
  setHobi,
  kendaraan,
  setKendaraan,
  baju,
  setBaju,
  elektronik,
  setElektronik,
  kesehatan,
  setKesehatan,
}) => {
  const dispatch = useDispatch();
  const [productPage, setProductPage] = useState({
    filter: "",
    page: 1,
  });
  
  return (
    <div
      className="btnCategory"
    >
      <button
        className={semua ? "btn-category-active" : "btn-category"}
        onClick={() => {
          dispatch(fetchAsyncProducts(productPage))
          setSemua(true);
          setHobi(false);
          setKendaraan(false);
          setBaju(false);
          setElektronik(false);
          setKesehatan(false);
        }}
      >
        <span className="mr-2 text-lg">
          <FiSearch />
        </span>
        Semua
      </button>
      <button
        className={hobi ? "btn-category-active" : "btn-category"}
        onClick={() => {
          setHobi(true);
          setSemua(false);
          setKendaraan(false);
          setBaju(false);
          setElektronik(false);
          setKesehatan(false);
        }}
      >
        <span className="mr-2 text-lg">
          <FiSearch />
        </span>
        Hobi
      </button>
      <button
        className={kendaraan ? "btn-category-active" : "btn-category"}
        onClick={() => {
          setKendaraan(true);
          setSemua(false);
          setHobi(false);
          setBaju(false);
          setElektronik(false);
          setKesehatan(false);
        }}
      >
        <span className="mr-2 text-lg">
          <FiSearch />
        </span>
        Kendaraan
      </button>
      <button
        className={baju ? "btn-category-active" : "btn-category"}
        onClick={() => {
          setBaju(true);
          setSemua(false);
          setHobi(false);
          setKendaraan(false);
          setElektronik(false);
          setKesehatan(false);
        }}
      >
        <span className="mr-2 text-lg">
          <FiSearch />
        </span>
        Baju
      </button>
      <button
        className={elektronik ? "btn-category-active" : "btn-category"}
        onClick={() => {
          setElektronik(true);
          setSemua(false);
          setHobi(false);
          setKendaraan(false);
          setBaju(false);
          setKesehatan(false);
        }}
      >
        <span className="mr-2 text-lg">
          <FiSearch />
        </span>
        Elektronik
      </button>
      <button
        className={kesehatan ? "btn-category-active" : "btn-category"}
        onClick={() => {
          setKesehatan(true);
          setSemua(false);
          setHobi(false);
          setKendaraan(false);
          setBaju(false);
          setElektronik(false);
        }}
      >
        <span className="mr-2 text-lg">
          <FiSearch />
        </span>
        Kesehatan
      </button>
    </div>
  );
};

export default ButtonCategory;
