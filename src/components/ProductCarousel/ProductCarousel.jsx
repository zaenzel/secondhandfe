import React from "react";
import Slider from "react-slick";
import imageProd from "../../images/heroImg.png";
import "../../styles/productCarousel.css";

const productCarousel = () => {
  const settings = {
    // customPaging: function(i) {
    //   return (
    //     <a>
    //       <img src={imageProd} alt="produc_img_preview"/>
    //     </a>
    //   );
    // },
    dots: true,
    dotsClass: "slick-dots slick-thumb",
    speed: 500,
    slidesToShow: 1,
  };
  return (
    <div className="w-full">
      <Slider {...settings}>
        <div>
          <img
            src={imageProd}
            alt="produc_img"
            className="object-cover mt-0 mb-0 ml-auto mr-auto"
          />
        </div>
        <div>
          <img
            src={imageProd}
            alt="produc_img"
            className="object-cover mt-0 mb-0 ml-auto mr-auto"
          />
        </div>
        <div>
          <img
            src={imageProd}
            alt="produc_img"
            className="object-cover mt-0 mb-0 ml-auto mr-auto"
          />
        </div>
        <div>
          <img
            src={imageProd}
            alt="produc_img"
            className="object-cover mt-0 mb-0 ml-auto mr-auto"
          />
        </div>
      </Slider>
    </div>
  );
};

export default productCarousel;
