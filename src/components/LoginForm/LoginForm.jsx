import { Link, useNavigate } from "react-router-dom";
import { loginUser } from "../../redux/authSlice";
import { BiShowAlt, BiHide, BiArrowBack } from "react-icons/bi";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux"
import { fetchMyProfile } from "../../redux/userSlice";

const LoginForm = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [passwordType, setPasswordType] = useState("password");
  const [user, setUser] = useState({
    email: "",
    password: "",
  });

  const auth = useSelector((state) => state.auth)
  const userProfile = useSelector((state) => state.user)

  useEffect(() => {
    if(auth._id) {
      navigate('/dashboard')
    }
  }, [auth._id, navigate]);

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(loginUser(user));
    dispatch(fetchMyProfile(userProfile))
  };

  const togglePasswordShown = () => {
    if (passwordType === "password") {
      setPasswordType("text");
      return;
    }
    setPasswordType("password");
  };

  return (
    <>
      <form onSubmit={handleSubmit} className="max-w-[400px] w-full mx-auto p-8 px-8 rounded-lg" data-testid="login-form">
        <div className="pb-8">
          <BiArrowBack className="text-3xl sm:hidden" />
        </div>
        <h4 className="text-2xl text-black font-bold pb-3">Masuk</h4>
        <div className="flex flex-col text-gray-700 py-2 ">
          <label>Email</label>
          <input
            className="bg-white h-10 px-5 pr-10 rounded-2xl text-sm border border-gray-400 w-full focus:outline-none focus:bg-gray-200"
            placeholder="Contoh: johndee@gmail.com"
            type="email"
            id="email"
            name="email"
            // value={email}
            onChange={(e) => setUser({ ...user, email: e.target.value })}
          />
        </div>
        <div className="relative flex flex-col text-gray-700 py-2 ">
          <label>Password</label>
          <div className="relative text-gray-600">
            <input
              className="bg-white h-10 px-5 pr-10 rounded-2xl text-sm border border-gray-400 w-full focus:outline-none focus:bg-gray-200"
              placeholder="Masukkan password"
              type={passwordType}
              id="password"
              name="password"
              // value={password}
              onChange={(e) => setUser({ ...user, password: e.target.value })}
            />
            <i
              onClick={togglePasswordShown}
              className="absolute right-0 top-0 mt-3 mr-4 cursor-pointer"
            >
              {passwordType === "password" && <BiHide />}
              {passwordType === "text" && <BiShowAlt />}
            </i>
          </div>
        </div>
        <button
          type="submit"
          className="w-full my-5 py-2 rounded-2xl text-white bg-primary-purple-4 shadow-lg shadow-purple-200/50 font-semibold"
          
        >
          {auth.loginStatus === "pending" ? (
            <div className="flex items-center justify-center space-x-2">
              <svg
                role="status"
                className="w-5 h-5 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-primary-purple-2"
                viewBox="0 0 100 101"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                  fill="currentColor"
                />
                <path
                  d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                  fill="currentFill"
                />
              </svg>
              Processing...
            </div>
          ) : (
            <p>Masuk</p>
          )}
        </button>

        <div className="mb-8">
          {auth.loginStatus === "rejected" && (
            <p className="text-red-700">{auth.loginMessage}</p>
          )}
          {auth.loginStatus === "success" && (
            <p className="text-green-700">{auth.loginMessage}</p>
          )}
        </div>

        <div className="text-center pt-3 inline">
          <p>
            Belum punya akun?{" "}
            <span className="text-purple-800">
              <Link to="/register"> Daftar di sini</Link>
            </span>
          </p>
        </div>
      </form>
    </>
  );
};

export default LoginForm;