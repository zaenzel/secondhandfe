import React, { useEffect, useState } from "react";
import CardCheckOut from "../../components/Card/CardCheckOut";
import CardSeller from "../../components/Card/CardSeller";
import imgProduct from "../../images/jam-gede.png";
import ProductCarousel from "../../components/ProductCarousel/ProductCarousel";
import { MdArrowBack } from "react-icons/md";
import { Link, useNavigate, useParams } from "react-router-dom";
import CardDeskripsi from "../../components/Card/CardDeskripsi";
import BargainModal from "../../components/Modal/BargainModal";
import Navbar from "../../components/Navbar/Navbar";
import { MdOutlineFavoriteBorder, MdFavorite } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchAsyncDetailProduct,
  fetchDetailProductNoAuth,
  getDetailProduct,
  getDetailProductNoAuth,
  productByHobi,
  removeDetail,
} from "../../redux/productSlice";
import LoadingScreen from "../../components/Loading/LoadingScreen";
import CarouselDetail from "../../components/Carousel/CarouselDetail";
import {
  addWishlist,
  fetchWishlist,
  getStatusWishList,
  getWishlist,
} from "../../redux/wishlist";
import Status from "../../components/Toast/Status";
import { data } from "autoprefixer";
import {
  fetchOfferByBidder,
  fetchOfferByBidderAccepted,
  fetchOfferByBidderPending,
  offerByBidderAcceptedState,
  offerByBidderPendingState,
  offerByBidderState,
  removeStatusOffer,
} from "../../redux/offerSlice";
import { fetchMyProfile } from "../../redux/userSlice";
import CardCheckOutNoAuth from "../../components/Card/CardCheckoutNoAuth";
import { fetchAsyncNotifs } from "../../redux/notifSlice";

const Detail = () => {
  const { slug } = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const product = useSelector(getDetailProduct);
  const productNoAuth = useSelector(getDetailProductNoAuth);
  const isAuth = useSelector((state) => state.auth);
  const userProfile = useSelector((state) => state.user);
  const allOfferByBidderPending = useSelector(offerByBidderPendingState);
  const allOfferByBidderAccepted = useSelector(offerByBidderAcceptedState);

  const statusWishlist = useSelector(getStatusWishList);
  const dataWishList = useSelector(getWishlist);
  const [match, setMatch] = useState();
  const [showBargainModal, setShowBargainModal] = useState(false);
  const [wishList, setWishlist] = useState(false);

  useEffect(() => {
    dispatch(fetchAsyncDetailProduct(slug));
    dispatch(fetchDetailProductNoAuth(slug));
    dispatch(fetchWishlist());
    dispatch(fetchAsyncNotifs());
    dispatch(removeDetail());
    dispatch(fetchMyProfile(userProfile));
    dispatch(fetchOfferByBidder());
    dispatch(fetchOfferByBidderPending());
    dispatch(fetchOfferByBidderAccepted());
  }, [dispatch, slug]);

  const handleWishlist = (e) => {
    setWishlist(!wishList);
    dispatch(addWishlist(product.product.id));
  };

  const productId =
    product && product.message === "success" && product.product.id;

  const bidderArrPending =
    allOfferByBidderPending &&
    allOfferByBidderPending.message === "success" &&
    allOfferByBidderPending.result.disc_products
      .slice()
      .some((product) => product.product_id === productId);

  const bidderArrAccepted =
    allOfferByBidderAccepted &&
    allOfferByBidderAccepted.message === "success" &&
    allOfferByBidderAccepted.result.disc_products
      .slice()
      .some((product) => product.product_id === productId);

  console.log(bidderArrPending);

  let detailWithAuth =
    product.message === "success" ? (
      <div className="">
        <div className="hidden lg:block">
          <Navbar />
        </div>
        <BargainModal
          product={product}
          showBargainModal={showBargainModal}
          setShowBargainModal={setShowBargainModal}
        />
        <div className="lg:mt-20 max-w-4xl mx-auto lg:grid lg:grid-cols-[2fr_1fr] lg:gap-5 lg:py-5">
          {statusWishlist === "wishListAdd" && <Status wishList={wishList} />}
          <div className="absolute left-4 top-12 sm:left-10 z-40">
            <button
              className="bg-white rounded-full lg:hidden"
              onClick={(e) => {
                navigate("/");
              }}
            >
              <MdArrowBack className="text-xl" />
            </button>
          </div>

          <CarouselDetail product={product} />

          <div className="mx-auto px-3 w-full max-w-screen-sm -translate-y-7 lg:-translate-y-0 space-y-5 lg:space-y-5 lg:px-0">
            <CardCheckOut
              setShowBargainModal={setShowBargainModal}
              product={product}
              wishList={wishList}
              handleWishlist={handleWishlist}
            />
            <CardSeller product={product} />
          </div>
          <div className="mx-auto px-3 max-w-screen-sm lg:min-w-full mb-20 lg:mb-0 lg:px-0">
            <CardDeskripsi product={product} />
          </div>
          <div className="flex items-center justify-center mx-auto fixed bottom-5 inset-x-5 max-w-screen-sm lg:hidden space-y-2 space-x-3">
            {userProfile &&
              userProfile.userProfileStatus === "success" &&
              userProfile.data.id === product.product.user_id && (
                <button
                  className="btn-category-active px-5 py-3 w-full justify-center hover:bg-primary-purple-5"
                  onClick={(e) => {
                    navigate(
                      `/dashboard/updateproduct/${product.product.slug}`
                    );
                  }}
                >
                  Edit
                </button>
              )}
            {userProfile &&
              userProfile.userProfileStatus === "success" &&
              userProfile.data.id !== product.product.user_id &&
              bidderArrPending === false &&
              bidderArrAccepted === false && (
                <button
                  className="btn-category-active px-5 py-3 w-full justify-center hover:bg-primary-purple-5"
                  onClick={(e) => {
                    if (!isAuth._id) {
                      navigate("/login");
                    }
                    dispatch(removeStatusOffer());
                    setShowBargainModal(true);
                  }}
                >
                  Saya tertarik dan ingin nego
                </button>
              )}
            {userProfile &&
            userProfile.userProfileStatus === "success" &&
            userProfile.data.id !== product.product.user_id &&
            bidderArrPending === true ? (
              <button
                className="btn-category-active px-5 py-3 w-full justify-center bg-gray-400"
                disabled
              >
                Menunggu respon penjual
              </button>
            ) : null}

            {userProfile &&
              userProfile.userProfileStatus === "success" &&
              userProfile.data.id !== product.product.user_id &&
              bidderArrAccepted === true && (
                <button
                  className="btn-category-active px-5 py-3 w-full justify-center bg-yellow-400"
                  disabled
                >
                  Tawaran diterima
                </button>
              )}

            <button
              className="text-4xl flex items-center justify-center"
              onClick={(e) => handleWishlist(e)}
            >
              {wishList ? (
                <MdFavorite className="text-red-500 flex items-center justify-center" />
              ) : (
                <MdOutlineFavoriteBorder />
              )}
            </button>
          </div>
        </div>
      </div>
    ) : (
      <LoadingScreen done={100} />
    );

  let detailNoAuth =
    productNoAuth.message === "success" ? (
      <div className="">
        <div className="hidden lg:block">
          <Navbar />
        </div>
        <BargainModal
          product={productNoAuth}
          showBargainModal={showBargainModal}
          setShowBargainModal={setShowBargainModal}
        />
        <div className="lg:mt-20 max-w-4xl mx-auto lg:grid lg:grid-cols-[2fr_1fr] lg:gap-5 lg:py-5">
          {statusWishlist === "wishListAdd" && <Status wishList={wishList} />}

          {/* btn back */}
          <div className="absolute left-4 top-12 sm:left-10 z-40">
            <button
              className="bg-white rounded-full lg:hidden"
              onClick={(e) => {
                navigate(-1);
              }}
            >
              <MdArrowBack className="text-xl" />
            </button>
          </div>

          {/* carousel */}
          <CarouselDetail product={productNoAuth} />

          {/* card checkout & seller */}
          <div className="mx-auto px-3 max-w-screen-sm -translate-y-7 lg:-translate-y-0 space-y-5 lg:space-y-5 lg:px-0">
            <CardCheckOutNoAuth
              setShowBargainModal={setShowBargainModal}
              product={productNoAuth}
              wishList={wishList}
              handleWishlist={handleWishlist}
            />
            <CardSeller product={productNoAuth} />
          </div>

          {/* deskripsi */}
          <div className="mx-auto px-3 max-w-screen-sm lg:min-w-full mb-20 lg:mb-0 lg:px-0">
            <CardDeskripsi product={productNoAuth} />
          </div>

          {/* btn nego melayang */}
          <div className="flex items-center justify-center mx-auto fixed bottom-5 inset-x-5 max-w-screen-sm lg:hidden space-y-2 space-x-3">
            <button
              className="btn-category-active px-5 py-3 w-full justify-center hover:bg-primary-purple-5"
              onClick={(e) => {
                if (!isAuth._id) {
                  navigate("/login");
                }
                dispatch(removeStatusOffer());
                setShowBargainModal(true);
              }}
            >
              Saya tertarik dan ingin nego
            </button>
            <button
              className="text-4xl flex items-center justify-center"
              onClick={(e) => {
                if (!isAuth._id) {
                  navigate("/login");
                } else {
                  handleWishlist(e);
                }
              }}
            >
              {wishList ? (
                <MdFavorite className="text-red-500 flex items-center justify-center" />
              ) : (
                <MdOutlineFavoriteBorder />
              )}
            </button>
          </div>
        </div>
      </div>
    ) : (
      <LoadingScreen done={100} />
    );

  return <>{isAuth._id ? detailWithAuth : detailNoAuth}</>;
};

export default Detail;
