import React, { useState, useEffect } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { BiShowAlt, BiHide, BiArrowBack, BiHandicap } from "react-icons/bi";
import { GrClose } from "react-icons/gr";
import { FiCamera } from "react-icons/fi";
import { RiUploadLine } from "react-icons/ri";
import Navbar from "../../components/Navbar/Navbar";
import ModalUpImg from "../../components/Modal/ModalUpImg";
import { useDispatch, useSelector } from "react-redux";
import {
  removeStatus,
  getDetailProduct,
  fetchAsyncDetailProduct,
  removeDetail,
  updateProduct,
  editNewState,
  getStatusProduct,
} from "../../redux/productSlice";
import { ModalLoading } from "../../components/Loading/ModalLoading";
import ModalRejected from "../../components/Modal/ModalRejected";

const AddProduct = () => {
  const { slug } = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const status = useSelector(getStatusProduct);
  const editProduct = useSelector(editNewState);
  const product = useSelector(getDetailProduct);

  const [openModalUp, setOpenModalUp] = useState(false);
  const [imgErr, setImgErr] = useState(false);
  const [formData, setFormData] = useState("");
  const [img, setImg] = useState([]);
  const [data, setData] = useState({
    name: product.message === "success" ? product.product.product_name : "",
    desc: product.message === "success" ? product.product.product_desc : "",
    price: product.message === "success" ? product.product.product_price : 0,
    category: product.message === "success" ? product.product.category_id : "1",
  });

  // const photoProduct =
  //   product.message === "success" &&
  //   product.product.product_images.map((img) => {
  //     return (
  //       <div className="w-16 h-16" key={img.product_name}>
  //         <img
  //           className="w-24 h-24 object-contain"
  //           src={img.product_images_path}
  //           alt={img.product_images_name}
  //         />
  //       </div>
  //     );
  //   });

  const photos = img.map((file, index) => (
    <div className="w-16 h-16" key={file.name}>
      <img
        className="w-24 h-24 object-contain"
        src={file.preview}
        // Revoke data uri after image is loaded
        onLoad={() => {
          URL.revokeObjectURL(file.preview);
        }}
      />
    </div>
  ));

  const addForm = () => {
    formData.append("product_name", data.name);
    formData.append("product_desc", data.desc);
    formData.append("product_price", data.price);
    formData.append("category_id", data.category);
    img.forEach((file) => {
      formData.append("product_images_name", file);
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (img.length === 0) {
      setImgErr(true);
    } else {
      addForm();
      dispatch(updateProduct({ slug, formData }));
    }
  };

  if (status === "updateProduct") {
    navigate("/dashboard");
  }

  useEffect(() => {
    setFormData(new FormData());
    dispatch(removeStatus());
    dispatch(fetchAsyncDetailProduct(slug));
  }, [dispatch, slug]);

  return (
    <div>
      {status === "pending" && <ModalLoading />}
      {status === "rejected" && <ModalRejected editProduct={editProduct} />}
      {openModalUp && (
        <div className="absolute top-1/4 left-1/2 -translate-x-1/2 z-10">
          <ModalUpImg
            setOpenModalUp={setOpenModalUp}
            setImg={setImg}
            data={data}
            setData={setData}
          />
        </div>
      )}
      <div className="hidden sm:block">
        <Navbar />
      </div>
      <div className="sm:mt-24 sm:block mt-8 flex pt-5">
        <div className="flex w-full sm:max-w-[712px] mx-auto ">
          <div className="flex flex-row flex-auto w-4 sm:w-16 gap-3 text-2xl py-2">
            <BiArrowBack
              className="hidden lg:block cursor-pointer"
              onClick={(e) => {
                navigate(-1);
              }}
            />
          </div>
          <form
            className="flex sm:max-w-[568px] flex-col w-full mx-auto"
            onSubmit={handleSubmit}
          >
            <div className="relative gap-3 text-2xl mb-6 lg:hidden">
              <BiArrowBack
                className="absolute top-0 text-2xl cursor-pointer"
                onClick={(e) => {
                  navigate(-1);
                }}
              />
              <p className="text-gray-900 text-lg leading-tight font-bold text-center">
                Lengkapi Detail Produk
              </p>
            </div>
            <div className="flex flex-col text-gray-700 py-2 ">
              <label>Nama Produk*</label>
              <input
                className="bg-white h-10 px-5 pr-10 rounded-2xl text-sm border border-gray-400 w-full focus:outline-none focus:bg-gray-200"
                placeholder={
                  product.message === "success"
                    ? product.product.product_name
                    : "Nama Produk"
                }
                type="text"
                id="name"
                name="name"
                required
                onChange={(e) => setData({ ...data, name: e.target.value })}
              />
            </div>
            <div className="flex flex-col text-gray-700 py-2 ">
              <label>Harga Produk*</label>
              <input
                className="bg-white h-10 px-5 pr-10 rounded-2xl text-sm border border-gray-400 w-full focus:outline-none focus:bg-gray-200"
                placeholder={
                  product.message === "success"
                    ? "Rp. " +
                      product.product.product_price.toLocaleString("id-ID")
                    : "Rp 0,00"
                }
                type="number"
                id="price"
                name="price"
                required
                onChange={(e) => setData({ ...data, price: e.target.value })}
              />
            </div>
            <div className="flex flex-col text-gray-700 py-2 ">
              <label>Kategory*</label>
              <div className="inline-block relative w-full">
                <select
                  placeholder={
                    product.message === "success"
                      ? product.product.category_id
                      : "Rp 0,00"
                  }
                  className="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded-2xl leading-tight focus:outline-none focus:shadow-outline"
                  id="product_category"
                  name="product_category"
                  required
                  onChange={(e) =>
                    setData({ ...data, category: e.target.value })
                  }
                >
                  <option value="1">Hobi</option>
                  <option value="2">Kendaraan</option>
                  <option value="3">Baju</option>
                  <option value="4">Elektronik</option>
                  <option value="5">Kesehatan</option>
                </select>
                <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                  <svg
                    className="fill-current h-4 w-4"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                  >
                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                  </svg>
                </div>
              </div>
            </div>
            <div className="flex flex-col text-gray-700 py-2 ">
              <label>Deskripsi*</label>
              <textarea
                className="bg-white h-10 px-5 py-2 pr-10 rounded-2xl text-sm border border-gray-400 w-full focus:outline-none focus:bg-gray-200"
                placeholder={
                  product.message === "success"
                    ? product.product.product_desc
                    : "Contoh: Jalan Ikan Hiu 33"
                }
                type="text"
                rows="4"
                cols="50"
                id="desc"
                name="desc"
                required
                onChange={(e) => setData({ ...data, desc: e.target.value })}
              />
            </div>
            <label>Foto Produk*</label>
            <div className="flex space-x-2">
              {/* {photoProduct} */}
              {photos}
              {img.length >= 4 ? null : (
                <div
                  className="border-dashed border-2 border-indigo-600 w-24 h-24 flex justify-center items-center cursor-pointer"
                  onClick={(e) => setOpenModalUp(true)}
                >
                  <RiUploadLine />
                </div>
              )}
            </div>
            <div>{imgErr && <h1>Kudu masukin foto</h1>}</div>
            <div className="flex gap-5">
              <button
                type="submit"
                className="w-full my-5 py-2 rounded-2xl border-2 border-primary-purple-4 text-primary-purple-4 font-semibold"
                onClick={(e) => navigate("preview")}
              >
                Preview
              </button>
              <button
                type="submit"
                className="w-full my-5 py-2 rounded-2xl text-white bg-primary-purple-4 shadow-lg shadow-purple-200/50 font-semibold"
                required
              >
                edit
              </button>
            </div>
          </form>
          <div className="flex flex-row flex-auto w-4 sm:w-16 gap-3 text-3xl mb-4"></div>
        </div>
      </div>
    </div>
  );
};

export default AddProduct;
