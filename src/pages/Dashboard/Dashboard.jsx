import React, { useEffect } from "react";
import ContentSeller from "../../components/ContentSeller/ContentSeller";
import CardSeller from "../../components/Card/CardSeller";
import Navbar from "../../components/Navbar/Navbar";
import { useDispatch, useSelector } from "react-redux";
import { fetchMyProfile, getUserProfile } from "../../redux/userSlice";
import {
  deleteProductState,
  fetchAsyncProductsUser,
  getAllProductsUser,
  getdeleteStatusProduct,
  getModalBargain,
  getStatusDelete,
  getStatusProduct,
  getStatusUpdateProduct,
  removeStatus,
} from "../../redux/productSlice";
import LoadingScreen from "../../components/Loading/LoadingScreen";
import { fetchWishlist, getStatusWishList } from "../../redux/wishlist";
import {
  getAllTransaction,
  getTransactionBySeller,
} from "../../redux/transactionSlice";
import {
  fetchAllProductOffer,
  fetchOfferByBidder,
  fetchOfferByBidderAccepted,
  fetchOfferByBidderPending,
  fetchOfferByBidderRejected,
  fetchOfferBySeller,
  fetchOfferBySellerAccepted,
  fetchOfferBySellerPending,
  fetchOfferBySellerRejected,
  removeStatusAcceptOffer,
  removeStatusRejectOffer,
} from "../../redux/offerSlice";
import Status from "../../components/Toast/Status";
import VertifikasiAkun from "../VertifikasiAkun/VertifikasiAkun";
import { fetchAsyncNotifs } from "../../redux/notifSlice";

const Dashboard = () => {
  const dispatch = useDispatch();
  const AllProductUser = useSelector(getAllProductsUser);
  const userProfile = useSelector(getUserProfile);
  const status = useSelector(getStatusProduct);
  const statusDelete = useSelector(getdeleteStatusProduct);
  const statusDeleteWishlist = useSelector(getStatusWishList);

  useEffect(() => {
    dispatch(fetchMyProfile(userProfile));
    dispatch(fetchAsyncNotifs());
    dispatch(fetchAllProductOffer());
    dispatch(fetchWishlist());
    dispatch(getAllTransaction());
    dispatch(fetchOfferByBidder());
    dispatch(fetchOfferByBidderAccepted());
    dispatch(fetchOfferByBidderPending());
    dispatch(fetchOfferByBidderRejected());
    dispatch(fetchOfferBySeller());
    dispatch(fetchOfferBySellerAccepted());
    dispatch(fetchOfferBySellerPending());
    dispatch(fetchOfferBySellerRejected());
    dispatch(getTransactionBySeller());
    dispatch(removeStatusAcceptOffer());
    dispatch(removeStatusRejectOffer());
  }, [dispatch]);

  useEffect(() => {
    if (userProfile && userProfile.userProfileStatus === "success") {
      dispatch(fetchAsyncProductsUser(userProfile.data.id));
    }
  }, [userProfile.userProfileStatus]);

  return userProfile.userProfileStatus === "pending" ? (
    <LoadingScreen done={100} />
  ) : userProfile.userProfileStatus === "success" ? (
    <div className="space-y-7 mt-24 mb-10">

      {/* modal */}
      {(statusDeleteWishlist === "wishListDelete" && (
        <Status deleteWishList={true} remove={true} />
      )) ||
        (status === "deleteProduct" && (
          <Status deleteProduct={true} remove={true} />
        )) ||
        (status === "addProduct" && <Status addProduct={true} />)
        ||
        (status === "updateProduct" && <Status updateProduct={true} />)}

      <nav className="">
        <Navbar />
      </nav>
      <section className="contianer mx-auto px-3 md:max-w-screen-md xl:max-w-screen-lg">
        <CardSeller btnEdit={true} data={userProfile} />
      </section>
      <section className="contianer mx-auto md:max-w-screen-md xl:max-w-screen-lg">
        {AllProductUser && AllProductUser.message === "success" ? (
          <ContentSeller />
        ) : null}
      </section>
    </div>
  ) : (
    <VertifikasiAkun />
  );
  //  userProfile.userProfileStatus === "rejected" ? (
  //   <VertifikasiAkun />
  // ) : (
  //   <h1>lagi error</h1>
  // );
};

export default Dashboard;
