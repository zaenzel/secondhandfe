import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { fetchAsyncProducts, productByHobi } from "../../redux/productSlice";

const Page404 = () => {
  return <div>Page404</div>;
};

export default Page404;
