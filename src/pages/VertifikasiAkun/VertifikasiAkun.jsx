import React from "react";
import login from "../../images/login.jpg";
import { AiOutlineLeft, AiOutlineRight } from "react-icons/ai";

const VertifikasiAkun = () => {
  return (
    <div className="w-screen h-screen" data-testid="verti-akun">
      <div className=" flex flex-col items-center">
        <img
          src={login}
          alt="ilustrasi login"
          className="max-w-xs sm:max-w-md"
        />
        <div className="space-y-5 flex flex-col items-center lg:-translate-y-10">
          <h1 className="text-xl">Kamu harus login dulu</h1>
          <div className="flex space-x-10 text-sm text-blue-500">
            <div className="flex items-center">
              <AiOutlineLeft className="mr-2"/>
              <a href="/">Kembali</a>
            </div>
            <div className="flex items-center">
              <a href="/login">Lanjut login</a>
              <AiOutlineRight className="ml-2"/>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VertifikasiAkun;
