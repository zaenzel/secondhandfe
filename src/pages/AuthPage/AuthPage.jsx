import React from "react";
import authImage from "../../images/auth.png";
import { useLocation } from "react-router-dom";
import RegistFormWrapper from "../../components/RegisterForm/RegisterForm";
import LoginFormWrapper from "../../components/LoginForm/LoginForm";

const Register = () => {
  const location = useLocation();
  return (
    <div className="grid grid-cols-1 sm:grid-cols-2 h-screen w-full">
      <div className="hidden sm:block">
        <img src={authImage} className="w-full h-screen object-cover" alt="" />
      </div>

      <div className="bg-white flex flex-col justify-center">
        {location.pathname === "/register" && <RegistFormWrapper />}
        {location.pathname === "/login" && <LoginFormWrapper />}
      </div>
    </div>
  );
};

export default Register;