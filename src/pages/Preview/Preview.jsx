import React, { useEffect } from "react";
import imgProduct from "../../images/jam-gede.png";
import { MdArrowBack } from "react-icons/md";
import { useNavigate } from "react-router-dom";
import Navbar from "../../components/Navbar/Navbar";
import imgFail from "../../images/imgFail.png";
import jam from "../../images/jam.png";
import { useDispatch, useSelector } from "react-redux";
import { previewState } from "../../redux/productSlice";
import Carousel from "better-react-carousel";
import { fetchMyProfile } from "../../redux/userSlice";

const Preview = ({ setPreview, preview, data }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch()
  const userProfile = useSelector((state) => state.user);

  useEffect(() => {
    dispatch(fetchMyProfile(userProfile));
  }, [dispatch]);

  return (
    <div className="">
      <div className="lg:mt-20 max-w-4xl mx-auto lg:grid lg:grid-cols-[2fr_1fr] lg:gap-5 lg:py-5">
        <div className="absolute left-4 top-12 sm:left-10">
          <button
            className="bg-white rounded-full "
            onClick={(e) => {
              setPreview(!preview);
            }}
          >
            <MdArrowBack className="text-xl" />
          </button>
        </div>

        {data ? (
          <div>
            <Carousel cols={1} rows={1} gap={5} loop>
              {data.img.map((e, i) => {
                return (
                  <Carousel.Item key={i} data-testid="carousel-preview">
                    <img
                      className="w-full min-h-72 max-h-80 lg:rounded-2xl object-contain lg:object-cover"
                      src={e.preview}
                      alt={"haa"}
                    />
                  </Carousel.Item>
                );
              })}
            </Carousel>
          </div>
        ) : (
          <img
            src={jam}
            alt="adda"
            className="w-full min-h-72 max-h-80 lg:rounded-2xl object-contain lg:object-cover"
          />
        )}

        <div className="mx-auto px-3 w-full max-w-screen-sm -translate-y-7 lg:-translate-y-0 space-y-5 lg:space-y-5 lg:px-0">
          {/* card checkout */}
          <div className="py-4 px-5 shadow-lg rounded-2xl font-poppins space-y-3 border-gray-100 bg-white">
            <p className="text-sm font-medium truncate">{data.name}</p>
            {/* <p className="text-sm font-medium truncate">asdasd</p> */}
            <p className="text-xs text-neutral-3">{data.category}</p>
            {/* <p className="text-xs text-neutral-3">adadad</p> */}
            <p className="text-sm">{data.price}</p>
            {/* <p className="text-sm">asdad</p> */}
            <div className="space-y-2 hidden lg:block">
              <button className="btn-category-active px-5 py-3 w-full justify-center hover:bg-primary-purple-5">
                Terbitkan
              </button>
              <button className="text-neutral-4 text-sm flex items-center font-poppins rounded-xl border border-primary-purple-4 px-5 py-3 w-full justify-center hover:bg-slate-200">
                Edit
              </button>
            </div>
          </div>
          <div className="flex justify-between py-4 px-5 h-fit shadow-lg rounded-2xl font-poppin border-gray-10 items-center bg-white font-poppins">
            <div className="flex">
              <img
                src={userProfile.message === "success" ? userProfile.data.profile_picture_path : null}
                alt="avatar"
                className="w-12 h-12 object-contain rounded-xl"
              />

              <div className="space-y-2 ml-2 max-w-[190px]">
                <p className="text-sm font-medium truncate">{userProfile.message === "success" ? userProfile.data.name : null}</p>
                <p className="text-sm font-medium truncate">{userProfile.message === "success" ? userProfile.data.address : null}</p>
              </div>
            </div>
          </div>
        </div>
        <div className="mx-auto px-3 max-w-screen-sm lg:min-w-full mb-20 lg:mb-0 lg:px-0">
          <div className="px-5 py-4 shadow-lg rounded-2xl font-poppins space-y-3 border-gray-10 bg-white">
            <h1 className="text-neutral-5 font-medium text-sm">Deskripsi</h1>
            <p className="text-neutral-3 text-sm">{data.desc}</p>
            {/* <p className="text-neutral-3 text-sm">asdasd</p> */}
          </div>
        </div>
        <div className="mx-auto fixed bottom-5 inset-x-5 max-w-screen-sm lg:hidden">
          <button
            className="w-full btn-category-active px-5 py-3 justify-center hover:bg-primary-purple-5"
            onClick={(e) => {
              navigate("/dashboard");
            }}
          >
            Terbitkan
          </button>
        </div>
      </div>
    </div>
  );
};

export default Preview;
