import React, { useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { BiShowAlt, BiHide, BiArrowBack } from "react-icons/bi";
import avatar from "../../images/sasuke.jpg";
import miniWatch from "../../images/miniWatch.png";
import DealModal from "../../components/Modal/DealModal";
import StatusModal from "../../components/Modal/StatusModal";
import Navbar from "../../components/Navbar/Navbar";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import {
  acceptOfferState,
  fetchAllProductOffer,
  fetchProductOfferById,
  getAllProductOffer,
  getProductOfferById,
  putAcceptProductOffer,
  putRejectProductOffer,
  rejectOfferState,
  removeStatusAcceptOffer,
  removeStatusRejectOffer,
  statusAcceptOfferState,
  statusRejectOfferState,
} from "../../redux/offerSlice";
import LoadingCard from "../../components/Loading/LoadingCard";
import { BsWhatsapp } from "react-icons/bs";
import { fetchAsyncNotifs } from "../../redux/notifSlice";

const OfferPage = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { id } = useParams();

  const allProductOffer = useSelector(getAllProductOffer);
  const productOfferById = useSelector(getProductOfferById);
  const acceptOffer = useSelector(acceptOfferState);
  const statusAcceptOffer = useSelector(statusAcceptOfferState);
  const rejectOffer = useSelector(rejectOfferState);
  const statusRejectOffer = useSelector(statusRejectOfferState);
  const [showDealModal, setShowDealModal] = useState(false);
  const [showStatusDealModal, setStatusDealModal] = useState(false);
  const [showStatusModal, setShowStatusModal] = useState(false);

  useEffect(() => {
    dispatch(fetchAllProductOffer());
    dispatch(fetchProductOfferById(id));
    dispatch(fetchAsyncNotifs());
    dispatch(removeStatusAcceptOffer());
    dispatch(removeStatusRejectOffer());
  }, [dispatch, id]);

  useEffect(() => {
    if (statusAcceptOffer === "success") {
      setShowDealModal(true);
    }
    if (statusRejectOffer === "success") {
      navigate("/dashboard");
    }
  }, [statusAcceptOffer, statusRejectOffer]);

  const offer = {
    userId:
      productOfferById && productOfferById.message === "success"
        ? productOfferById.result.disc_product.user_id
        : "",
    productId:
      productOfferById && productOfferById.message === "success"
        ? productOfferById.result.disc_product.product_id
        : "",
  };

  const acceptOfferSubmit = (e) => {
    e.preventDefault();
    dispatch(putAcceptProductOffer(offer));
  };

  const rejectOfferSubmit = (e) => {
    e.preventDefault();
    dispatch(putRejectProductOffer(offer));
  };

  console.log(productOfferById);

  let renderProductOffer = "";

  renderProductOffer =
    productOfferById && productOfferById.message === "success" ? (
      <>
        {/* info pembeli */}
        <div className="w-full mt-4 p-4 border rounded-2xl gap-5 flex flex-row items-center">
          <img
            src={
              productOfferById.result.disc_product.bidder.profile_picture_path
            }
            alt="avatar"
            className="w-12 h-12 object-cover rounded-xl"
          />
          <div>
            <h6 className="text-sm font-medium truncate">
              {productOfferById.result.disc_product.bidder.name}
            </h6>
            <p className="text-xs text-neutral-3">
              {productOfferById.result.disc_product.bidder.address}
            </p>
          </div>
        </div>
        {/* Daftar Produkmu yang Ditawar */}
        <div className="my-4">
          <h6 className="text-sm font-medium truncate mb-2">
            Daftar Produkmu Yang Ditawar
          </h6>
          <div className="w-full p-4 shadow-md gap-5 flex flex-row items-start">
            <img
              src={
                productOfferById.result.disc_product.product_offered
                  .product_images[0].product_images_path
              }
              alt="avatar"
              className="w-12 h-12 object-cover rounded-xl"
            />
            <div className="w-full space-y-2">
              <div className="relative w-full">
                <p className="text-xs text-neutral-3">Penawaran produk</p>
                <p className="text-xs text-neutral-3 absolute top-0 right-0">
                  {productOfferById.result.createdAt}
                </p>
              </div>
              <h6 className="text-sm font-normal truncate">
                {
                  productOfferById.result.disc_product.product_offered
                    .product_name
                }
              </h6>
              <h6 className="text-sm font-normal truncate">
                Rp.{" "}
                {productOfferById.result.disc_product.product_offered.product_price.toLocaleString(
                  "id-ID"
                )}
              </h6>
              <h6 className="text-sm font-normal text-green-500 truncate">
                Ditawar Rp{" "}
                {productOfferById.result.disc_product.bargain_price.toLocaleString(
                  "id-ID"
                )}
              </h6>
            </div>
          </div>
        </div>
      </>
    ) : (
      <div className="grid grid-cols-2 gap-3">
        <LoadingCard />
        <LoadingCard />
        <LoadingCard />
        <LoadingCard />
      </div>
    );

  return (
    <div>
      <DealModal
        showDealModal={showDealModal}
        setShowDealModal={setShowDealModal}
      />
      <StatusModal
        showStatusModal={showStatusModal}
        setShowStatusModal={setShowStatusModal}
        productOfferById={productOfferById}
      />
      <div className="hidden sm:block lg:mt-24">
        <Navbar />
      </div>
      <div className="sm:mt-8 sm:block flex pt-5">
        <div className="flex w-full sm:max-w-[712px] mx-auto">
          {/* kiri */}
          <div className="flex flex-row flex-auto w-4 sm:w-16 gap-3 text-2xl py-2">
            <BiArrowBack
              className="hidden lg:block cursor-pointer"
              onClick={(e) => {
                navigate(-1);
              }}
            />
          </div>
          {/* tengah */}
          <div className="flex sm:max-w-[568px] flex-col w-full mx-auto bg-white">
            {/* back and title */}
            <div className="relative gap-3 text-2xl my-4 lg:hidden">
              <BiArrowBack
                className="absolute top-0 text-2xl cursor-pointer"
                onClick={(e) => {
                  navigate(-1);
                }}
              />
              <p className="text-gray-900 text-lg leading-tight font-bold text-center">
                Info Penawar
              </p>
            </div>
            {/* product offer */}
            {renderProductOffer}

            {/* button */}
            {productOfferById && productOfferById.message === "success" ? (
              <>
                {productOfferById.result.disc_product.status === "rejected" && (
                  <div className="flex gap-5">
                    <button
                      type="submit"
                      className="w-full my-5 py-2 rounded-md bg-red-100 text-gray-500 font-semibold"
                      disabled
                    >
                      Kamu telah menolak tawaran dari{" "}
                      {productOfferById.result.disc_product.bidder.name}
                    </button>
                  </div>
                )}
                {productOfferById.result.disc_product.status === "accepted" &&
                  productOfferById.result.disc_product.product_offered
                    .status === "sold" && (
                    <div className="flex gap-5">
                      <button
                        type="submit"
                        className="w-full my-5 py-2 rounded-md bg-green-100 text-gray-500 font-semibold"
                        disabled
                      >
                        {`Kamu telah menjual ${productOfferById.result.disc_product.product_offered.product_name} ke ${productOfferById.result.disc_product.bidder.name}`}
                      </button>
                    </div>
                  )}
                {productOfferById.result.disc_product.status === "pending" && (
                  <div className="flex gap-5">
                    <button
                      type="submit"
                      className="w-full my-5 py-2 rounded-2xl border-2 border-primary-purple-4 text-primary-purple-4 font-semibold"
                      onClick={rejectOfferSubmit}
                    >
                      {statusRejectOffer === "pending" ? "Loading..." : "Tolak"}
                    </button>
                    <button
                      type="submit"
                      className="w-full my-5 py-2 rounded-2xl text-white bg-primary-purple-4 shadow-lg shadow-purple-200/50 font-semibold"
                      onClick={acceptOfferSubmit}
                    >
                      {statusAcceptOffer === "pending"
                        ? "Loading..."
                        : "Terima"}
                    </button>
                  </div>
                )}
                {productOfferById.result.disc_product.status === "accepted" &&
                  productOfferById.result.disc_product.product_offered
                    .status === "unsold" && (
                    <div className="flex gap-5">
                      <button
                        type="submit"
                        className="w-full my-5 py-2 rounded-2xl border-2 border-primary-purple-4 text-primary-purple-4 font-semibold"
                        onClick={() => setShowStatusModal(true)}
                      >
                        Status
                      </button>

                      <button
                        type="submit"
                        className="w-full my-5 py-2 rounded-2xl text-white bg-primary-purple-4 shadow-lg shadow-purple-200/50 font-semibold"
                        onClick={() => {
                          setShowDealModal(false);
                        }}
                      >
                        <a
                          href={`https://wa.me/${productOfferById.result.disc_product.bidder.phone_number}`}
                          target="_blank"
                          className=""
                        >
                          <p>Hubungi di WhatsApp</p>
                        </a>
                      </button>
                    </div>
                  )}
              </>
            ) : null}
          </div>

          <div className="flex flex-row flex-auto w-4 sm:w-16 gap-3 text-3xl mb-4"></div>
        </div>
      </div>
    </div>
  );
};

export default OfferPage;
