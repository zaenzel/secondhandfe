import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import Navbar from "../../components/Navbar/Navbar";
import jam from "../../images/jam.png";
import { getAllNotifs, fetchAsyncNotifs } from "../../redux/notifSlice";
import LoadingCard from "../../components/Loading/LoadingCard";
import NullData from "../../components/NullData/NullData";
import { fetchMyProfile, getUserProfile } from "../../redux/userSlice";
import LoadingScreen from "../../components/Loading/LoadingScreen";

const NotifMobile = () => {
  const dispatch = useDispatch();
  const userProfile = useSelector(getUserProfile);
  const getAllNotif = useSelector(getAllNotifs);

  useEffect(() => {
    dispatch(fetchMyProfile(userProfile));
    dispatch(fetchAsyncNotifs());
  }, [dispatch]);

  let renderModalNotif,
    thumbnail = "";

  thumbnail =
    getAllNotif &&
    getAllNotif.message === "success" &&
    getAllNotif.result.thumbnail.map((product, index) => {
      return product;
    });

  const date =
    getAllNotif &&
    getAllNotif.message === "success" &&
    getAllNotif.result.createdAt.map((date, index) => {
      return date;
    });

  renderModalNotif =
    getAllNotif && getAllNotif.message === "success" ? (
      getAllNotif.result.notification.length === 0 ? (
        <div className="py-10 lg:py-0">
          <NullData props={false} />
        </div>
      ) : (
        <div className="flex justify-center flex-col space-y-5">
          <h1 className="flex justify-center sm:mt-10 text-xl text-neutral-5">
            Semua notifikasi
          </h1>
          <div className="flex justify-center flex-col sm:w-full max-w-screen-sm self-center">
            {getAllNotif.result.notification.map((product, index) => (
              <Link to={product.url} key={index}>
                <div className="flex p-3 relative space-x-5 mt-2">
                  <img
                    src={thumbnail[index]}
                    alt={product.product_notification.product_name.slice(0, 6)}
                    className="w-12 h-12 rounded-xl text-sm"
                  />
                  <div className="text-start space-y-1">
                    <p className="text-xsx text-neutral-3">
                      {product.action_message}
                    </p>
                    <p className="text-xs text-neutral-5">
                      {product.product_notification.product_name}
                    </p>
                    <p className="text-xs text-neutral-5">
                      Rp.{" "}
                      {product.product_notification.product_price.toLocaleString(
                        "id-ID"
                      )}
                    </p>
                    {product.additional_info_1 && (
                      <p className="text-xs text-neutral-5">
                        {product.additional_info_1} Rp.{" "}
                        {product.bargain_price.toLocaleString("id-ID")}
                      </p>
                    )}
                    {product.additional_info_2 && (
                      <p className="text-xsx text-neutral-3">
                        {product.additional_info_2}
                      </p>
                    )}
                  </div>
                  <div className="flex sm:absolute sm:right-2 space-x-2">
                    <p className="text-xsx text-neutral-3">{date[index]}</p>
                    {product.is_read === false ? (
                      <div className="p-1 h-1 items-center rounded-full bg-alert-red" />
                    ) : null}
                  </div>
                </div>
                <span>
                  <hr></hr>
                </span>
              </Link>
            ))}
          </div>
        </div>
      )
    ) : (
      <div className="grid grid-cols-2 gap-3">
        <LoadingScreen />
      </div>
    );

  return (
    <div data-testid="notif-mobile">
      <nav>
        <Navbar notifMobile={true} />
      </nav>

      <div className="p-3 space-y-3 divide-y-2 mt-12">
        {renderModalNotif}
        {/* <Link to={"dashboard/tawaran/:id"}>
                <div className="flex p-3 space-x-5 border-b-2">
                    <img src={jam} alt="photo product" className="w-12 h-12 rounded-xl" />
                    <div className="text-start space-y-1">
                        <p className="text-xsx text-neutral-3">Penawaran produk</p>
                        <p className="text-sm text-neutral-5">Jam Tangan Casio</p>
                        <p className="text-sm text-neutral-5 line-through">Rp 250.000</p>
                        <p className="text-sm text-neutral-5">Ditawar Rp 200.000</p>
                    </div>
                    <div className="flex space-x-1">
                        <p className="text-xsx text-neutral-3">20 Apr, 14:04</p>
                        <div className="p-1 h-1 items-center rounded-full bg-alert-red" />
                    </div>
                </div>
            </Link>
            <Link to={"/dashboard"}>
                <div className="flex p-3 space-x-5">
                    <img src={jam} alt="photo product" className="w-12 h-12 rounded-xl" />
                    <div className="text-start space-y-1">
                        <p className="text-xsx text-neutral-3">Berhasil di terbitkan</p>
                        <p className="text-sm text-neutral-5">Jam Tangan Casio</p>
                        <p className="text-sm text-neutral-5">Rp 250.000</p>
                    </div>
                    <div className="flex space-x-1">
                        <p className="text-xsx text-neutral-3">20 Apr, 14:04</p>
                        <div className="p-1 h-1 items-center rounded-full bg-alert-red" />
                    </div>
                </div>
            </Link> */}
      </div>
    </div>
  );
};

export default NotifMobile;
