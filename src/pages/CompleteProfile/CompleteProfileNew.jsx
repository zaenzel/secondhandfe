import React, { useRef, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { BiShowAlt, BiHide, BiArrowBack } from "react-icons/bi";
import { GrClose } from "react-icons/gr";
import { FiCamera } from "react-icons/fi";
import Navbar from "../../components/Navbar/Navbar";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import {
  fetchMyProfile,
  updateUserProfile,
  updateUserProfileState,
} from "../../redux/userSlice";
import {
  fetchAsyncCities,
  fetchAsyncCityById,
  getAllCities,
  getCityById,
} from "../../redux/citySlice";
import LoadingScreen from "../../components/Loading/LoadingScreen";
import { validPhoneNumber } from "../../utils/Regex";

const CompleteProfileNew = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const img = useRef();

  const userProfile = useSelector((state) => state.user);
  const AllCities = useSelector(getAllCities);
  const userUpdate = useSelector(updateUserProfileState);
  const data = AllCities.data;
  const [phoneNumError, setPhoneNumError] = useState(false);
  const [picture, setPicture] = useState(null);

  useEffect(() => {
    dispatch(fetchAsyncCities());
    dispatch(fetchMyProfile(userProfile));
  }, [dispatch]);

  const [user, setUser] = useState({
    email: "",
    name:
      userProfile && userProfile.userProfileStatus === "success"
        ? userProfile.data.name
        : "pending",
    address:
      userProfile && userProfile.userProfileStatus === "success"
        ? userProfile.data.address
        : "pending",
    profilePicture:
      userProfile && userProfile.userProfileStatus === "success"
        ? userProfile.data.profile_picture
        : "pending",
    phoneNumber:
      userProfile && userProfile.userProfileStatus === "success"
        ? userProfile.data.phone_number
        : "pending",
    cityId:
      userProfile && userProfile.userProfileStatus === "success"
        ? userProfile.data.city_id
        : "pending",
  });

  const validate = () => {
    if (!validPhoneNumber.test(user.phoneNumber)) {
      setPhoneNumError(true);
    } else {
      dispatch(updateUserProfile(user));
      navigate("/");
    }
  };

  const handleUpdateSubmit = (e) => {
    e.preventDefault();
    validate();
  };

  return (
    <>
      {userProfile.userProfileStatus === "rejected" && null}
      {userProfile.userProfileStatus === "pending" && <LoadingScreen />}
      {userProfile.userProfileStatus === "success" && (
        <div>
          <div className="hidden sm:block">
            <Navbar />
          </div>
          <div className="sm:mt-24 sm:block flex pt-5">
            <div className="flex w-full sm:max-w-[712px] mx-auto ">
              <div className="flex flex-row flex-auto w-4 sm:w-16 gap-3 text-2xl py-2">
                <BiArrowBack
                  className="hidden lg:block cursor-pointer"
                  onClick={(e) => {
                    navigate(-1);
                  }}
                />
              </div>
              <form
                onSubmit={handleUpdateSubmit}
                className="flex sm:max-w-[568px] flex-col w-full mx-auto"
              >
                <div className="relative gap-3 text-2xl mb-6 lg:hidden">
                  <BiArrowBack
                    className="absolute top-0 text-2xl cursor-pointer"
                    onClick={(e) => {
                      navigate(-1);
                    }}
                  />
                  <p className="text-gray-900 text-lg leading-tight font-bold text-center">
                    Lengkapi Info Akun
                  </p>
                </div>
                <div className="flex justify-center">
                  <div className="relative flex justify-center w-20 h-20 bg-primary-purple-1 rounded-2xl border">
                    <img
                      src={
                        picture
                          ? picture
                          : userProfile.data.profile_picture_path || null
                      }
                      alt={userProfile.data.profile_picture}
                      className="rounded-2xl"
                    />
                    <input
                      type="file"
                      accept="image/*"
                      name="image"
                      hidden
                      ref={img}
                      onChange={(e) => {
                        let pic = URL.createObjectURL(e.target.files[0]);
                        setUser({ ...user, profilePicture: e.target.files[0] });
                        setPicture(pic);
                      }}
                    />
                    <div
                      onClick={() => img.current.click()}
                      className="absolute -left-2 -top-2 bg-primary-purple-1 p-1 rounded-full border-2 border-white z-40"
                    >
                      <FiCamera className=" text-primary-purple-4" />
                    </div>
                  </div>
                </div>
                <div className="flex flex-col text-gray-700 py-2 ">
                  <label>Email<span className="text-red-700">*</span></label>
                  <input
                    className="bg-white h-10 px-5 pr-10 rounded-2xl text-sm border border-gray-400 w-full focus:outline-none focus:bg-gray-200"
                    placeholder="Email"
                    type="text"
                    defaultValue={userProfile.data.email}
                    disabled
                  />
                </div>
                <div className="flex flex-col text-gray-700 py-2 ">
                  <label>Nama<span className="text-red-700">*</span></label>
                  <input
                    className="bg-white h-10 px-5 pr-10 rounded-2xl text-sm border border-gray-400 w-full focus:outline-none focus:bg-gray-200"
                    placeholder="Nama Lengkap"
                    defaultValue={userProfile.data.name}
                    type="text"
                    required
                    onChange={(e) => setUser({ ...user, name: e.target.value })}
                  />
                </div>
                <div className="flex flex-col text-gray-700 py-2 ">
                  <label>Kota<span className="text-red-700">*</span></label>
                  <div className="inline-block relative w-full">
                    <select
                      placeholder="Pilih Kota"
                      required
                      defaultValue={userProfile.data.city_id}
                      onChange={(e) =>
                        setUser({ ...user, cityId: e.target.value })
                      }
                      className="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded-2xl leading-tight focus:outline-none focus:shadow-outline"
                    >
                      <option defaultValue={userProfile.data.city_id}></option>
                      {AllCities.message === "success" ? (
                        data.map((city, index) => {
                          return <option value={city.id}>{city.city}</option>;
                        })
                      ) : (
                        <option>select</option>
                      )}
                    </select>
                    <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                      <svg
                        className="fill-current h-4 w-4"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 20 20"
                      >
                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                      </svg>
                    </div>
                  </div>
                </div>
                <div className="flex flex-col text-gray-700 py-2 ">
                  <label>Alamat<span className="text-red-700">*</span></label>
                  <textarea
                    className="bg-white h-10 px-5 pr-10 py-2 rounded-2xl text-sm border border-gray-400 w-full focus:outline-none focus:bg-gray-200"
                    placeholder="Contoh: Jalan Ikan Hiu 33"
                    type="text"
                    rows="4"
                    cols="50"
                    defaultValue={userProfile.data.address}
                    required
                    onChange={(e) =>
                      setUser({ ...user, address: e.target.value })
                    }
                  />
                </div>
                <div className="flex flex-col text-gray-700 py-2 ">
                  <label>No Telepon<span className="text-red-700">*</span></label>
                  <input
                    className="bg-white h-10 px-5 pr-10 rounded-2xl text-sm border border-gray-400 w-full focus:outline-none focus:bg-gray-200"
                    placeholder="contoh: 628123456789"
                    type="number"
                    title="masukan valid number"
                    required
                    defaultValue={userProfile.data.phone_number}
                    onChange={(e) =>
                      setUser({ ...user, phoneNumber: e.target.value })
                    }
                  />
                  {phoneNumError ? <p className="text-sm text-red-700">Format salah, nomor diawali dengan 62</p> : ""}
                </div>
                <button
                  type="submit"
                  className="w-full my-5 py-2 rounded-2xl text-white bg-primary-purple-4 shadow-lg shadow-purple-200/50 font-semibold"
                >
                  Simpan
                </button>
              </form>
              <div className="flex flex-row flex-auto w-4 sm:w-16 gap-3 text-3xl mb-4"></div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default CompleteProfileNew;
