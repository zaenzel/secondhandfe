import React, {useState} from "react";
import { Link, useNavigate } from "react-router-dom";
import { BiShowAlt, BiHide, BiArrowBack } from "react-icons/bi";
import { GrClose } from "react-icons/gr";
import { FiCamera } from "react-icons/fi";
import Navbar from "../../components/Navbar/Navbar";

const CompleteProfile = () => {
  const navigate = useNavigate();
  return (
    <div>
      <div className="hidden sm:block">
        <Navbar />
      </div>
      <div className="sm:mt-24 sm:block flex pt-5">
        <div className="flex w-full sm:max-w-[712px] mx-auto ">
          <div className="flex flex-row flex-auto w-4 sm:w-16 gap-3 text-2xl py-2">
            <BiArrowBack
              className="hidden lg:block cursor-pointer"
              onClick={(e) => {
                navigate(-1);
              }}
            />
          </div>
          <form className="flex sm:max-w-[568px] flex-col w-full mx-auto">
            <div className="relative gap-3 text-2xl mb-6 lg:hidden">
              <BiArrowBack
                className="absolute top-0 text-2xl cursor-pointer"
                onClick={(e) => {
                  navigate(-1);
                }}
              />
              <p className="text-gray-900 text-lg leading-tight font-bold text-center">
                Lengkapi Info Akun
              </p>
            </div>
            <div className="flex justify-center">
              <div className="flex justify-center bg-primary-purple-1 rounded-2xl p-9">
                <FiCamera />
              </div>
            </div>
            <div className="flex flex-col text-gray-700 py-2 ">
              <label>Nama*</label>
              <input
                className="bg-white h-10 px-5 pr-10 rounded-2xl text-sm border border-gray-400 w-full focus:outline-none focus:bg-gray-200"
                placeholder="Nama Lengkap"
                type="text"
              />
            </div>
            <div className="flex flex-col text-gray-700 py-2 ">
              <label>Kota*</label>
              <div className="inline-block relative w-full">
                <select
                  placeholder="Pilih Kota"
                  className="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded-2xl leading-tight focus:outline-none focus:shadow-outline"
                >
                  <option>Option 1</option>
                  <option>Option 2</option>
                  <option>Option 3</option>
                </select>
                <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                  <svg
                    className="fill-current h-4 w-4"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                  >
                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                  </svg>
                </div>
              </div>
            </div>
            <div className="flex flex-col text-gray-700 py-2 ">
              <label>Alamat*</label>
              <textarea
                className="bg-white h-10 px-5 pr-10 rounded-2xl text-sm border border-gray-400 w-full focus:outline-none focus:bg-gray-200"
                placeholder="Contoh: Jalan Ikan Hiu 33"
                type="text"
                rows="4"
                cols="50"
              />
            </div>
            <div className="flex flex-col text-gray-700 py-2 ">
              <label>No Telepon*</label>
              <input
                className="bg-white h-10 px-5 pr-10 rounded-2xl text-sm border border-gray-400 w-full focus:outline-none focus:bg-gray-200"
                placeholder="contoh: +628123456789"
                type="text"
              />
            </div>
            <button
              type="submit"
              className="w-full my-5 py-2 rounded-2xl text-white bg-primary-purple-4 shadow-lg shadow-purple-200/50 font-semibold"
            >
              Simpan
            </button>
          </form>
          <div className="flex flex-row flex-auto w-4 sm:w-16 gap-3 text-3xl mb-4"></div>
        </div>
      </div>
    </div>
  );
};

export default CompleteProfile;
