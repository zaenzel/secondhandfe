import React, { useState } from "react";
import { useEffect } from "react";
import {
  FaHome,
  FaBell,
  FaPlusCircle,
  FaListUl,
  FaRegUser,
} from "react-icons/fa";
import { FiCamera, FiEdit3, FiSettings, FiLogOut } from "react-icons/fi";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import ButtonCategory from "../../components/Button/ButtonCategory/ButtonCategory";
import { logoutUser } from "../../redux/authSlice";
import { fetchMyProfile } from "../../redux/userSlice";

const AccountMobile = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const userProfile = useSelector((state) => state.user);

  useEffect(() => {
    dispatch(fetchMyProfile(userProfile));
  }, [dispatch]);

  return (
    <div className="">
      <p className="font-black text-xl my-4 mx-4">Akun Saya</p>
      {userProfile.userProfileStatus === "success" && (
        <div className="flex justify-center flex-col">
          {userProfile.data.profile_picture === null ? (
            <div className="flex justify-center bg-primary-purple-1 p-5 rounded-2xl w-20 self-center">
              <FiCamera className="text-primary-purple-4" />
            </div>
          ) : (
            <div className="flex justify-center rounded-2xl w-20 self-center">
              <img
                className="rounded-md object-cover"
                src={userProfile.data.profile_picture_path}
                alt={userProfile.data.profile_picture}
              />
            </div>
          )}

          <div className="text-center space-y-1">
            <p className="font-normal text-base text-neutral-5 mt-2 underline">
              {userProfile.data.name}
            </p>
          </div>
        </div>
      )}

      <div className="mt-8 mx-4">
        <Link
          to="/profile-edit"
          className="w-fill flex p-3 hover:text-primary-purple-4 active:text-primary-purple-4 border-b"
        >
          <FiEdit3 className="w-5 h-full text-primary-purple-4 font-bold" />
          <p className="ml-4 truncate flex font-bold">Ubah Profil</p>
        </Link>
        <Link
          to="/"
          className="w-fill flex p-3 hover:text-primary-purple-4 active:text-primary-purple-4 border-b"
        >
          <FiSettings className="w-5 h-full text-primary-purple-4 font-bold" />
          <p className="ml-4 truncate flex font-bold">Pengaturan Akun</p>
        </Link>
        <button
          href="#"
          className="w-fill flex p-3 hover:text-primary-purple-4 active:text-primary-purple-4 border-b"
          onClick={() => {
            dispatch(logoutUser(null));
            navigate("/");
          }}
        >
          <FiLogOut className="w-5 h-full text-primary-purple-4 font-bold" />
          <p className="ml-4 truncate flex font-bold">Keluar</p>
        </button>
      </div>
      <div className="flex justify-center mt-4">
        <p className="flex justify-center text-gray-400 lg:text-xl sm:text-xs">
          Version 1.0.0
        </p>
      </div>
      <section className="block fixed bottom-0 inset-x-0 z-50 border-t-2 pb-2">
        <div id="tabs" className="flex justify-between">
          <Link
            to="/"
            className="w-full focus:text-royal hover:text-royal justify-center inline-block text-center pt-2 pb-1 hover:text-primary-purple-4 hover:font-bold"
          >
            <FaHome className="md:w-6 md:h-6 sm:w-5 sm:h-auto inline-block mb-1" />
            <span className="tab block text-xs">Home</span>
          </Link>
          <Link
            to="/notification"
            className="w-full focus:text-royal hover:text-royal justify-center inline-block text-center pt-2 pb-1 hover:text-primary-purple-4 hover:font-bold"
          >
            <FaBell className="md:w-6 md:h-6 sm:w-5 inline-block mb-1" />
            <span className="tab block text-xs">Notifikasi</span>
          </Link>
          <Link
            to="/dashboard/addproduct"
            className="w-full focus:text-royal hover:text-royal justify-center inline-block text-center pt-2 pb-1 hover:text-primary-purple-4 hover:font-bold"
          >
            <FaPlusCircle className="md:w-6 md:h-6 sm:w-5 inline-block mb-1" />
            <span className="tab block text-xs">Jual</span>
          </Link>
          <Link
            to="/dashboard"
            className="w-full focus:text-royal hover:text-royal justify-center inline-block text-center pt-2 pb-1 hover:text-primary-purple-4 hover:font-bold"
          >
            <FaListUl className="md:w-6 md:h-6 sm:w-5 inline-block mb-1" />
            <span className="tab block text-xs">Daftar Jual</span>
          </Link>
        </div>
      </section>
    </div>
  );
};

export default AccountMobile;
