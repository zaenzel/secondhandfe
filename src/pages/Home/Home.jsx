import React, { useEffect, useState } from "react";
import CardListing from "../../components/CardListing/CardListing";
import Carousel from "../../components/Carousel/Carousel";
import ButtonJual from "../../components/Button/ButtonJual/ButtonJual";
import Navbar from "../../components/Navbar/Navbar";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchAsyncProducts,
  fetchAsyncProductsUser,
  fetchProductBySlug,
  getAllProducts,
  getProductBySlug,
  productByBaju,
  productByElektronik,
  productByHobi,
  productByKendaraan,
  productByKesehatan,
} from "../../redux/productSlice";
import { fetchMyProfile, getUserProfile } from "../../redux/userSlice";
import { fetchAllProductOffer } from "../../redux/offerSlice";
import { fetchAsyncNotifs } from "../../redux/notifSlice";

const Home = () => {
  const dispatch = useDispatch();
  const userProfile = useSelector(getUserProfile);

  const [productPage, setProductPage] = useState({
    filter: "",
    page: 1,
  });

  useEffect(() => {
    dispatch(fetchMyProfile(userProfile));
    dispatch(fetchAsyncNotifs());
    dispatch(fetchAsyncProducts(productPage));
    dispatch(productByHobi());
    dispatch(productByKendaraan());
    dispatch(productByBaju());
    dispatch(productByElektronik());
    dispatch(productByKesehatan());
    dispatch(fetchAllProductOffer());
  }, [dispatch]);

  return (
    <div className="mb-10 sm:mb-10">
      <nav className="">
        <Navbar input={true} />
      </nav>

      <section className="sm:mt-28 md:mb-10">
        <Carousel />
      </section>

      <section className="container mx-auto -translate-y-20 sm:-translate-y-0 md:max-w-screen-md lg:max-w-screen-lg">
        <CardListing />
      </section>

      <div className="fixed bottom-5 left-[50%] -translate-x-1/2 shadow-primary-purple-4">
        <ButtonJual />
      </div>
    </div>
  );
};

export default Home;
