import { render, fireEvent, screen, cleanup } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import '@testing-library/jest-dom';
import { Provider } from "react-redux";
import { store } from "../../redux/store";
import NotifMobile from "../../pages/NotifMobile/NotifMobile";


afterEach(() => {
    cleanup(); // Resets the DOM after each test suite
})

//testing Notif
describe("Notif", () => {
    it("should render NotifMobile", () => {
        render(
            <BrowserRouter>
                <Provider store={store}><NotifMobile/></Provider>
            </BrowserRouter>
        );
        const notifMobile = screen.getByTestId("notif-mobile");
        expect(notifMobile).toBeInTheDocument();
    });
})