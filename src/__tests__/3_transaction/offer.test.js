import { render, fireEvent, screen, cleanup } from "@testing-library/react";
import LoginForm from "../../components/LoginForm/LoginForm";
import RegistForm from "../../components/RegisterForm/RegisterForm";
import { BrowserRouter } from "react-router-dom";
import '@testing-library/jest-dom';
import { loginUser } from "../../redux/authSlice";
import { Provider } from "react-redux";
import { store } from "../../redux/store";
import LoadingCard from "../../components/Loading/LoadingCard";
import DealModal from "../../components/Modal/DealModal";
import StatusModal from "../../components/Modal/StatusModal";


afterEach(() => {
    cleanup(); // Resets the DOM after each test suite
})

//testing offer
describe("LoginRegister", () => {
    it("should render LoadingCard", () => {
        render(
            <BrowserRouter>
                <Provider store={store}><LoadingCard/></Provider>
            </BrowserRouter>
        );
        const loadingCard = screen.getByTestId("loading-card");
        expect(loadingCard).toBeInTheDocument();
    });
    it("should render DealModal", () => {
        render(
            <BrowserRouter>
                <Provider store={store}><DealModal/></Provider>
            </BrowserRouter>
        );
        const dealModal = screen.getByTestId("deal-modal");
        expect(dealModal).toBeInTheDocument();
    });
    it("should render StatusModal", () => {
        render(
            <BrowserRouter>
                <Provider store={store}><StatusModal/></Provider>
            </BrowserRouter>
        );
        const statusModal = screen.getByTestId("status-modal");
        expect(statusModal).toBeInTheDocument();
    });
})