import { render, fireEvent, screen, cleanup } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import '@testing-library/jest-dom';
import { Provider } from "react-redux";
import { store } from "../../redux/store";
import Navbar from "../../components/Navbar/Navbar";
import Carousel from "../../components/Carousel/Carousel";
import CardListing from "../../components/CardListing/CardListing";
import ButtonJual from "../../components/Button/ButtonJual/ButtonJual";


afterEach(() => {
    cleanup(); // Resets the DOM after each test suite
})

//testing home
describe("Home", () => {
    it("should render Navbar with true prop", () => {
        render(
            <BrowserRouter>
                <Provider store={store}><Navbar input={true}/></Provider>
            </BrowserRouter>
        );
        const navTrue = screen.getByTestId("navbar-true");
        expect(navTrue).toBeInTheDocument();
    });
    it("should render Carousel", () => {
        render(
            <BrowserRouter>
                <Provider store={store}><Carousel/></Provider>
            </BrowserRouter>
        );
        const carousel = screen.getByTestId("carousel");
        expect(carousel).toBeInTheDocument();
    });
    it("should render CardListing", () => {
        render(
            <BrowserRouter>
                <Provider store={store}><CardListing/></Provider>
            </BrowserRouter>
        );
        const cardListing = screen.getByTestId("card-listing");
        expect(cardListing).toBeInTheDocument();
    });
    it("should render ButtonJual", () => {
        render(
            <BrowserRouter>
                <Provider store={store}><ButtonJual/></Provider>
            </BrowserRouter>
        );
        const btnJual = screen.getByTestId("btn-jual");
        expect(btnJual).toBeInTheDocument();
    });
})