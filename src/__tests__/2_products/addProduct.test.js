import { render, fireEvent, screen, cleanup } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import '@testing-library/jest-dom';
import { Provider } from "react-redux";
import { store } from "../../redux/store";
import { ModalLoading } from "../../components/Loading/ModalLoading";
import ModalRejected from "../../components/Modal/ModalRejected";
import ModalUpImg from "../../components/Modal/ModalUpImg";


afterEach(() => {
    cleanup(); // Resets the DOM after each test suite
})

//testing AddProduct
describe("addProduct", () => {
    it("should render ModalLoading", () => {
        render(
            <BrowserRouter>
                <Provider store={store}><ModalLoading/></Provider>
            </BrowserRouter>
        );
        const modalLoading = screen.getByTestId("modal-loading");
        expect(modalLoading).toBeInTheDocument();
    });
    it("should render ModalRejected", () => {
        render(
            <BrowserRouter>
                <Provider store={store}><ModalRejected/></Provider>
            </BrowserRouter>
        );
        const modalRejected = screen.getByTestId("modal-rejected");
        expect(modalRejected).toBeInTheDocument();
    });
    it("should render ModalUpImg", () => {
        render(
            <BrowserRouter>
                <Provider store={store}><ModalUpImg/></Provider>
            </BrowserRouter>
        );
        const modalUpImg = screen.getByTestId("modal-upImg");
        expect(modalUpImg).toBeInTheDocument();
    });
})