import { render, waitFor, screen, cleanup } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import '@testing-library/jest-dom';
import LoadingScreen from "../../components/Loading/LoadingScreen";
import Status from "../../components/Toast/Status";
import { Provider, } from "react-redux";
import { store } from "../../redux/store";
import ContentSeller from "../../components/ContentSeller/ContentSeller";
import VertifikasiAkun from "../../pages/VertifikasiAkun/VertifikasiAkun";



afterEach(() => {
    cleanup(); // Resets the DOM after each test suite
})

//testing login
describe("Dashboard", () => {
    it("should render LoadingScreen", () => {
        render(<BrowserRouter><LoadingScreen/></BrowserRouter>);
        const loginForm = screen.getByTestId("loading-screen");
        expect(loginForm).toBeInTheDocument();
    });
    it("should render status", () => {
        render(
            <BrowserRouter>
                <Provider store={store}><Status /></Provider>
            </BrowserRouter>
        );
        const statusToast = screen.getByTestId("status-toast");
        expect(statusToast).toBeInTheDocument();
    });
    it("should render VertifikasiAkun", () => {
        render(
            <BrowserRouter>
                <Provider store={store}><VertifikasiAkun /></Provider>
            </BrowserRouter>
        );
        const vertifAkun = screen.getByTestId("verti-akun");
        expect(vertifAkun).toBeInTheDocument();
    });
    it("should render ContentSeller", () => {
        render(
            <BrowserRouter>
                <Provider store={store}><ContentSeller /></Provider>
            </BrowserRouter>
        );
        const contentSeller = screen.getByTestId("content-seller");
        expect(contentSeller).toBeInTheDocument();
    });
})