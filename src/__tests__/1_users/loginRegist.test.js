import { render, fireEvent, screen, cleanup } from "@testing-library/react";
import LoginForm from "../../components/LoginForm/LoginForm";
import RegistForm from "../../components/RegisterForm/RegisterForm";
import { BrowserRouter } from "react-router-dom";
import '@testing-library/jest-dom';
import { loginUser } from "../../redux/authSlice";
import { Provider } from "react-redux";
import { store } from "../../redux/store";


afterEach(() => {
    cleanup(); // Resets the DOM after each test suite
})

//testing login
describe("LoginRegister", () => {
    it("should render LoginForm", () => {
        render(
            <BrowserRouter>
                <Provider store={store}><LoginForm/></Provider>
            </BrowserRouter>
        );
        const loginForm = screen.getByTestId("login-form");
        expect(loginForm).toBeInTheDocument();
    });
    // it("OTHER TEST CASE", () => {
    //     render(<LoginFormWrapper />);
    //     const loginForm = screen.getByTestId("login-form");
    //     expect(loginForm).toBeInTheDocument();
    // });
})

//testing register
describe("RegistForm", () => {
    it("should render RegistForm", () => {
        render(
            <BrowserRouter>
                <Provider store={store}><RegistForm/></Provider>
            </BrowserRouter>
        );
        const registForm = screen.getByTestId("regist-form");
        expect(registForm).toBeInTheDocument();
    });
})