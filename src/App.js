import { Route, Routes } from "react-router-dom";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./App.css";
import AddProduct from "./pages/AddProduct/AddProduct";
import Dashboard from "./pages/Dashboard/Dashboard";
import Detail from "./pages/DetailBuyer/Detail";
import Home from "./pages/Home/Home";
import OfferPage from "./pages/OfferPage/OfferPage";
import Page404 from "./pages/Page404/Page404";
import Preview from "./pages/Preview/Preview";
import AuthPage from "./pages/AuthPage/AuthPage.jsx";
import UpdateProduct from "./pages/UpdateProduct/UpdateProduct";
import CompleteProfile from "./pages/CompleteProfile/CompleteProfile";
import AccountMobile from "./pages/AccountMobile/AccountMobile";
import CompleteProfileNew from "./pages/CompleteProfile/CompleteProfileNew";
import NotifMobile from "./pages/NotifMobile/NotifMobile";
import { loadUser } from "./redux/authSlice";
import { fetchMyProfile } from "./redux/userSlice";

function App() {
  const dispatch = useDispatch();
  const userProfile = useSelector((state) => state.user);

  // useEffect(() => {
  //   dispatch(fetchMyProfile(userProfile));
  // }, [dispatch]);

  dispatch(loadUser(null));

  return (
    <>
      <Routes>
        <Route path="/">
          <Route index element={<Home />} />
          <Route path="register" element={<AuthPage />} />
          <Route path="login" element={<AuthPage />} />
          <Route path="profile" element={<CompleteProfile />} />
          <Route path="profile-edit" element={<CompleteProfileNew />} />
          <Route path="detail/:slug" element={<Detail />} />
          <Route path="coba/:slug" element={<Page404 />} />
          <Route path="account-settings" element={<AccountMobile />} />
          <Route path="notification" element={<NotifMobile />} />
          <Route path="dashboard">
            <Route index element={<Dashboard />} />
            <Route path="addproduct" element={<AddProduct />} />
            <Route path="updateproduct/:slug" element={<UpdateProduct />} />
            {/* <Route path="addproduct/preview" element={<Preview />} /> */}
            <Route path="tawaran/:id" element={<OfferPage />} />
          </Route>
          <Route path="*" element={<Page404 />} />
        </Route>
      </Routes>
    </>
  );
}

export default App;
