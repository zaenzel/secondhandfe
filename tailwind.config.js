/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        "primary-purple-5": "#4B1979",
        "primary-purple-4": "#7126B5",
        "primary-purple-3": "#A06ECE",
        "primary-purple-2": "#D0B7E6",
        "primary-purple-1": "#E2D4F0",
        "primary-cream-5": "#AA9B87",
        "primary-cream-4": "#D4C2A8",
        "primary-cream-3": "#FFE9CA",
        "primary-cream-2": "#FFF0DC",
        "primary-cream-1": "#FFF8ED",
        "alert-red": "#FA2C5A",
        "alert-yellow": "#F9CC00",
        "alert-green": "#73CA5C",
        "neutral-5": "#151515",
        "neutral-4": "#3C3C3C",
        "neutral-3": "#8A8A8A",
        "neutral-2": "#D0D0D0",
        "neutral-1": "#FFFFFF",
        "light-gray-1": "#EEEEEE",
      },
      fontFamily: {
        poppins: ["Poppins", "sans-serif"],
      },
      fontSize: {
        xsx: ["10px","12px"],
      },
      spacing: {
        6: "48px",
        90: "360px",
      },
      maxWidth: {
        mltsm: "360px",
      },
    },
  },
  plugins: [],
};
